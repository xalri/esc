use crate::geom::Vec2;
use crate::input::*;
use glium::glutin::{MouseButton, *};
use nuklear::{Context, *};

/// An input map for passing input to a nuklear context.
#[derive(Default)]
pub struct UIInputMap {}

#[derive(Copy, Clone)]
pub enum UIAction {
   Char { c: char },
   Key { key: Key, pressed: bool },
   MouseButton { button: Button, pressed: bool },
   MouseWheel { diff: f32 },
}

#[derive(Copy, Clone, Debug, Default)]
pub struct UIInputState {
   mouse: Vec2,
}

/// Forward input to a nuklear context
pub fn ui_input(controller: &mut Controller<UIAction, UIInputState>, ctx: &mut Context) {
   let mx = controller.mouse.x as i32;
   let my = controller.mouse.y as i32;
   ctx.input_begin();
   ctx.input_motion(controller.mouse.x.round() as i32, controller.mouse.y.round() as i32);
   controller.get_actions(|a| {
      match a {
         UIAction::Char { c } => ctx.input_unicode(c),
         UIAction::Key { key, pressed } => ctx.input_key(key, pressed),
         UIAction::MouseButton { button, pressed } => ctx.input_button(button, mx, my, pressed),
         UIAction::MouseWheel { diff } => ctx.input_scroll(nuklear::Vec2{ x: 0.0, y: diff }),
      }
      ActionResult::Consumed
   });
   ctx.input_end();
}

impl InputMap<UIAction, UIInputState> for UIInputMap {
   fn update_state(&self, input: &InputState, state: &mut UIInputState) {
      //state.mouse.x = input.mouse.view_pos.x;
      //state.mouse.y = input.mouse.view_pos.y;
      state.mouse.x = input.mouse.logical_pos.x;
      state.mouse.y = input.mouse.logical_pos.y;
   }

   fn map_ev(&self, ev: &WindowEvent, new_action: &mut dyn FnMut(UIAction)) -> bool {
      match ev {
         WindowEvent::ReceivedCharacter(c) => new_action(UIAction::Char { c: *c }),
         WindowEvent::KeyboardInput { input, .. } => {
            if let Some(k) = input.virtual_keycode {
               let key = match k {
                  VirtualKeyCode::Back => Key::Backspace,
                  VirtualKeyCode::Delete => Key::Del,
                  VirtualKeyCode::Up => Key::Up,
                  VirtualKeyCode::Down => Key::Down,
                  VirtualKeyCode::Left => Key::Left,
                  VirtualKeyCode::Right => Key::Right,
                  VirtualKeyCode::Tab => Key::Tab,
                  VirtualKeyCode::Return => Key::Enter,
                  _ => Key::None,
               };
               new_action(UIAction::Key {
                  key,
                  pressed: input.state == ElementState::Pressed,
               })
            }
         }
         WindowEvent::MouseInput { state, button, .. } => {
            let button = match button {
               MouseButton::Left => Button::Left,
               MouseButton::Middle => Button::Middle,
               MouseButton::Right => Button::Right,
               _ => Button::Max,
            };
            new_action(UIAction::MouseButton {
               button,
               pressed: *state == ElementState::Pressed,
            })
         }
         WindowEvent::MouseWheel { delta, .. } => {
            let diff = match delta {
               MouseScrollDelta::LineDelta(_, y) => y * 22.0,
               MouseScrollDelta::PixelDelta(pos) => pos.y as f32,
            };
            new_action(UIAction::MouseWheel { diff })
         }
         _ => {}
      }
      false
   }
}
