use super::*;
use std::marker::PhantomData;
use std::mem;
use static_assertions::*;

const MAX_SUBSCRIPTION_COMPONENTS: usize = 32;

/// A subscription to a component query, tracks all the entities which satisfy some query.
pub struct Subscription<T> {
   current: Vec<usize>,
   prev: Vec<usize>,
   hash: [u64; MAX_SUBSCRIPTION_COMPONENTS],
   changed: bool,
   _phantom: PhantomData<T>,
}

impl<T> Default for Subscription<T> {
   fn default() -> Self {
      Subscription {
         current: vec![],
         prev: vec![],
         hash: Default::default(),
         changed: false,
         _phantom: Default::default()
      }
   }
}

impl<T: Query> Subscription<T> {
   /// Update the subscription
   pub fn update(&mut self, store: T::Stores) {
      let _prof = crate::util::hprof::enter("update query");
      // Check if there have been any additions / removals from the relevant stores.
      let hash = T::hash(store);
      self.changed = self.hash[0..T::len()] != hash[0..T::len()];

      if self.changed {
         self.hash = hash;
         self.changed = true;
         // Store the previous state & re-run the query.
         mem::swap(&mut self.current, &mut self.prev);
         self.current.clear();
         T::query(store, |e| self.current.push(e));
      }
   }

   pub fn len(&self) -> usize {
      self.current.len()
   }

   /// Get an iterator over all entities which match the query
   pub fn iter(&self) -> impl Iterator<Item=usize> + '_ {
      self.current.iter().cloned()
   }

   /// Get an iterator over entities which were removed between the last two updates.
   pub fn removed(&self) -> impl Iterator<Item=usize> + '_ {
      DiffIter { done: !self.changed, from: self.current.iter(), to: self.prev.iter(), from_i: None, to_i: None }
   }

   /// Get an iterator over entities which were added between the last two updates.
   pub fn added(&self) -> impl Iterator<Item=usize> + '_ {
      DiffIter { done: !self.changed, from: self.prev.iter(), to: self.current.iter(), from_i: None, to_i: None }
   }
}

impl<'a, T: Query> IntoIterator for &'a Subscription<T> {
   type Item = usize;
   type IntoIter = std::iter::Cloned<::std::slice::Iter<'a, usize>>;
   fn into_iter(self) -> Self::IntoIter {
      self.current.iter().cloned()
   }
}

/// Get the elements which have been added from one sorted slice to another.
pub struct DiffIter<'a> {
   done: bool,
   from: ::std::slice::Iter<'a, usize>,
   to: ::std::slice::Iter<'a, usize>,
   from_i: Option<usize>,
   to_i: Option<usize>,
}

impl<'a> Iterator for DiffIter<'a> {
   type Item = usize;
   fn next(&mut self) -> Option<usize> {
      if self.done { return None; }

      loop {
         if self.from_i.is_none() {
            self.from_i = self.from.next().cloned();
         }

         if self.to_i.is_none() {
            self.to_i = self.to.next().cloned();
            if self.to_i.is_none() {
               self.done = true;
               return None;
            }
         }

         let new = self.to_i.unwrap();
         if self.from_i.is_none() || new < self.from_i.unwrap() {
            self.to_i = None;
            return Some(new);
         } else {
            if new == self.from_i.unwrap() { self.to_i = None; }
            self.from_i = None;
         }
      }
   }
}

// Auto-implemented for tuples of components (Position, Velocity) etc
pub trait Query {
   /// A tuple of references to the component stores required by the query
   type Stores: Copy;

   /// Get the set of entity IDs in the given store which satisfies the query.
   fn query<F: FnMut(usize)>(store: Self::Stores, callback: F);

   /// Get the hash for each component store reference by the query.
   fn hash(store: Self::Stores) -> [u64; MAX_SUBSCRIPTION_COMPONENTS];

   /// Get the number of components referenced by the query
   fn len() -> usize;
}

macro_rules! count {
   ($v:ident) => { 1 };
   ($v:ident, $($tail:ident),*) => { 1 + count!($($tail),*) }
}

/// Create a unique query for a set of components, allows iterating added & removed
/// entities.
pub macro query($subscriptions:ident, ($($t:ident),*), ($($s:ident),*)) {
   {
   pub struct UniqueQuery;
   impl Query for UniqueQuery {
      type Stores = ($(*const Store<$t>,)*);

      fn query<F: FnMut(usize)>(store: Self::Stores, callback: F) {
         <($($t,)*) as Query>::query(store, callback)
      }

      fn hash(store: Self::Stores) -> [u64; MAX_SUBSCRIPTION_COMPONENTS] {
         <($($t,)*) as Query>::hash(store)
      }

      fn len() -> usize {
         <($($t,)*) as Query>::len()
      }
   }
   $subscriptions.get::<UniqueQuery>(($($s,)*))
   }
}

/// Implement query for a tuple
macro_rules! impl_query {
   ($($v:ident: $t:ident),*) => {
      impl<$($t: 'static),*> Query for ($($t,)*) {
         // TODO: can take borrows when we have GATs
         type Stores = ($(*const Store<$t>,)*);

         fn query<Callback: FnMut(usize)>(($($v,)*): Self::Stores, mut callback: Callback) {
            const_assert!(count!($($v),*) > 0);

            $(let $v = unsafe { &*$v };)*
            let n = *[$($v.blocks.len()),*].iter().min().unwrap();

            for ia in 0..n {
               if $($v.blocks[ia].is_some() &&)* true {
                  let bitset = $($v.block(ia).usage)&*;
                  if bitset.count_ones() == 0 { continue }
                  for ib in 0..bitset.len() {
                     if bitset.contains(ib) {
                        callback(ia * bitset.len() + ib);
                     }
                  }
               }
            }
         }

         fn hash(($($v,)*): Self::Stores) -> [u64; MAX_SUBSCRIPTION_COMPONENTS] {
            const_assert!(count!($($v),*) < MAX_SUBSCRIPTION_COMPONENTS);

            $(let $v = unsafe { &*$v };)*

            let mut ret: [u64; MAX_SUBSCRIPTION_COMPONENTS] = unsafe { mem::zeroed() };
            let mut i = -1;
            $(
               i += 1;
               ret[i as usize] = $v.hash;
            )*
            return ret;
         }

         fn len() -> usize { count!($($v),*) }
      }
   };
}

impl_query!(a:A);
impl_query!(a:A, b:B);
impl_query!(a:A, b:B, c:C);
impl_query!(a:A, b:B, c:C, d:D);
impl_query!(a:A, b:B, c:C, d:D, e:E);
impl_query!(a:A, b:B, c:C, d:D, e:E, f:F);
impl_query!(a:A, b:B, c:C, d:D, e:E, f:F, g:G);
impl_query!(a:A, b:B, c:C, d:D, e:E, f:F, g:G, h:H);
impl_query!(a:A, b:B, c:C, d:D, e:E, f:F, g:G, h:H, i:I);
impl_query!(a:A, b:B, c:C, d:D, e:E, f:F, g:G, h:H, i:I, j:J);
impl_query!(a:A, b:B, c:C, d:D, e:E, f:F, g:G, h:H, i:I, j:J, k:K);

