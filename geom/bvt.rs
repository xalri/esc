//! A port of Box2D's (http://www.box2d.org) dynamic bounding volume tree.
// Copyright (c) 2009 Erin Catto http://www.box2d.org
// Rust port by <lewis@xa1.uk> 2018-05-17.
//
// This software is provided 'as-is', without any express or implied
// warranty.  In no event will the authors be held liable for any damages
// arising from the use of this software.
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
// 1. The origin of this software must not be misrepresented; you must not
// claim that you wrote the original software. If you use this software
// in a product, an acknowledgment in the product documentation would be
// appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
// misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.

use crate::geom::Bounds;
use crate::geom::Vec2;
use std::cell::Cell;
use std::cmp;

pub const NULL_NODE: i32 = -1;

/// A node in the dynamic tree.
#[derive(Clone, Default)]
pub struct Node {
   pub parent: i32,
   pub child1: i32,
   pub child2: i32,
   pub height: i32,
   pub data: Cell<i32>,
   pub aabb: Bounds,
}

impl Node {
   pub fn is_leaf(&self) -> bool {
      self.child1 == NULL_NODE
   }
}

/// A dynamic AABB tree broad-phase, inspired by Nathanael Presson's btDbvt.
/// A dynamic tree arranges data in a binary tree to accelerate
/// queries such as volume queries and ray casts. Leafs are proxies
/// with an AABB. In the tree we expand the proxy AABB by `AABB_EXTENSION`
/// so that the proxy AABB is bigger than the client object. This allows the client
/// object to move by small amounts without triggering a tree update.
///
/// Nodes are pooled and relocatable, so we use node indices rather than pointers.
pub struct DynamicTree {
   pub(crate) root: i32,
   pub(crate) nodes: Vec<Node>,
   /// The index of the first free element
   free_list: i32,
   expansion: f32,
}

impl Default for DynamicTree {
   fn default() -> Self { Self::with_expansion(2.0) }
}

impl DynamicTree {
   /// Create a new AABB tree.
   pub fn new() -> DynamicTree {
      Self::with_expansion(2.0)
   }

   /// Create a new AABB tree with the given expansion.
   pub fn with_expansion(expansion: f32) -> DynamicTree {
      let mut tree = DynamicTree {
         expansion,
         root: NULL_NODE,
         nodes: vec![],
         free_list: 0,
      };
      tree.expand(16);
      return tree;
   }

   /// Create a proxy. Provide a tight fitting AABB and some user data.
   pub fn create_proxy(&mut self, aabb: Bounds, data: i32) -> i32 {
      let id = self.allocate_node();
      self.nodes[id as usize].aabb = aabb.expand(self.expansion);
      self.nodes[id as usize].height = 0;
      self.nodes[id as usize].data = Cell::new(data);
      self.insert_leaf(id);
      id
   }

   /// Destroy a proxy. This asserts if the id is invalid.
   pub fn destroy_proxy(&mut self, id: i32) {
      self.remove_leaf(id);
      self.free_node(id);
   }

   /// Move a proxy with a swept AABB. If the proxy has moved outside of its fattened AABB,
   /// then the proxy is removed from the tree and re-inserted. Otherwise
   /// the function returns immediately.
   /// @return true if the proxy was re-inserted.
   pub fn move_proxy(&mut self, id: i32, aabb: Bounds) -> bool {
      if self.nodes[id as usize].aabb.contains_bounds(&aabb) {
         return false;
      }

      self.remove_leaf(id);
      self.nodes[id as usize].aabb = aabb.expand(self.expansion);
      self.insert_leaf(id);

      true
   }

   /// Query the tree for proxies which overlap with a given aabb. The callback
   /// is called with the user data for each proxy that overlaps the supplied AABB.
   pub fn query_aabb<F>(&mut self, aabb: Bounds, callback: F) where
      F: FnMut(i32) -> bool
   {
      self.query(|node| node.intersects(aabb), callback)
   }

   /// Query the tree for proxies which intersect a given ray.
   pub fn query_ray<F>(&mut self, pos: Vec2, dir: Vec2, callback: F) where
      F: FnMut(i32) -> bool
   {
      self.query(|node| node.ray_intersects(pos, dir), callback)
   }

   /// Query the tree for proxies given an overlap function.
   pub fn query<O, F>(&mut self, overlap: O, mut callback: F) where
      O: Fn(Bounds) -> bool,
      F: FnMut(i32) -> bool
   {
      fn check_node<O, F>(id: i32, nodes: &[Node], overlap: &O, callback: &mut F) where
         O: Fn(Bounds) -> bool,
         F: FnMut(i32) -> bool
      {
         if id == NULL_NODE { return }
         let node = &nodes[id as usize];

         if overlap(node.aabb) {
            if node.is_leaf() {
               if !callback(node.data.get()) {
                  return;
               }
            } else {
               check_node(node.child1, nodes, overlap, callback);
               check_node(node.child2, nodes, overlap, callback);
            }
         }
      }

      check_node(self.root, &self.nodes, &overlap, &mut callback)
   }

   fn clear_traverse_bit(&mut self) {
      for n in &mut self.nodes {
         if !n.is_leaf() { n.data.set(0); }
      }
   }

   /// Find all overlapping pairs of leaf nodes.
   pub fn overlapping_pairs<F: FnMut(i32, i32)>(&mut self, mut callback: F) {
      if self.root == NULL_NODE { return; }
      self.clear_traverse_bit();

      let root = &self.nodes[self.root as usize];
      if root.is_leaf() { return; }

      fn check_pair<F: FnMut(i32, i32)>(a: i32, b: i32, nodes: &[Node], callback: &mut F) {
         let node_a = &nodes[a as usize];
         let node_b = &nodes[b as usize];

         if !node_a.is_leaf() && !node_b.is_leaf() {
            if node_a.data.get() == 0 {
               node_a.data.set(1);
               check_pair(node_a.child1, node_a.child2, nodes, callback);
            }
            if node_b.data.get() == 0 {
               node_b.data.set(1);
               check_pair(node_b.child1, node_b.child2, nodes, callback);
            }
         }

         if !node_a.aabb.intersects(node_b.aabb) {
            return
         }

         if node_a.is_leaf() && node_b.is_leaf() {
            callback(node_a.data.get(), node_b.data.get());
            return;
         }

         if node_a.is_leaf() {
            check_pair(a, node_b.child1, nodes, callback);
            check_pair(a, node_b.child2, nodes, callback);
            return;
         }

         if node_b.is_leaf() {
            check_pair(node_a.child1, b, nodes, callback);
            check_pair(node_a.child2, b, nodes, callback);
            return;
         }

         check_pair(node_a.child1, node_b.child1, nodes, callback);
         check_pair(node_a.child1, node_b.child2, nodes, callback);
         check_pair(node_a.child2, node_b.child1, nodes, callback);
         check_pair(node_a.child2, node_b.child2, nodes, callback);
      }

      check_pair(root.child1, root.child2, &self.nodes, &mut callback);
   }

   /// Expand the pool to the given capacity
   fn expand(&mut self, capacity: usize) {
      self.free_list = self.nodes.len() as i32;

      while self.nodes.len() < capacity {
         let next = (self.nodes.len() + 1) as i32;
         self.nodes.push(Node {
            child2: next,
            height: -1,
            ..Node::default()
         });
      }
      self.nodes.push(Node {
         child2: NULL_NODE,
         height: -1,
         ..Node::default()
      });
   }

   /// Allocate a node from the pool. Grow the pool if necessary.
   fn allocate_node(&mut self) -> i32 {
      if self.free_list == NULL_NODE {
         self.expand(self.nodes.len() * 2);
      }

      let id = self.free_list;
      self.free_list = self.nodes[id as usize].child2;
      self.nodes[id as usize].parent = NULL_NODE;
      self.nodes[id as usize].child1 = NULL_NODE;
      self.nodes[id as usize].child2 = NULL_NODE;
      self.nodes[id as usize].height = 0;

      id
   }

   /// Return a node to the pool.
   fn free_node(&mut self, node: i32) {
      self.nodes[node as usize].child2 = self.free_list;
      self.nodes[node as usize].height = -1;
      self.free_list = node;
   }

   fn insert_leaf(&mut self, leaf: i32) {
      if self.root == NULL_NODE {
         self.root = leaf;
         self.nodes[self.root as usize].parent = NULL_NODE;
         return;
      }

      // Find the best sibling for this node
      let leaf_aabb = self.nodes[leaf as usize].aabb;
      let mut index = self.root;
      while !self.nodes[index as usize].is_leaf() {
         let child1 = self.nodes[index as usize].child1;
         let child2 = self.nodes[index as usize].child2;

         let area = self.nodes[index as usize].aabb.perimeter();

         let combined_aabb = self.nodes[index as usize].aabb.combine(leaf_aabb);
         let combined_area = combined_aabb.perimeter();

         // Cost of creating a new parent for this node and the new leaf
         let cost = (2.0 * combined_area) as f32;

         // Minimum cost of pushing the leaf further down the tree
         let inheritance_cost = 2.0 * (combined_area - area) as f32;

         let get_cost = |child| {
            if self.nodes[child as usize].is_leaf() {
               let aabb = leaf_aabb.combine(self.nodes[child as usize].aabb);
               aabb.perimeter() + inheritance_cost
            } else {
               let aabb = leaf_aabb.combine(self.nodes[child as usize].aabb);
               let old_area = self.nodes[child as usize].aabb.perimeter();
               let new_area = aabb.perimeter();
               (new_area - old_area) + inheritance_cost
            }
         };

         let cost1 = get_cost(child1);
         let cost2 = get_cost(child2);

         // Descend according to the minimum cost.
         if cost < cost1 && cost < cost2 {
            break;
         }

         // Descend
         if cost1 < cost2 {
            index = child1;
         } else {
            index = child2;
         }
      }

      let sibling = index;

      // Create a new parent.
      let old_parent = self.nodes[sibling as usize].parent;
      let new_parent = self.allocate_node();
      self.nodes[new_parent as usize].parent = old_parent;
      self.nodes[new_parent as usize].aabb = leaf_aabb.combine(self.nodes[sibling as usize].aabb);
      self.nodes[new_parent as usize].height = self.nodes[sibling as usize].height + 1;
      self.nodes[new_parent as usize].data.set(-1);

      if old_parent != NULL_NODE {
         let parent = self.get_mut(old_parent);
         if parent.child1 == sibling {
            parent.child1 = new_parent
         } else {
            parent.child2 = new_parent
         }
      } else {
         self.root = new_parent;
      }

      self.nodes[new_parent as usize].child1 = sibling;
      self.nodes[new_parent as usize].child2 = leaf;
      self.nodes[sibling as usize].parent = new_parent;
      self.nodes[leaf as usize].parent = new_parent;

      // Walk back up the tree fixing heights and AABBs
      index = self.nodes[leaf as usize].parent;
      while index != NULL_NODE {
         index = self.balance(index);

         let child1 = self.nodes[index as usize].child1;
         let child2 = self.nodes[index as usize].child2;

         self.nodes[index as usize].height =
            1 + cmp::max(self.nodes[child1 as usize].height, self.nodes[child2 as usize].height);
         self.nodes[index as usize].aabb = self.nodes[child1 as usize]
            .aabb
            .combine(self.nodes[child2 as usize].aabb);

         index = self.nodes[index as usize].parent;
      }
   }

   fn remove_leaf(&mut self, leaf: i32) {
      if leaf == self.root {
         self.root = NULL_NODE;
         return;
      }

      let parent = self.nodes[leaf as usize].parent;
      let grand_parent = self.nodes[parent as usize].parent;
      let sibling = if self.nodes[parent as usize].child1 == leaf {
         self.nodes[parent as usize].child2
      } else {
         self.nodes[parent as usize].child1
      };

      if grand_parent != NULL_NODE {
         // Destroy parent and connect sibling to grand_parent.
         if self.nodes[grand_parent as usize].child1 == parent {
            self.nodes[grand_parent as usize].child1 = sibling;
         } else {
            self.nodes[grand_parent as usize].child2 = sibling;
         }
         self.nodes[sibling as usize].parent = grand_parent;
         self.free_node(parent);

         // Adjust ancestor bounds.
         let mut index = grand_parent;
         while index != NULL_NODE {
            index = self.balance(index);

            let child1 = self.nodes[index as usize].child1;
            let child2 = self.nodes[index as usize].child2;

            self.nodes[index as usize].aabb = self.nodes[child1 as usize]
               .aabb
               .combine(self.nodes[child2 as usize].aabb);
            self.nodes[index as usize].height =
               1 + cmp::max(self.nodes[child1 as usize].height, self.nodes[child2 as usize].height);

            index = self.nodes[index as usize].parent;
         }
      } else {
         self.root = sibling;
         self.nodes[sibling as usize].parent = NULL_NODE;
         self.free_node(parent);
      }
   }

   fn get_mut(&self, idx: i32) -> &'static mut Node {
      #[allow(mutable_transmutes)]
      unsafe {
         ::std::mem::transmute(&self.nodes[idx as usize])
      }
   }

   fn balance(&mut self, a_idx: i32) -> i32 {
      let a = self.get_mut(a_idx);
      if a.is_leaf() || a.height < 2 {
         return a_idx;
      }

      let b_idx = a.child1;
      let c_idx = a.child2;

      let b = self.get_mut(b_idx);
      let c = self.get_mut(c_idx);

      let balance = c.height - b.height;

      // Rotate C up
      if balance > 1 {
         let f_idx = c.child1;
         let g_idx = c.child2;
         let f = self.get_mut(f_idx);
         let g = self.get_mut(g_idx);

         // Swap a and C
         c.child1 = a_idx;
         c.parent = a.parent;
         a.parent = c_idx;

         // a's old parent should point to C
         if c.parent != NULL_NODE {
            let parent = self.get_mut(c.parent);
            if parent.child1 == a_idx {
               parent.child1 = c_idx;
            } else {
               parent.child2 = c_idx;
            }
         } else {
            self.root = c_idx;
         }

         // Rotate
         if f.height > g.height {
            c.child2 = f_idx;
            a.child2 = g_idx;
            g.parent = a_idx;
            a.aabb = b.aabb.combine(g.aabb);
            c.aabb = a.aabb.combine(f.aabb);

            a.height = 1 + cmp::max(b.height, g.height);
            c.height = 1 + cmp::max(a.height, f.height);
         } else {
            c.child2 = g_idx;
            a.child2 = f_idx;
            f.parent = a_idx;
            a.aabb = b.aabb.combine(f.aabb);
            c.aabb = a.aabb.combine(g.aabb);

            a.height = 1 + cmp::max(b.height, f.height);
            c.height = 1 + cmp::max(a.height, g.height);
         }

         return c_idx;
      }

      // Rotate b up
      if balance < -1 {
         let d_idx = b.child1;
         let e_idx = b.child2;
         let d = self.get_mut(d_idx);
         let e = self.get_mut(e_idx);

         // Swap a and b
         b.child1 = a_idx;
         b.parent = a.parent;
         a.parent = b_idx;

         // a's old parent should point to b
         if b.parent != NULL_NODE {
            let parent = self.get_mut(b.parent);
            if parent.child1 == a_idx {
               parent.child1 = b_idx;
            } else {
               parent.child2 = b_idx;
            }
         } else {
            self.root = b_idx;
         }

         // Rotate
         if d.height > e.height {
            b.child2 = d_idx;
            a.child1 = e_idx;
            e.parent = a_idx;
            a.aabb = c.aabb.combine(e.aabb);
            b.aabb = a.aabb.combine(d.aabb);

            a.height = 1 + cmp::max(c.height, e.height);
            b.height = 1 + cmp::max(a.height, d.height);
         } else {
            b.child2 = e_idx;
            a.child1 = d_idx;
            d.parent = a_idx;
            a.aabb = c.aabb.combine(d.aabb);
            b.aabb = a.aabb.combine(e.aabb);

            a.height = 1 + cmp::max(c.height, d.height);
            b.height = 1 + cmp::max(a.height, e.height);
         }

         return b_idx;
      }

      return a_idx;
   }
}
