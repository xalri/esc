use std::fmt;
use std::str::FromStr;
use serde::*;
use crate::error::*;

/// A floating point HSV colour
#[derive(Default, Debug, PartialEq, Copy, Clone)]
#[derive(Serialize, Deserialize)]
#[repr(C)]
pub struct HSV {
   pub hue: f32,
   pub saturation: f32,
   pub value: f32,
}

use glium::implement_uniform_block;
implement_uniform_block!(HSV, hue, saturation, value);

impl HSV {
   /// Create a HSV colour
   pub fn new(hue: f32, saturation: f32, value: f32) -> HSV {
      HSV { hue, saturation, value }
   }

   pub fn one() -> HSV {
      Self::new(1.0, 1.0, 1.0)
   }

   /// Convert to RGB
   pub fn to_rgb(&self) -> Colour {
      use crate::geom::math::*;
      let p_r = ((self.hue + 1.0).fract()       * 6.0 - 3.0).abs();
      let p_g = ((self.hue + 2.0 / 3.0).fract() * 6.0 - 3.0).abs();
      let p_b = ((self.hue + 1.0 / 3.0).fract() * 6.0 - 3.0).abs();

      Colour {
         r: self.value * map(self.saturation, [0.0, 1.0], [1.0, clamp(p_r - 1.0, 0.0, 1.0)]),
         g: self.value * map(self.saturation, [0.0, 1.0], [1.0, clamp(p_g - 1.0, 0.0, 1.0)]),
         b: self.value * map(self.saturation, [0.0, 1.0], [1.0, clamp(p_b - 1.0, 0.0, 1.0)]),
         a: 1.0,
      }
   }
}

/// A floating point RGBA colour
#[derive(Default, Debug, PartialEq, Copy, Clone)]
#[derive(Serialize, Deserialize)]
#[repr(C)]
pub struct Colour {
   pub r: f32,
   pub g: f32,
   pub b: f32,
   pub a: f32,
}

impl Colour {
   /// Create a colour from floats
   pub fn new(r: f32, g: f32, b: f32, a: f32) -> Colour {
      Colour { r, g, b, a }
   }

   /// Create a colour from `u8`s
   pub const fn from_u8(r: u8, g: u8, b: u8, a: u8) -> Colour {
      Colour {
         r: r as f32 / 255.0,
         g: g as f32 / 255.0,
         b: b as f32 / 255.0,
         a: a as f32 / 255.0,
      }
   }

   /// Create a colour from an array
   pub fn from_arr(a: [u8; 4]) -> Colour {
      Self::from_u8(a[0], a[1], a[2], a[3])
   }

   /// Convert to `u8`s
   pub fn to_u8(&self) -> (u8, u8, u8, u8) {
      (
         (self.r * 255.0) as u8,
         (self.g * 255.0) as u8,
         (self.b * 255.0) as u8,
         (self.a * 255.0) as u8,
      )
   }

   /// Solid white
   pub fn white() -> Colour {
      Colour::new(1.0, 1.0, 1.0, 1.0)
   }
}

impl From<[f32; 4]> for Colour {
   fn from(x: [f32; 4]) -> Colour {
      Colour {
         r: x[0],
         g: x[1],
         b: x[2],
         a: x[3],
      }
   }
}

impl Into<[f32; 4]> for Colour {
   fn into(self) -> [f32; 4] {
      [self.r, self.g, self.b, self.a]
   }
}

impl Into<[f32; 3]> for HSV {
   fn into(self) -> [f32; 3] {
      [self.hue, self.saturation, self.value]
   }
}

impl From<(f32, f32, f32, f32)> for Colour {
   fn from(x: (f32, f32, f32, f32)) -> Colour {
      Colour {
         r: x.0,
         g: x.1,
         b: x.2,
         a: x.3,
      }
   }
}

impl FromStr for Colour {
   type Err = Error;
   fn from_str(s: &str) -> Result<Colour> {
      let s = if s.starts_with("#") { &s[1..] } else { s };
      if s.len() != 6 && s.len() != 8 {
         bail!("Invalid length for colour string")
      }

      // Colours can be #rrggbb or #aarrggbb
      let (a, offset) = if s.len() == 8 {
         (u8::from_str_radix(&s[0..2], 16), 2)
      } else {
         (Ok(255u8), 0)
      };
      let r = u8::from_str_radix(&s[0 + offset..2 + offset], 16);
      let g = u8::from_str_radix(&s[2 + offset..4 + offset], 16);
      let b = u8::from_str_radix(&s[4 + offset..6 + offset], 16);

      return Ok(Colour::from_u8(r?, g?, b?, a.unwrap()));
   }
}

impl fmt::Display for Colour {
   fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
      let x = self.to_u8();
      write!(f, "#{:02X}{:02X}{:02X}{:02X}", x.3, x.0, x.1, x.2)
   }
}

//use crate::xml::impl_attr;
//impl_attr!(Colour);

use glium::uniforms::*;
use glium::vertex::*;

impl AsUniformValue for Colour {
   fn as_uniform_value(&self) -> UniformValue {
      UniformValue::Vec4((*self).into())
   }
}

unsafe impl Attribute for Colour {
   #[inline]
   fn get_type() -> AttributeType {
      AttributeType::F32F32F32F32
   }
}

impl AsUniformValue for HSV {
   fn as_uniform_value(&self) -> UniformValue {
      UniformValue::Vec3((*self).into())
   }
}

unsafe impl Attribute for HSV {
   #[inline]
   fn get_type() -> AttributeType {
      AttributeType::F32F32F32
   }
}
