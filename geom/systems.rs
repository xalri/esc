use crate::geom::*;
use crate::geom::bvt::*;
use crate::geom::components::*;
use crate::geom::constraints::*;
use crate::storage::*;
use crate::util::hprof;
use std::mem;
use std::ptr;

/// Increments position & orientation by velocity & angular velocity respectively.
pub fn integrate_velocity(
   subscription: &mut Subscriptions,
   transform: &mut Store<Transform>,
   velocity: &mut Store<Velocity>,
) {
   let _prof = hprof::enter("integrate velocity");

   for e in subscription.iter::<(Transform, Velocity)>((transform, velocity)) {
      let t = 1.0 - velocity[e].relative_time;
      transform[e].position += velocity[e].linear * t;
      transform[e].orientation += velocity[e].angular * t;
      transform[e].orientation = wrap_angle(transform[e].orientation);
      velocity[e].relative_time = 0.0;
   }
}


/// Modifies velocity by acceleration & applies damping
pub fn integrate_acceleration(
   subscription: &mut Subscriptions,
   velocity: &mut Store<Velocity>,
   acceleration: &Store<Acceleration>,
) {
   let _prof = hprof::enter("integrate acceleration");

   for e in subscription.iter::<(Velocity, Acceleration)>((velocity, acceleration)) {
      let v = &mut velocity[e];
      let a = &acceleration[e];

      v.linear += a.linear;
      v.angular += a.angular;

      let factor = |v: f32, threshold: f32, amount: f32| -> f32 {
         if threshold == 0.0 { return 1.0 }
         1.0 - (amount * clamp((v / threshold).powf(2.0), 0.0, 1.0))
      };

      v.linear *= factor(v.linear.length(), a.linear_damping_threshold, a.linear_damping_power);
      v.angular *= factor(v.angular, a.angular_damping_threshold, a.angular_damping_power);
   }
}


/// Update the bounding volumes of all colliders
pub fn calculate_bounds(
   subscriptions: &mut Subscriptions,
   bounds: &mut Store<Bounds>,
   collider: &Store<CollisionPoly>,
   transform: &Store<Transform>,
   velocity: &Store<Velocity>,
) {
   let _prof = hprof::enter("calculate bounds");
   let query = query!(subscriptions, (CollisionPoly, Transform), (collider, transform));

   for e in query.added() {
      bounds.emplace(e);
   }

   for e in query.iter() {
      let mut b = collider[e].hull.bounds(&transform[e]);
      if velocity.has(e) {
         b = b.apply_velocity(velocity[e].linear)
      }
      bounds[e] = b;
   }
}


/// A collision broad-phase based on dynamic bounding volume trees.
#[derive(Default)]
pub struct Broadphase {
   pub tree: DynamicTree,
   pub tree_id: Store<i32>,
}

/// Cast a ray against the scene, returns the distance to the first intersection.
pub fn raycast(
   broadphase: &mut Broadphase,
   c_mask: &Store<CollisionMask>,
   c_poly: &Store<CollisionPoly>,
   transform: &Store<Transform>,
   pos: Vec2,
   dir: Vec2,
   mask: u32,
) -> f32 {
   let dir = dir.normalize();
   let mut dist = std::f32::INFINITY;
   broadphase.tree.query_ray(pos, dir, |i| {
      let i = i as usize;
      if c_mask.has(i) && c_poly.has(i) {
         if c_mask[i].id & mask != 0 {
            let m = transform[i].to_matrix();
            dist = dist.min(c_poly[i].hull.raycast(m, pos, dir));
         }
      }
      true
   });
   dist
}

/// Update the broadphase with the current entity bounds
pub fn update_broadphase(
   subscriptions: &mut Subscriptions,
   broadphase: &mut Broadphase,
   bounds: &Store<Bounds>,
   collider: &Store<CollisionMask>,
) {
   let _prof = hprof::enter("update broadphase");
   let query = query!(subscriptions, (Bounds), (bounds));

   for e in query.removed() {
      if !broadphase.tree_id.has(e) { continue }
      broadphase.tree.destroy_proxy(broadphase.tree_id[e]);
      broadphase.tree_id.remove(e);
   }

   for e in query.added() {
      assert!(!broadphase.tree_id.has(e));
      if collider[e].id != 0 {
         let id = broadphase.tree.create_proxy(bounds[e], e as i32);
         broadphase.tree_id.insert(e, id);
      }
   }

   for e in query.iter() {
      if !broadphase.tree_id.has(e) { continue }
      broadphase.tree.move_proxy(broadphase.tree_id[e], bounds[e]);
   }
}

/// A collection of contacts between bodies.
#[derive(Default)]
pub struct Collision {
   pub pairs: Vec<(usize, usize)>,
   pub contacts: Vec<Contact>,
}

/// Query the broadphase for possible collision pairs, stores them in `Collision::pairs`.
pub fn find_pairs(
   subscriptions: &mut Subscriptions,
   collision: &mut Collision,
   broadphase: &mut Broadphase,
   bounds: &Store<Bounds>,
   mask: &Store<CollisionMask>,
) {
   let _prof = hprof::enter("find collision pairs");
   collision.pairs.clear();

   // Check colliders which aren't in the tree
   for a in subscriptions.iter::<(CollisionMask,Bounds)>((mask,bounds)) {
      if mask[a].id == 0 && mask[a].mask != 0 {
         broadphase.tree.query_aabb(bounds[a], |b| {
            let b = b as usize;

            if mask[a].mask & mask[b].id != 0 {
               collision.pairs.push((a, b))
            }
            true
         })
      }
   }

   broadphase.tree.overlapping_pairs(|a, b| {
      let a = a as usize;
      let b = b as usize;
      if a == b { return }

      assert!(mask.has(a));
      assert!(mask.has(b));

      if mask[a].mask & mask[b].id != 0 {
         collision.pairs.push((a, b))
      } else if mask[b].mask & mask[a].id != 0 {
         collision.pairs.push((b, a))
      }
   });
}

/// Find all contacts between two bodies
pub fn test_contact(
   ta: &Transform,
   va: &Velocity,
   ca: &CollisionPoly,
   tb: &Transform,
   vb: &Velocity,
   cb: &CollisionPoly,
   mut callback: impl FnMut(Contact),
) {
   let mut data_a = CollisionData {
      poly: ptr::null(),
      position: ta.position,
      axis_transform: Mat2::rotate(ta.orientation) * Mat2::scale(ta.scale),
      offset: va.linear,
   };

   let mut data_b = CollisionData {
      poly: ptr::null(),
      position: tb.position,
      axis_transform: Mat2::rotate(tb.orientation) * Mat2::scale(tb.scale),
      offset: vb.linear,
   };

   for part_a in ca.hull.convex_iter() {
      for part_b in cb.hull.convex_iter() {
         data_a.poly = part_a;
         data_b.poly = part_b;
         if let Some(c) = Contact::collide(data_a, data_b) {
            callback(c);
         }
      }
   }
}

/// Performs narrow-phase collision checks to populate `Collision::contacts`.
pub fn find_contacts(
   collision: &mut Collision,
   collider: &Store<CollisionPoly>,
   transform: &Store<Transform>,
   velocity: &Store<Velocity>,
) {
   let _prof = hprof::enter("calculate contacts");
   collision.contacts.clear();
   let contacts = &mut collision.contacts;

   for &(a, b) in &collision.pairs {
      if !velocity.has(a) || !velocity.has(b) { continue; }

      test_contact(
         &transform[a], &velocity[a], &collider[a],
         &transform[b], &velocity[b], &collider[b],
         |mut c| { c.a = a; c.b = b; contacts.push(c); }
      )
   }
}


/// Add constraints which solve contacts between entities with the `RigidBody` component.
pub fn solve_rigid_body_contacts(
   constraints: &mut Constraints,
   collision: &Collision,
   body: &Store<RigidBody>
) {
   let _prof = hprof::enter("create contact constraints");
   constraints.contact.clear();

   for c in &collision.contacts {
      if !body.has(c.a) || !body.has(c.b) { continue; }

      let mut constraint = ContactConstraint::new(*c);

      constraint.friction = (body[c.a].friction * body[c.b].friction).sqrt();
      constraint.elasticity = (body[c.a].elasticity + body[c.b].elasticity) / 2.0;

      constraints.contact.push(constraint);
   }
}


/// A set of constraints to be solved
#[derive(Default)]
pub struct Constraints {
   pub contact: Vec<ContactConstraint>,
}

/// Solve the constraints.
pub fn solve_constraints(
   constraints: &mut Constraints,
   transform: &mut Store<Transform>,
   velocity: &mut Store<Velocity>,
   mass: &mut Store<Mass>
) {
   // Sort contacts by overlap/time.
   // Should help stability of stacks, items overlap more with items below
   let prof = hprof::enter("sort contacts");
   constraints.contact.sort_by(|a, b| {
      a.contact.intersection.t.partial_cmp(&b.contact.intersection.t).unwrap()
   });
   mem::drop(prof);

   let prof = hprof::enter("pre-solve");
   for contact in &mut constraints.contact {
      contact.pre_step(transform, velocity, mass);
      contact.step(velocity);
   }
   mem::drop(prof);

   let prof = hprof::enter("solve");
   for _ in 0..40 {
      for contact in &mut constraints.contact {
         contact.step(velocity);
      }
   }
   mem::drop(prof);
}

use crate::render::*;
use smallvec::*;

/// Visualise the broadphase's BVT.
pub fn debug_draw_broadphase(broad_phase: &Broadphase, lines: &mut DebugLines) {
   let mut stack = SmallVec::<[i32; 128]>::new();
   stack.push(broad_phase.tree.root);
   while let Some(id) = stack.pop() {
      if id == NULL_NODE {
         continue;
      }
      let node = &broad_phase.tree.nodes[id as usize];
      lines.bounds(&node.aabb);

      if !node.is_leaf() {
         stack.push(node.child1);
         stack.push(node.child2);
      }
   }
}
