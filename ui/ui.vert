#version 150

in vec2 pos;
in vec2 uv;
in vec4 colour;

out vec2 v_uv;
out vec4 v_colour;

uniform mat3 transform = mat3(1.0);

void main() {
   v_uv = uv;
   v_colour = colour / 255;
   gl_Position = vec4((transform * vec3(pos, 1.0)).xy, 0.0, 1.0);
}
