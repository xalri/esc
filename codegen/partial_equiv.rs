use darling::*;
use syn::*;

#[derive(FromDeriveInput)]
#[darling(attributes(partial_equiv))]
pub struct PartialEquivOpts {
   remote: String,
}

#[derive(FromField)]
#[darling(attributes(partial_equiv))]
struct FieldOpts {
   #[darling(default)]
   skip: bool,
}

pub fn derive_partial_equiv_impl(input: ::proc_macro::TokenStream) -> ::proc_macro::TokenStream {
   let ast: DeriveInput = parse(input).unwrap();

   let opts = PartialEquivOpts::from_derive_input(&ast).unwrap();
   let name = ast.ident;
   let remote: Ident = parse_str(&opts.remote).unwrap();

   match ast.data {
      Data::Struct(s) => partial_equiv_struct(s, name, remote),
      _ => unimplemented!(),
   }
}

pub fn partial_equiv_struct(s: DataStruct, name: Ident, remote: Ident) -> ::proc_macro::TokenStream {
   let from = match &s.fields {
      Fields::Named(fields) => {
         let iter = fields.named.iter().filter_map(|field| {
            let opts = FieldOpts::from_field(&field).unwrap();
            if !opts.skip {
               let name = field.ident.as_ref().unwrap();
               Some(quote! { self.#name = other.#name })
            } else {
               None
            }
         });
         quote!{ #(#iter);* }
      }
      _ => unimplemented!(),
   };

   let to = match &s.fields {
      Fields::Named(fields) => {
         let iter = fields.named.iter().map(|field| {
            /*
            let name = field.ident.as_ref().unwrap();
            quote!{ target.#name = self.#name }
            */
            let opts = FieldOpts::from_field(&field).unwrap();
            if !opts.skip {
               let name = field.ident.as_ref().unwrap();
               Some(quote! { target.#name = self.#name })
            } else {
               None
            }
         });
         quote!{ { #(#iter);* } }
      }
      _ => unimplemented!(),
   };

   quote!(
        impl PartialEquiv for #name {
           type Target = #remote;
           fn partial_to(&self, target: &mut Self::Target) {
              #to
           }
           fn partial_from(&mut self, other: &Self::Target) {
               #from
           }
        }
     )
   .into()
}
