#version 150

in vec2 pos;
in vec2 uv;

out vec2 v_uv;

uniform mat3 transform = mat3(1.0);
uniform mat3 tx_transform = mat3(1.0);
uniform uint depth = 0u;
uniform vec2 parallax = vec2(1.0);

void main() {
   v_uv = (tx_transform * vec3(uv, 1.0)).xy;
   vec2 inv_p = 1.0 / parallax;
   mat3 p = mat3(inv_p.x, 0, 0,  0, inv_p.y, 0, 0, 0, 1.0);
   float z = float(depth) / 16777215.0;
   gl_Position = vec4((p * transform * vec3(pos, 1.0)).xy, z, 1.0);
}
