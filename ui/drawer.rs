// Based on https://github.com/snuk182/nuklear-backend-glium.

// Copyright (c) 2015 The nuklear-rust Developers
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

use crate::geom::*;
use crate::render::*;
use glium::*;
use nuklear::{
   Buffer, Context, ConvertConfig, DrawVertexLayoutAttribute, DrawVertexLayoutElements, DrawVertexLayoutFormat, Handle,
};
use std::ops::Deref;

#[derive(Clone, Copy, Default)]
struct Vertex {
   pos: Vec2,
   uv: Vec2,
   colour: [u8; 4]
}

implement_vertex!(Vertex, pos, uv, colour);

pub struct Drawer {
   cmd: Buffer,
   tex: Vec<glium::Texture2d>,
   vbf: Vec<Vertex>,
   ebf: Vec<u16>,
   vbo: glium::VertexBuffer<Vertex>,
   ebo: glium::IndexBuffer<u16>,
   vle: DrawVertexLayoutElements,
}

impl Drawer {
   pub fn new(
      display: &mut glium::Display,
      texture_count: usize,
      vbo_size: usize,
      ebo_size: usize,
      command_buffer: Buffer,
   ) -> Drawer {
      Drawer {
         cmd: command_buffer,
         tex: Vec::with_capacity(texture_count + 1),
         vbf: vec![Vertex::default(); vbo_size * ::std::mem::size_of::<Vertex>()],
         ebf: vec![0u16; ebo_size * ::std::mem::size_of::<u16>()],
         vbo: glium::VertexBuffer::empty_dynamic(display, vbo_size * ::std::mem::size_of::<Vertex>()).unwrap(),
         ebo: glium::IndexBuffer::empty_dynamic(
            display,
            glium::index::PrimitiveType::TrianglesList,
            ebo_size * ::std::mem::size_of::<u16>(),
         )
         .unwrap(),
         vle: DrawVertexLayoutElements::new(&[
            (
               DrawVertexLayoutAttribute::Position,
               DrawVertexLayoutFormat::Float,
               0,
            ),
            (
               DrawVertexLayoutAttribute::TexCoord,
               DrawVertexLayoutFormat::Float,
               8,
            ),
            (
               DrawVertexLayoutAttribute::Color,
               DrawVertexLayoutFormat::R8G8B8A8,
               16,
            ),
            (
               DrawVertexLayoutAttribute::AttributeCount,
               DrawVertexLayoutFormat::Count,
               32
            ),
         ]),
      }
   }

   pub fn add_texture(&mut self, display: &mut glium::Display, image: &[u8], width: u32, height: u32) -> Handle {
      let image = glium::texture::RawImage2d {
         data: std::borrow::Cow::Borrowed(image),
         width,
         height,
         format: glium::texture::ClientFormat::U8U8U8U8,
      };
      let tex = glium::Texture2d::new(display, image).unwrap();
      let hnd = Handle::from_id(self.tex.len() as i32 + 1);
      self.tex.push(tex);
      hnd
   }

   pub fn draw(
      &mut self,
      program: &UIShader,
      view: Mat3,
      draw_params: DrawParameters,
      ctx: &mut Context,
      cfg: &mut ConvertConfig,
      frame: &mut impl glium::Surface,
   ) {
      let draw_params = DrawParameters {
         blend: Blend::alpha_blending(),
         backface_culling: glium::draw_parameters::BackfaceCullingMode::CullingDisabled,
         ..draw_params.clone()
      };

      cfg.set_vertex_layout(&self.vle);
      cfg.set_vertex_size(::std::mem::size_of::<Vertex>());

      {
         self.vbo.invalidate();
         self.ebo.invalidate();

         let mut rvbuf = unsafe {
            ::std::slice::from_raw_parts_mut(self.vbf.as_mut() as *mut [Vertex] as *mut u8, self.vbf.capacity())
         };
         let mut rebuf = unsafe {
            ::std::slice::from_raw_parts_mut(self.ebf.as_mut() as *mut [u16] as *mut u8, self.ebf.capacity())
         };
         let mut vbuf = Buffer::with_fixed(&mut rvbuf);
         let mut ebuf = Buffer::with_fixed(&mut rebuf);

         ctx.convert(&mut self.cmd, &mut vbuf, &mut ebuf, &cfg);

         self.vbo.slice_mut(0..self.vbf.capacity()).unwrap().write(&self.vbf);
         self.ebo.slice_mut(0..self.ebf.capacity()).unwrap().write(&self.ebf);
      }

      let mut idx_start = 0;
      let mut idx_end;

      for cmd in ctx.draw_command_iterator(&self.cmd) {
         if cmd.elem_count() < 1 {
            continue;
         }

         let id = cmd.texture().id().unwrap();
         let ptr = self.find_res(id).unwrap();

         idx_end = idx_start + cmd.elem_count() as usize;

         frame
            .draw(
               &self.vbo,
               &self.ebo.slice(idx_start..idx_end).unwrap(),
               &program.0,
               &uniform! { transform: view, tx: ptr.sampled() },
               &draw_params
            )
            .unwrap();
         idx_start = idx_end;
      }
   }

   fn find_res(&self, id: i32) -> Option<&glium::Texture2d> {
      if id > 0 && id as usize <= self.tex.len() {
         Some(&self.tex[(id - 1) as usize])
      } else {
         None
      }
   }
}

/// Shader for nuklear UI
pub struct UIShader(pub Program);

impl Default for UIShader {
   fn default() -> Self {
      UIShader(ProgramSource {
         vertex_shader: include_str!("./ui.vert"),
         fragment_shader: include_str!("./ui.frag"),
         geometry_shader: None,
      }.load().unwrap())
   }
}

impl Deref for UIShader {
   type Target = Program;
   fn deref(&self) -> &Program { &self.0 }
}
