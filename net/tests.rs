extern crate net_emu;

use self::net_emu::*;
use crate::net::connection::*;
use crate::net::guarantee::*;
use std::mem;
use std::time::*;

type Message = Guaranteed<i32>;

fn test_msg(value: i32) -> Message {
   Guaranteed {
      addr: ([0, 0, 0, 0], 0).into(),
      guarantee: GuaranteeLevel::Order,
      guarantee_data: None,
      data: Some(value),
   }
}

struct TestConnection(Connection<i32>);

impl Endpoint<Message, Message> for TestConnection {
   fn send(&mut self, m: Message, time: u64) -> Option<Message> {
      let mut now: Instant = unsafe { mem::zeroed() };
      now += Duration::from_millis(time);
      Some(self.0.send(m, now).clone())
   }
   fn recv(&mut self, m: Message, time: u64) -> (Vec<Message>, Vec<Message>) {
      let mut now: Instant = unsafe { mem::zeroed() };
      now += Duration::from_millis(time);

      let mut disp = Vec::new();
      let mut snd = Vec::new();

      self.0.receive(m, now, |msg| disp.push(msg), |msg| snd.push(msg));
      (snd, disp)
   }
   fn update(&mut self, time: u64) -> Vec<Message> {
      let mut now: Instant = unsafe { mem::zeroed() };
      now += Duration::from_millis(time);
      let mut ret = Vec::new();
      self.0.update(now, |msg| ret.push(msg.clone()));
      ret
   }
}

#[test]
fn normal_seq() {
   const MESSAGES: i32 = 10_000;

   let now: Instant = unsafe { mem::zeroed() };

   let emu_conf = EmuParams {
      latency: 100.0,
      latency_deviation: 10.0,
      packet_loss: 0.3,
      packet_dup: 0.3,
   };

   let mut emu = NetEmu::new(
      emu_conf,
      TestConnection(Connection::new(now)),
      TestConnection(Connection::new(now)),
   );

   for i in 1..MESSAGES {
      emu.send_to_b(test_msg(i));
      emu.run_ms(10);
   }
   emu.run();

   assert_eq!(
      (1..MESSAGES).collect::<Vec<_>>(),
      emu.recv_b().into_iter().map(|m| m.data.unwrap()).collect::<Vec<_>>()
   );
}
