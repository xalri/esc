use super::*;
use std::ops::*;
use serde::*;

/// A 3x3 column major float matrix
#[repr(C)]
#[derive(Clone, Copy, Debug, PartialEq)]
#[derive(Serialize, Deserialize)]
pub struct Mat3 {
   pub m00: f32,
   pub m01: f32,
   pub m02: f32,
   pub m10: f32,
   pub m11: f32,
   pub m12: f32,
   pub m20: f32,
   pub m21: f32,
   pub m22: f32,
}

#[repr(C)]
#[derive(Clone, Copy, Debug, PartialEq)]
#[derive(Serialize, Deserialize)]
pub struct Mat2 {
   pub m00: f32,
   pub m01: f32,
   pub m10: f32,
   pub m11: f32,
}

impl Mat2 {
   pub fn id() -> Mat2 {
      mat2(1.0, 0.0, 0.0, 1.0)
   }

   /// Create a transformation matrix which rotates a point by the given angle in radians.
   pub fn rotate(angle: f32) -> Mat2 {
      debug_assert!(angle.is_finite());
      if !angle.is_finite() { return Mat2::id() }
      let s = angle.sin();
      let c = angle.cos();
      mat2(c, s, -s, c)
   }

   /// Create a transformation matrix which scales a point.
   pub fn scale(scale: Vec2) -> Mat2 {
      mat2(scale.x, 0.0, 0.0, scale.y)
   }

   /// Transpose the matrix
   pub fn transpose(self) -> Mat2 {
      mat2(self.m00, self.m10, self.m01, self.m11)
   }

   /// Check if the components are valid finite floats.
   pub fn valid(&self) -> bool {
      self.m00.is_finite() && self.m00.abs() < 100_000_000.0 &&
      self.m01.is_finite() && self.m01.abs() < 100_000_000.0 &&
      self.m10.is_finite() && self.m10.abs() < 100_000_000.0 &&
      self.m11.is_finite() && self.m11.abs() < 100_000_000.0
   }
}

impl Mul<Mat2> for Mat2 {
   type Output = Mat2;
   fn mul(self, a: Mat2) -> Mat2 {
      let b = self;
      mat2(
         a.m00 * b.m00 + a.m01 * b.m10,
         a.m00 * b.m01 + a.m01 * b.m11,
         a.m10 * b.m00 + a.m11 * b.m10,
         a.m10 * b.m01 + a.m11 * b.m11,
      )
   }
}

impl Mul<Vec2> for Mat2 {
   type Output = Vec2;
   fn mul(self, v: Vec2) -> Vec2 {
      vec2(self.m00 * v.x + self.m10 * v.y, self.m01 * v.x + self.m11 * v.y)
   }
}

pub fn mat2(m00: f32, m01: f32, m10: f32, m11: f32) -> Mat2 {
   Mat2 { m00, m01, m10, m11 }
}

pub fn mat3(m00: f32, m01: f32, m02: f32, m10: f32, m11: f32, m12: f32, m20: f32, m21: f32, m22: f32) -> Mat3 {
   Mat3 {
      m00,
      m01,
      m02,
      m10,
      m11,
      m12,
      m20,
      m21,
      m22,
   }
}

impl Mat3 {
   /// The identity matrix.
   pub fn id() -> Mat3 {
      mat3(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0)
   }

   /// Create a translation matrix.
   pub fn translate(v: Vec2) -> Mat3 {
      mat3(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, v.x, v.y, 1.0)
   }

   /// Create a transformation matrix to flip a point along the axes.
   pub fn flip(h: bool, v: bool) -> Mat3 {
      let x = if h { -1.0 } else { 1.0 };
      let y = if v { -1.0 } else { 1.0 };
      mat3(x, 0.0, 0.0, 0.0, y, 0.0, 0.0, 0.0, 1.0)
   }

   /// Create a transformation matrix to flip along the x axes.
   pub fn flip_x() -> Mat3 {
      mat3(-1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0)
   }

   /// Create a transformation matrix to flip along the y axes.
   pub fn flip_y() -> Mat3 {
      mat3(1.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, 1.0)
   }

   /// Create a transformation matrix which scales a point.
   pub fn scale(scale: Vec2) -> Mat3 {
      mat3(scale.x, 0.0, 0.0, 0.0, scale.y, 0.0, 0.0, 0.0, 1.0)
   }

   /// Create a transformation matrix which scales a point.
   pub fn scale_uniform(scale: f32) -> Mat3 {
      mat3(scale, 0.0, 0.0, 0.0, scale, 0.0, 0.0, 0.0, 1.0)
   }

   /// A combined transformation
   pub fn transform(translate: Vec2, angle: f32, scale: Vec2) -> Mat3 {
      let s = angle.sin();
      let c = angle.cos();
      mat3(
         c * scale.x,
         s * scale.x,
         0.0,
         -s * scale.y,
         c * scale.y,
         0.0,
         translate.x,
         translate.y,
         1.0,
      )
   }

   /// Decompose a transformation matrix into a translation, rotation, and scale.
   pub fn decompose(&self) -> (Vec2, f32, Vec2) {
      let pos = vec2(self.m20, self.m21);
      let scale = vec2(
         (self.m00 * self.m00 + self.m01 * self.m01).sqrt() * self.m00.signum(),
         (self.m10 * self.m10 + self.m11 * self.m11).sqrt() * self.m11.signum(),
      );
      let angle = (self.m01 / scale.x).atan2(self.m00 / scale.x);
      (pos, angle, scale)
   }

   /// Create a view matrix from world bounds
   pub fn view(bounds: Bounds) -> Mat3 {
      Mat3::translate(vec2(-1.0, -1.0))
         * Mat3::scale(vec2(2.0 / bounds.x.width(), 2.0 / bounds.y.width()))
         * Mat3::translate(vec2(-bounds.x.min, -bounds.y.min))
   }

   /// Create a transformation matrix which rotates a point by the given angle
   /// in radians.
   pub fn rotate(angle: f32) -> Mat3 {
      let s = angle.sin();
      let c = angle.cos();
      mat3(c, s, 0.0, -s, c, 0.0, 0.0, 0.0, 1.0)
   }

   /// Create a rotation matrix about a point
   pub fn rotate_about(angle: f32, pos: Vec2) -> Mat3 {
      Mat3::translate(pos) * Mat3::rotate(angle) * Mat3::translate(-pos)
   }

   /// Calculate the determinant
   pub fn determinant(self) -> f32 {
      self.m00 * (self.m11 * self.m22 - self.m12 * self.m21)
         + self.m10 * (self.m21 * self.m02 - self.m01 * self.m22)
         + self.m20 * (self.m01 * self.m12 - self.m11 * self.m02)
   }

   /// Transpose the matrix
   pub fn transpose(self) -> Mat3 {
      mat3(
         self.m00, self.m10, self.m20, self.m01, self.m11, self.m21, self.m02, self.m12, self.m22,
      )
   }

   /// Get the matrix's inverse
   pub fn invert(self) -> Mat3 {
      let det = self.determinant();
      if det == 0.0 {
         panic!("Matrix::invert on non-invertible matrix");
      }
      let x = 1.0 / det;
      mat3(
         x * (self.m11 * self.m22 - self.m12 * self.m21),
         x * (self.m21 * self.m02 - self.m01 * self.m22),
         x * (self.m01 * self.m12 - self.m11 * self.m02),
         x * (self.m20 * self.m12 - self.m10 * self.m22),
         x * (self.m00 * self.m22 - self.m20 * self.m02),
         x * (self.m10 * self.m02 - self.m00 * self.m12),
         x * (self.m10 * self.m21 - self.m20 * self.m11),
         x * (self.m20 * self.m01 - self.m00 * self.m21),
         x * (self.m00 * self.m11 - self.m10 * self.m01),
      )
   }

   /// Multiply by some vector, ignoring translation.
   pub fn apply_vec(self, v: Vec2) -> Vec2 {
      vec2(self.m00 * v.x + self.m10 * v.y, self.m01 * v.x + self.m11 * v.y)
   }

   /// Convert to an array
   pub fn to_arr(self) -> [[f32; 3]; 3] {
      unsafe { ::std::mem::transmute(self) }
   }
}

impl Mul<Mat3> for Mat3 {
   type Output = Mat3;
   fn mul(self, a: Mat3) -> Mat3 {
      let b = self;
      mat3(
         a.m00 * b.m00 + a.m01 * b.m10 + a.m02 * b.m20,
         a.m00 * b.m01 + a.m01 * b.m11 + a.m02 * b.m21,
         a.m00 * b.m02 + a.m01 * b.m12 + a.m02 * b.m22,
         a.m10 * b.m00 + a.m11 * b.m10 + a.m12 * b.m20,
         a.m10 * b.m01 + a.m11 * b.m11 + a.m12 * b.m21,
         a.m10 * b.m02 + a.m11 * b.m12 + a.m12 * b.m22,
         a.m20 * b.m00 + a.m21 * b.m10 + a.m22 * b.m20,
         a.m20 * b.m01 + a.m21 * b.m11 + a.m22 * b.m21,
         a.m20 * b.m02 + a.m21 * b.m12 + a.m22 * b.m22,
      )
   }
}

impl MulAssign<Mat3> for Mat3 {
   fn mul_assign(&mut self, rhs: Mat3) {
      *self = *self * rhs;
   }
}

impl Mul<Vec2> for Mat3 {
   type Output = Vec2;
   fn mul(self, v: Vec2) -> Vec2 {
      vec2(
         self.m00 * v.x + self.m10 * v.y + self.m20,
         self.m01 * v.x + self.m11 * v.y + self.m21,
      )
   }
}

// ========================================================================
// -------------------------------- Tests ---------------------------------
// ========================================================================

#[cfg(test)]
use approx::*;
#[cfg(test)]
use std::f32::consts::PI;

#[test]
fn test_id() {
   let point = vec2(1.0, 2.0);
   assert_relative_eq!(point, Mat3::id() * point);
}

#[test]
fn test_transform() {
   Mat3::transform(vec2(5.0, -5.0), 150.0, vec2(-1.0, 2.5));
}

#[test]
fn test_translate() {
   let point = vec2(1.0, 2.0);
   assert_relative_eq!(vec2(2.0, 4.0), Mat3::translate(vec2(1.0, 2.0)) * point);
   assert_relative_eq!(vec2(1.0, 2.0), Mat3::translate(vec2(0.0, 0.0)) * point);
}

#[test]
fn test_flip() {
   let point = vec2(1.0, 2.0);
   assert_relative_eq!(vec2(1.0, 2.0), Mat3::flip(false, false) * point);
   assert_relative_eq!(vec2(-1.0, 2.0), Mat3::flip(true, false) * point);
   assert_relative_eq!(vec2(1.0, -2.0), Mat3::flip(false, true) * point);
   assert_relative_eq!(vec2(-1.0, -2.0), Mat3::flip(true, true) * point);
}

#[test]
fn test_scale() {
   let point = vec2(1.0, 2.0);
   assert_relative_eq!(vec2(1.0, 2.0), Mat3::scale_uniform(1.0) * point);
   assert_relative_eq!(vec2(2.0, 4.0), Mat3::scale_uniform(2.0) * point);
   assert_relative_eq!(vec2(-1.0, -2.0), Mat3::scale_uniform(-1.0) * point);
   assert_relative_eq!(vec2(-2.0, -4.0), Mat3::scale_uniform(-2.0) * point);
}

#[test]
fn test_rotate() {
   let point = vec2(1.0, 2.0);
   assert_relative_eq!(vec2(2.0, -1.0), Mat3::rotate(-PI / 2.0) * point);
   assert_relative_eq!(vec2(-2.0, 1.0), Mat3::rotate(PI / 2.0) * point);
}

#[test]
fn test_decompose() {
   let pos = vec2(1.0, -10.0);
   let angle = 0.1;
   let scale = vec2(2.0, -1.5);

   let m = Mat3::transform(pos, angle, scale).decompose();
   assert_relative_eq!(m.0, pos);
   assert_relative_eq!(m.1, angle);
   assert_relative_eq!(m.2, scale);
}
