#version 150

in vec2 v_uv;
in vec4 v_colour;

out vec4 fragColor;

uniform sampler2D tx;

void main() {
  fragColor = texture(tx, v_uv) * v_colour;
}
