use std::any;
use std::mem;
use std::ptr;

/// A map which holds one item of each type. All types must have the same size as
/// the given placeholder type.
pub struct AnyMap<Placeholder> {
   map: Vec<Entry<Placeholder>>,
}

/// Holds a value of any type with the same size as some placeholder.
struct Entry<Placeholder> {
   id: any::TypeId,
   item: mem::ManuallyDrop<Placeholder>,
   destructor: Box<dyn Fn(&mut Placeholder) + Send + Sync>,
}

impl<P> Entry<P> {
   fn new<T: 'static>(item: T) -> Entry<P> {
      // Must have the same size as the placeholder.
      assert_eq!(mem::size_of::<T>(), mem::size_of::<P>());

      let e = Entry {
         id: any::TypeId::of::<T>(),
         // Assertion above ensures size is correct, item is forgotten below.
         item: mem::ManuallyDrop::new(unsafe { mem::transmute_copy::<T, P>(&item) }),
         destructor: Box::new(|item| unsafe {
            let item = mem::transmute::<&mut P, &mut T>(&mut *item);
            ptr::drop_in_place::<T>(item);
         }),
      };
      mem::forget(item);
      e
   }

   unsafe fn get_mut<T>(&mut self) -> &mut T {
      mem::transmute::<&mut P, &mut T>(&mut self.item)
   }
}

impl<P> Drop for Entry<P> {
   fn drop(&mut self) {
      // destructor casts to correct type & drops in place
      (self.destructor)(&mut self.item);
   }
}

impl<P> AnyMap<P> {
   /// Create a new `AnyMap`.
   pub fn new() -> AnyMap<P> {
      AnyMap { map: vec![] }
   }

   /// Create a new `AnyMap` with the given capacity.
   pub fn with_capacity(capacity: usize) -> AnyMap<P> {
      AnyMap {
         map: Vec::with_capacity(capacity),
      }
   }

   /// Get the number of elements in the map
   pub fn len(&self) -> usize {
      self.map.len()
   }

   /// Get the item of the specified type
   pub fn get<T: 'static>(&mut self) -> Option<&mut T> {
      for entry in &mut self.map {
         if entry.id == any::TypeId::of::<T>() {
            return Some(unsafe { entry.get_mut() });
         }
      }
      None
   }

   /// Check if an item of the given type is in the map
   pub fn has<T: 'static>(&mut self) -> bool {
      self.get::<T>().is_some()
   }

   /// Get an item or insert it's default value
   pub fn get_or_default<T: Default + 'static>(&mut self) -> &mut T {
      if !self.has::<T>() { self.insert::<T>(T::default()); }
      self.get::<T>().unwrap()
   }

   /// Insert an item, without checking if an existing item of the same type already exists.
   /// (if one does, it will be returned to any calls to `get`, rather than the item given)
   pub fn insert<T: 'static>(&mut self, item: T) -> &mut T {
      self.map.push(Entry::new(item));
      unsafe { self.map.last_mut().unwrap().get_mut() }
   }
}

impl<P> Default for AnyMap<P> {
   fn default() -> Self { AnyMap { map: vec![] } }
}
