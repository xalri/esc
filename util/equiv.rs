pub use esc_codegen::*;

/// A pair of types which can be converted between.
pub trait Equiv {
   /// The name of the remote type.
   type Remote;

   /// Create an instance of this type from the remote type.
   fn from(v: Self::Remote) -> Self;

   /// Create an instance of the remote type from this value.
   fn into(self) -> Self::Remote;
}

/// A struct which contains a subset of another type's fields
pub trait PartialEquiv {
   type Target;
   fn partial_from(&mut self, target: &Self::Target);
   fn partial_to(&self, target: &mut Self::Target);
}
