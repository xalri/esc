use glium::program::*;

use crate::error::*;
use crate::render::facade;

/// Given an empty VBO, generates a triangle covering the screen.
pub const FULL_SCREEN_VERT: &str = include_str!("../glsl/full_screen_tri.vert");

/// The source of a shader program
#[derive(Copy, Clone)]
pub struct ProgramSource {
   pub vertex_shader: *const str,
   pub fragment_shader: *const str,
   pub geometry_shader: Option<*const str>,
}

impl ProgramSource {
   pub fn load(&self) -> Result<Program> {
      let input = unsafe {
         ProgramCreationInput::SourceCode {
            vertex_shader: &*self.vertex_shader,
            fragment_shader: &*self.fragment_shader,
            geometry_shader: self.geometry_shader.map(|ptr| &*ptr),
            tessellation_control_shader: None,
            tessellation_evaluation_shader: None,
            transform_feedback_varyings: None,
            outputs_srgb: true,
            uses_point_size: false,
         }
      };

      Ok(Program::new(facade(), input)?)
   }
}

/// A GLSL Scalar type, used for pre-processing
#[derive(Clone, Copy)]
pub enum GlslScalarType {
   Bool,
   UInt,
   Int,
   Float,
   Double,
}

impl GlslScalarType {
   pub fn name(&self) -> &'static str {
      use self::GlslScalarType::*;
      match *self {
         Bool => "bool",
         UInt => "uint",
         Int => "int",
         Float => "float",
         Double => "double",
      }
   }

   pub fn prefix(&self) -> &'static str {
      use self::GlslScalarType::*;
      match *self {
         Bool => "b",
         UInt => "u",
         Int => "i",
         Float => "f",
         Double => "d",
      }
   }
}

pub trait GlslScalar {
   fn scalar_kind() -> GlslScalarType;
}

impl GlslScalar for i32 {
   fn scalar_kind() -> GlslScalarType { GlslScalarType::Int }
}

impl GlslScalar for u32 {
   fn scalar_kind() -> GlslScalarType { GlslScalarType::UInt }
}

impl GlslScalar for f32 {
   fn scalar_kind() -> GlslScalarType { GlslScalarType::Float }
}

impl GlslScalar for f64 {
   fn scalar_kind() -> GlslScalarType { GlslScalarType::Double }
}

impl GlslScalar for bool {
   fn scalar_kind() -> GlslScalarType { GlslScalarType::Bool }
}

