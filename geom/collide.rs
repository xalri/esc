//! Swept collision detection & impulse based response for 2D bodies

use super::math::*;
use super::math::Range;
use super::poly::*;
use std::f32;

/// The maximum perpendicular distance before a collision is considered to be supported
/// by an edge rather than a vertex.
pub const SUPPORT_EDGE_THRESHOLD: f32 = 0.5;
pub const SUPPORT_EDGE_WIDTH: f32 = 7.0;

/// Collision manifold.
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Contact {
   pub a: usize,
   pub b: usize,
   pub poly_a: *const ConvexPoly,
   pub poly_b: *const ConvexPoly,
   pub intersection: Intersection,
   /// True if both contacts are along an edge (-> the bodies are flush)
   pub flush: bool,
   /// Representative contact point for body A
   pub contact_a: Vec2,
   /// Representative contact point for body B
   pub contact_b: Vec2,
   /// Identifies the feature of body A which is colliding.
   pub feature_a: u32,
   /// Identifies the feature of body B which is colliding.
   pub feature_b: u32,
}

/// An intersection between two bodies
#[derive(Debug, Clone, Copy, PartialEq, Default)]
pub struct Intersection {
   pub normal: Vec2,
   /// The time to collision. A negative value represents the distance
   /// of an existing intersection.
   pub t: f32,
}

/// A supporting point or edge
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Support {
   id: u32,
   min: Vec2,
   max: Vec2,
   min_dist: f32,
   max_dist: f32,
   is_point: bool,
}

/// The input to a collision test
#[derive(Debug, Clone, Copy)]
pub struct CollisionData {
   pub poly: *const ConvexPoly,
   pub position: Vec2,
   pub offset: Vec2,
   pub axis_transform: Mat2,
}

impl Contact {
   /// Calculate the intersection & contact points for a collision between two bodies.
   /// Returns `None` if the bodies will not collide in the next time step.
   pub fn collide(a: CollisionData, b: CollisionData) -> Option<Contact> {
      let intersection = Intersection::test(a, b);

      if intersection.is_none() { return None; }
      let intersection = intersection.unwrap();

      // Calculate manifolds
      let support_a = Support::new(a, intersection);
      let support_b = Support::new(b, intersection.invert_normal()).inv();

      // Calculate contact points from supports.
      if support_a.is_point && support_b.is_point {
         Some(Contact {
            a: 0,
            b: 0,
            poly_a: a.poly,
            poly_b: b.poly,
            intersection,
            flush: false,
            contact_a: support_a.min,
            contact_b: support_b.min,
            feature_a: support_a.id,
            feature_b: support_b.id,
         })
      } else {
         let range_a = support_a.range();
         let range_b = support_b.range();

         if !range_a.intersects(range_b) {
            return None;
         }

         let mut ca;
         let mut cb;

         if range_a.min > range_b.min {
            ca = support_a.min;
            cb = project_on_segment(support_a.min, (support_b.min, support_b.max));
         } else {
            ca = project_on_segment(support_b.min, (support_a.min, support_a.max));
            cb = support_b.min;
         }

         if range_a.max < range_b.max {
            ca += support_a.max;
            cb += project_on_segment(support_a.max, (support_b.min, support_b.max));
         } else {
            ca += project_on_segment(support_b.max, (support_a.min, support_a.max));
            cb += support_b.max;
         }
         ca /= 2.0;
         cb /= 2.0;

         debug_assert!(ca.valid());
         debug_assert!(cb.valid());

         Some(Contact {
            a: 0,
            b: 0,
            poly_a: a.poly,
            poly_b: b.poly,
            intersection,
            flush: !support_a.is_point && !support_b.is_point,
            contact_a: ca,
            contact_b: cb,
            feature_a: support_a.id,
            feature_b: support_b.id,
         })
      }
   }
}

impl Support {
   /// Find the world-space support points for a collided body
   fn new(body: CollisionData, intersection: Intersection) -> Support {
      let mut support = Support {
         min: vec2(0.0, 0.0),
         max: vec2(0.0, 0.0),
         min_dist: f32::MAX,
         max_dist: f32::MIN,
         is_point: true,
         id: 0,
      };

      let normal = body.axis_transform.transpose() * intersection.normal;
      let tangent = vec2(-intersection.normal.y, intersection.normal.x);
      let pos = body.position + body.offset * f32::max(0.0, intersection.t);
      debug_assert!(pos.valid());

      let mut closest = f32::MAX;
      for &p in unsafe { &*body.poly }.iter() {
         let dist = p.dot(normal);
         if dist < closest {
            closest = dist;
         }
      }

      let mut candidates = 0;
      let mut min_idx = 0;
      let mut max_idx = 0;

      for (idx, &p) in unsafe { &*body.poly }.iter().enumerate() {
         if p.dot(normal) < closest + SUPPORT_EDGE_THRESHOLD {
            candidates += 1;

            let world_space = pos + body.axis_transform * p;
            let perp_dist = world_space.dot(tangent);

            if perp_dist < support.min_dist {
               support.min = world_space;
               support.min_dist = perp_dist;
               min_idx = idx as u32;
            }
            if perp_dist > support.max_dist {
               support.max = world_space;
               support.max_dist = perp_dist;
               max_idx = idx as u32;
            }
         }
      }

      support.is_point = candidates == 1 || (support.max_dist - support.min_dist < SUPPORT_EDGE_WIDTH);

      // Build some unique identifier for the feature of the body which is colliding.
      support.id = if support.is_point {
         min_idx << 1
      } else {
         1 | (min_idx + (max_idx * unsafe { (&*body.poly).len() as u32 }))
      };

      assert!(support.min.valid());
      assert!(support.max.valid());
      support
   }

   /// Get the contact's range along the perpendicular vector to the collision normal.
   fn range(&self) -> Range {
      Range {
         min: self.min_dist,
         max: self.max_dist,
      }
   }

   fn inv(self) -> Support {
      Support {
         id: self.id,
         min: self.max,
         max: self.min,
         min_dist: -self.max_dist,
         max_dist: -self.min_dist,
         is_point: self.is_point,
      }
   }
}

impl Intersection {
   /// Check for an intersection between two convex bodies.
   pub fn test(body_a: CollisionData, body_b: CollisionData) -> Option<Intersection> {
      let hull_a = unsafe { &*body_a.poly };
      let hull_b = unsafe { &*body_b.poly };

      let oa = body_a.axis_transform;
      let ob = body_b.axis_transform;

      debug_assert!(oa.valid());
      debug_assert!(ob.valid());

      let offset = body_a.position - body_b.position;
      let velocity = body_a.offset - body_b.offset;

      let mut collision = Intersection {
         normal: vec2(0.0, 0.0),
         t: -f32::MIN_POSITIVE,
      };
      let mut overlap = Intersection {
         normal: vec2(0.0, 0.0),
         t: -f32::MAX,
      };

      // Test for collision against a given axis
      let mut test = |normal: Vec2| -> bool {
         debug_assert!(normal.valid());
         let i = Intersection::axis_intersect(normal, hull_a, hull_b, oa, ob, offset, velocity, 1.0);
         if i.is_none() {
            return false;
         }
         let i = i.unwrap();
         if i.t > collision.t {
            collision = i
         } else if i.t > overlap.t {
            overlap = i
         }
         true
      };

      // Check possible axes, returns `None` if the bodies are separated
      // along any axis.
      for &normal in &hull_a.separating_axes {
         if !test(oa * normal) {
            return None;
         }
      }
      for &normal in &hull_b.separating_axes {
         if !test(ob * normal) {
            return None;
         }
      }

      // Bodies intersect on all axes -- take the minimum translation
      let mut min = if collision.t >= 0.0 { collision } else { overlap };
      let center_a = oa * hull_a.centroid + body_a.position;
      let center_b = ob * hull_b.centroid + body_b.position;
      if min.normal.dot(center_a - center_b) < 0.0 {
         min.normal = vec2(-min.normal.x, -min.normal.y);
      }

      Some(min)
   }

   /// Calculate the intersection of two polygons along an axis
   pub fn axis_intersect(
      axis: Vec2,
      hull_a: &ConvexPoly,
      hull_b: &ConvexPoly,
      transform_a: Mat2,
      transform_b: Mat2,
      offset: Vec2,
      velocity: Vec2,
      max_time: f32,
   ) -> Option<Intersection> {
      assert!(axis.length() > 0.000001);
      let axis = axis.normalize();

      // Project each hull onto the axis.
      let mut projection_a = hull_a.project(axis, transform_a);
      let projection_b = hull_b.project(axis, transform_b);

      // Adjust by the relative offset
      projection_a += offset.dot(axis);

      let d0 = projection_a.min - projection_b.max;
      let d1 = projection_b.min - projection_a.max;

      if d0 >= 0.0 || d1 >= 0.0 {
         // Calculate the time in frames to the collision
         let v = velocity.dot(axis);
         if v == 0.0 {
            return None;
         }
         if let Some(time) = smallest_positive(-d0 / v, d1 / v) {
            if time < max_time {
               return Some(Intersection { t: time, normal: axis });
            }
         }
         None
      } else {
         Some(Intersection {
            // Overlap distance.
            t: f32::max(d0, d1),
            normal: axis,
         })
      }
   }

   pub fn invert_normal(&self) -> Intersection {
      Intersection {
         t: self.t,
         normal: -self.normal,
      }
   }
}

fn smallest_positive(a: f32, b: f32) -> Option<f32> {
   if a >= 0.0 && b >= 0.0 {
      Some(f32::min(a, b))
   } else if a >= 0.0 {
      Some(a)
   } else if b >= 0.0 {
      Some(b)
   } else {
      None
   }
}

#[test]
fn test_intersection() {
   let poly = Poly::rect(20.0, 20.0, false);
   let body_a = CollisionData {
      poly: &*poly,
      position: vec2(10.0, 0.0),
      offset: vec2(0.0, 0.0),
      axis_transform: Mat2::rotate(0.0),
   };

   let body_b = CollisionData {
      poly: &*poly,
      position: vec2(0.0, 0.0),
      offset: vec2(0.0, 0.0),
      axis_transform: Mat2::rotate(0.0),
   };

   let intersection = Intersection::test(body_a, body_b);
   assert_eq!(
      Some(Intersection {
         normal: vec2(1.0, 0.0),
         t: -10.0
      }),
      intersection
   );
}

#[test]
fn test_axis_intersect() {
   let poly_a = Poly::new(&[vec2(0.0, 0.0), vec2(2.0, -2.0), vec2(-2.0, -2.0)]);

   let poly_b = Poly::new(&[vec2(2.0, 2.0), vec2(2.0, -2.0), vec2(-2.0, -2.0), vec2(-2.0, 2.0)]);

   assert!(
      Intersection::axis_intersect(
         vec2(1.0, 0.0),
         &*poly_a,
         &*poly_b,
         Mat2::scale(vec2(-1.0, -1.0)),
         Mat2::id(),
         vec2(-1.0, 1.0),
         vec2(0.0, 0.0),
         1.0
      )
      .is_some()
   );

   assert_eq!(
      None,
      Intersection::axis_intersect(
         vec2(1.0, 0.0),
         &*poly_a,
         &*poly_b,
         Mat2::rotate(-45.0),
         Mat2::id(),
         vec2(-4.2426405, 2.828427),
         vec2(0.0, 0.0),
         1.0
      )
   );
}

#[test]
fn negative_segment_contact() {
   let poly = Poly::ellipse(7, 20.0, 10.0);
   let convex = poly.convex_iter().next().unwrap() as *const _;

   let a = CollisionData { poly: convex, position: vec2(425.7369, 254.27013), offset: vec2(-0.0022510772, 0.41864455), axis_transform: Mat2 { m00: -1.2652454, m01: 0.22626114, m10: -0.22626114, m11: -1.2652454 } };
   let b = CollisionData { poly: convex, position: vec2(445.39734, 251.53616), offset: vec2(-0.15398516, 1.4457798), axis_transform: Mat2 { m00: -0.45789787, m01: -1.2009869, m10: 1.2009869, m11: -0.45789787 } };

   Contact::collide(a, b);
}
