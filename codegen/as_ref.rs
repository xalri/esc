use proc_macro2::Span;
use syn::*;

pub fn derive_as_ref_impl(input: ::proc_macro::TokenStream) -> ::proc_macro::TokenStream {
   let ast: DeriveInput = parse(input).unwrap();
   let name = ast.ident;

   match ast.data {
      Data::Struct(s) => {
         match s.fields {
            Fields::Named(fields) => {
               let iter = fields.named.iter().map(|field| {
                  let f_name = field.ident.as_ref().unwrap();
                  let ty = &field.ty;
                  quote!{
                     impl AsRef<#ty> for #name { fn as_ref(&self) -> &#ty { &self.#f_name } }
                  }
               });
               quote!(#(#iter)*).into()
            }
            Fields::Unnamed(fields) => {
               let iter = fields.unnamed.iter().enumerate().map(|(i, field)| {
                  let i = Ident::new(&format!("{}", i), Span::call_site());
                  let ty = &field.ty;
                  quote!{
                     impl AsRef<#ty> for #name { fn as_ref(&self) -> &#ty { &self.#i } }
                  }
               });
               quote!(#(#iter)*).into()
            }
            Fields::Unit => quote!().into(),
         }
      },
      Data::Enum(_) => panic!("can't derive AsRef for enum"),
      _ => unimplemented!(),
   }
}

pub fn derive_as_mut_impl(input: ::proc_macro::TokenStream) -> ::proc_macro::TokenStream {
   let ast: DeriveInput = parse(input).unwrap();
   let name = ast.ident;

   match ast.data {
      Data::Struct(s) => {
         match s.fields {
            Fields::Named(fields) => {
               let iter = fields.named.iter().map(|field| {
                  let f_name = field.ident.as_ref().unwrap();
                  let ty = &field.ty;
                  quote!{
                     impl AsMut<#ty> for #name { fn as_mut(&mut self) -> &mut #ty { &mut self.#f_name } }
                  }
               });
               quote!(#(#iter)*).into()
            }
            Fields::Unnamed(fields) => {
               let iter = fields.unnamed.iter().enumerate().map(|(i, field)| {
                  let i = Ident::new(&format!("{}", i), Span::call_site());
                  let ty = &field.ty;
                  quote!{
                     impl AsMut<#ty> for #name { fn as_mut(&mut self) -> &mut #ty { &mut self.#i } }
                  }
               });
               quote!(#(#iter)*).into()
            }
            Fields::Unit => quote!().into(),
         }
      },
      Data::Enum(_) => panic!("can't derive AsMut for enum"),
      _ => unimplemented!(),
   }
}
