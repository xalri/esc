//! A fixed circular buffer

use std::{cmp, ptr};

/// A fixed size circular buffer used for keeping copies of old data where it
/// might be needed again soon. Used in the event sequencer to keep event groups,
/// so we can resend them if the client reports them missing, among other things.
#[derive(Debug)]
pub struct Buffer<T> {
   data: Vec<T>,
   /// The current position in the buffer
   pos: usize,
}

impl<T> Buffer<T> {
   /// Create a new buffer with the given capacity
   pub fn with_capacity(size: usize) -> Buffer<T> {
      let mut data = Vec::with_capacity(size);
      unsafe {
         data.set_len(size);
      }
      Buffer { data, pos: 0 }
   }

   /// Returns true if no items have been added to the buffer.
   pub fn is_empty(&self) -> bool {
      self.data.is_empty()
   }

   /// Returns the number of items in the buffer
   pub fn len(&self) -> usize {
      self.data.len()
   }

   /// Add a value to the end of the buffer
   pub fn add(&mut self, value: T) {
      let idx = self.pos % self.data.len();
      self.pos += 1;
      unsafe {
         if self.pos > self.data.len() {
            ptr::drop_in_place(&mut self.data[idx])
         }
         ptr::write(&mut self.data[idx], value)
      }
   }

   /// Get a value from the buffer. Returns `None` if the value is out of range.
   pub fn get(&self, index: usize) -> Option<&T> {
      let overflow = if self.pos > self.data.len() {
         self.pos - self.data.len()
      } else {
         0
      };
      if index > overflow + self.data.len() - 1 || index < overflow {
         return None;
      }
      Some(&self.data[index % self.data.len()])
   }

   /// Get the most recently added value
   pub fn last(&self) -> Option<&T> {
      if self.pos == 0 { return None }
      Some(&self.data[(self.pos - 1) % self.data.len()])
   }

   /// Get an iterator over the buffer's elements, from oldest to newest.
   pub fn iter<'a>(&'a self) -> impl DoubleEndedIterator<Item = &'a T> {
      let idx = self.pos % self.data.len();
      let old = if self.pos < self.data.len() {
         [].iter()
      } else {
         self.data[idx..self.data.len()].iter()
      };
      let new = self.data[0..idx].iter();
      old.chain(new)
   }
}

impl<T> Drop for Buffer<T> {
   fn drop(&mut self) {
      for i in 0..(cmp::min(self.pos, self.data.len())) {
         unsafe { ptr::drop_in_place(&mut self.data[i]) }
      }
      unsafe {
         self.data.set_len(0);
      }
   }
}

#[test]
fn buffer() {
   let mut b = Buffer::with_capacity(4);
   for i in 0..6 {
      b.add(i);
   }
   assert_eq!(None, b.get(6));
   assert_eq!(Some(&5), b.get(5));
   assert_eq!(Some(&2), b.get(2));
   assert_eq!(None, b.get(1));

   for i in 0..6 {
      b.add(i);
   }
   assert_eq!(None, b.get(7));
   assert_eq!(None, b.get(12));
   assert_eq!(Some(&5), b.get(11));
   assert_eq!(Some(&2), b.get(8));
}

#[test]
fn buffer_iter() {
   let mut b = Buffer::with_capacity(5);
   for i in 0..3 {
      b.add(i);
   }
   assert_eq!(vec![0, 1, 2], b.iter().cloned().collect(): Vec<_>);

   for i in 3..7 {
      b.add(i);
   }

   assert_eq!(vec![2, 3, 4, 5, 6], b.iter().cloned().collect(): Vec<_>);

   for i in 7..11 {
      b.add(i);
   }

   assert_eq!(vec![6, 7, 8, 9, 10], b.iter().cloned().collect(): Vec<_>);
}

#[test]
fn buffer_iter_at_capacity() {
   let mut b = Buffer::with_capacity(5);
   for i in 0..5 {
      b.add(i);
   }
   assert_eq!(vec![0, 1, 2, 3, 4], b.iter().cloned().collect(): Vec<_>);
}
