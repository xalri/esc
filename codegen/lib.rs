#![recursion_limit = "128"]

extern crate darling;
extern crate proc_macro;
extern crate proc_macro2;
extern crate syn;
#[macro_use]
extern crate quote;
extern crate heck;

mod encode;
mod equiv;
mod as_ref;
mod partial_equiv;

use encode::*;
use equiv::*;
use as_ref::*;
use partial_equiv::*;

// #[derive(Equiv)]
// #[equiv(remote: Vec2)]
// pub struct MyVec2 { x: f32, y: f32 }
#[proc_macro_derive(Equiv, attributes(equiv))]
pub fn derive_equiv(input: ::proc_macro::TokenStream) -> ::proc_macro::TokenStream {
   derive_equiv_impl(input)
}

#[proc_macro_derive(PartialEquiv, attributes(partial_equiv))]
pub fn derive_partial_equiv(input: ::proc_macro::TokenStream) -> ::proc_macro::TokenStream {
   derive_partial_equiv_impl(input)
}

#[proc_macro_derive(AsRef, attributes())]
pub fn derive_as_ref(input: ::proc_macro::TokenStream) -> ::proc_macro::TokenStream {
   derive_as_ref_impl(input)
}

#[proc_macro_derive(AsMut, attributes())]
pub fn derive_as_mut(input: ::proc_macro::TokenStream) -> ::proc_macro::TokenStream {
   derive_as_mut_impl(input)
}

#[proc_macro_derive(Encode, attributes(rule))]
pub fn derive_encode(input: ::proc_macro::TokenStream) -> ::proc_macro::TokenStream {
   derive_encode_impl(input)
}

