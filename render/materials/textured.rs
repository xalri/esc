use glium::*;
use glium::uniforms::UniformBuffer;
use derive_deref::*;
use serde::*;
use crate::geom::*;
use crate::render::*;
use crate::resources::*;

/// A material for simple textured objects.
#[derive(Clone, Debug)]
#[derive(Serialize, Deserialize)]
pub struct Textured {
   /// The main colour texture
   pub albedo: Res<Texture>,
   /// The transformation from uv coordinates to pixels of
   /// the texture, can be changed over time for sprite sheet animation.
   #[serde(default = "Mat3::id")]
   pub tx_transform: Mat3,
   /// A clip-space position multiplier for 2D parallax effects.
   #[serde(default = "Vec2::one")]
   pub parallax: Vec2,
   /// Scene depth.
   #[serde(default)]
   pub depth: u32,
   /// Multiplied with the diffuse texture.
   #[serde(default = "Colour::white")]
   pub multiply: Colour,
   /// Colour modification
   #[serde(default)]
   pub tint: Vec<Tint>,
}

#[derive(Clone, Copy, Debug)]
#[derive(Serialize, Deserialize)]
#[repr(C)]
pub struct Tint {
   /// An additive HSV (hue, saturation & value) modifier
   pub hsv: HSV,
   #[serde(skip)]
   _padding1: f32,
   /// Filters the tint to only a particular colour 
   #[serde(default)]
   pub mask_center: HSV,
   #[serde(skip)]
   _padding2: f32,
   /// The width of the filter
   #[serde(default = "HSV::one")]
   pub mask_width: HSV,
   #[serde(skip)]
   _padding3: f32,
   #[serde(default)]
   pub mask_smooth: HSV,
   #[serde(skip)]
   _padding4: f32,
}

//implement_buffer_content!(Tint);
implement_uniform_block!(Tint, hsv, mask_center, mask_width);

impl Default for Tint {
   fn default() -> Self {
      Tint {
         hsv: HSV::default(),
         mask_center: HSV { hue: 0.5, saturation: 0.5, value: 0.5 },
         mask_width: HSV { hue: 1.0, saturation: 1.0, value: 1.0 },
         mask_smooth: HSV { hue: 0.0, saturation: 0.0, value: 0.0 },
         _padding1: 0.0,
         _padding2: 0.0,
         _padding3: 0.0,
         _padding4: 0.0,
      }
   }
}

/// Shader for the `Textured` material.
#[derive(Deref)]
pub struct TexturedShader(pub Program);

impl Default for TexturedShader {
   fn default() -> Self {
      TexturedShader(ProgramSource {
         vertex_shader: include_str!("../../glsl/parallax.vert"),
         fragment_shader: include_str!("../../glsl/textured.frag"),
         geometry_shader: None,
      }.load().unwrap())
   }
}

pub struct TexturedUniforms {
   t: Textured,
   transform: Mat3,
   tint_buffer: UniformBuffer<[Tint;8]>,
   sampler: Sampler<'static, Texture2d>,
}

impl Uniforms for TexturedUniforms {
   fn visit_values<'a, F: FnMut(&str, UniformValue<'a>)>(&'a self, mut f: F) {
      f("transform", self.transform.as_uniform_value());
      f("albedo", self.sampler.as_uniform_value());
      f("tx_transform", self.t.tx_transform.as_uniform_value());
      f("Tints", UniformValue::Block(self.tint_buffer.as_slice_any(), |_| Ok(())));
      f("multiply", self.t.multiply.as_uniform_value());
      f("parallax", self.t.parallax.as_uniform_value());
      f("depth", self.t.depth.as_uniform_value());
   }
}

impl<'a> Material for &'a Textured {
   type Program = TexturedShader;
   type Vertex = Vertex2;
   type Uniforms = TexturedUniforms;

   fn uniforms(self, transform: Mat3) -> Self::Uniforms {
      let mut tint = [Tint::default(); 8];
      for i in 0..self.tint.len() {
         if i >= 8 { panic!("Too many tints!") }
         tint[i] = self.tint[i];
      }
      
      TexturedUniforms {
         transform,
         sampler: unsafe { std::mem::transmute(self.albedo.sampled()) },
         tint_buffer: UniformBuffer::new(facade(), tint).unwrap(),
         t: self.clone(),
      }
   }

   fn params(self) -> DrawParameters<'static> {
      DrawParameters {
         blend: Blend::alpha_blending(),
         ..Default::default()
      }
   }
}

