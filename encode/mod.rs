//! Fundamental network message processing.
//!
//! Working with Altitude network message data is a somewhat non-trivial task. This library
//! provides a few handy abstractions, culminating in the means to express complex encodings
//! for our message types concisely.

mod bitstream;
mod collections;
mod containers;
mod geom;
mod prim;

pub use self::bitstream::*;
pub use self::collections::*;
pub use self::containers::*;
pub use self::geom::*;
pub use self::prim::*;
pub use esc_codegen::*;

use crate::error::*;

/// An encoding.
pub trait Encoding<Value> {
   /// Read a value from a BitReader, given the encoding configuration.
   fn read(&self, reader: &mut BitReader) -> Result<Value>;

   /// Write a value to a BitWriter, given the encoding configuration, and a borrowed value.
   fn write(&self, writer: &mut BitWriter, value: &Value) -> Result<()>;
}

/// Conversion from an Option<> that results from some `BitReader` operation into a
/// Result<>, with an 'unexpected end of data' error if we need more than the data provided.
pub fn need_data<T>(res: Option<T>) -> Result<T> {
   match res {
      Some(x) => Ok(x),
      None => bail!("Expected more data, but none remains to be read"),
   }
}

/// Encoding for a value that does not exist in the message data.
/// This is necessary to pass a constant value to a method or macro that accesses a parameter
/// through an `Encoding`.
pub struct ReifiedRule<T>(pub T);

impl<T> Encoding<T> for ReifiedRule<T>
where
   T: Clone,
{
   fn read(&self, _reader: &mut BitReader) -> Result<T> {
      Ok(self.0.clone())
   }
   fn write(&self, _writer: &mut BitWriter, _value: &T) -> Result<()> {
      Ok(())
   }
}

/// Encode a value to a raw byte vector.
pub fn encode<E, V>(encoding: &E, value: &V) -> Result<Vec<u8>>
where
   E: Encoding<V>,
{
   let mut writer = BitWriter::new();
   encoding.write(&mut writer, value)?;
   Ok(writer.into_inner())
}

/// Decode a value from a raw byte slice.
pub fn decode<E, V>(encoding: &E, data: &[u8]) -> Result<V>
where
   E: Encoding<V>,
{
   let mut reader = BitReader::open(data);
   encoding.read(&mut reader)
}
