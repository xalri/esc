use crate::geom::*;

/// Triangulate a polygon using ear-clipping, returns a vector of indices.
pub fn triangulate(poly: &[Vec2]) -> Option<Vec<usize>> {
   let n = poly.len();
   if n < 3 {
      return None;
   }

   let mut verts: Vec<usize> = (0..n).collect();
   if cross(poly) < 0.0 {
      verts.reverse();
   }

   let mut result = Vec::new();
   let mut count = verts.len() * 2;
   let mut u;
   let mut v = verts.len() - 1;
   let mut w;

   while verts.len() > 2 {
      if count == 0 {
         return None;
      }
      count -= 1;

      u = v;
      if u >= verts.len() {
         u = 0;
      }
      v = u + 1;
      if v >= verts.len() {
         v = 0;
      }
      w = v + 1;
      if w >= verts.len() {
         w = 0;
      }

      if cross(&[poly[verts[u]], poly[verts[v]], poly[verts[w]]]) == 0.0 {
         verts.remove(v);
         continue;
      }

      if is_ear(poly, &verts, u, v, w) {
         result.extend_from_slice(&[verts[u], verts[v], verts[w]]);
         verts.remove(v);
         count = verts.len() * 2;
      }
   }

   Some(result)
}

/// Test if some three points in a polygon are an ear.
fn is_ear(poly: &[Vec2], verts: &[usize], u: usize, v: usize, w: usize) -> bool {
   let tri = [poly[verts[u]], poly[verts[v]], poly[verts[w]]];
   if cross(&tri) < 0.0 {
      return false;
   }

   !verts
      .iter()
      .enumerate()
      .filter(|&(i, _)| !(i == u || i == v || i == w))
      .map(|(_, &i)| &poly[i])
      .any(|p| point_in_triangle(tri[0], tri[1], tri[2], *p))
}

/// Check if a point lies within a convex triangle
fn point_in_triangle(a: Vec2, b: Vec2, c: Vec2, p: Vec2) -> bool {
   (c.x - p.x) * (a.y - p.y) - (a.x - p.x) * (c.y - p.y) >= 0.0
      && (a.x - p.x) * (b.y - p.y) - (b.x - p.x) * (a.y - p.y) >= 0.0
      && (b.x - p.x) * (c.y - p.y) - (c.x - p.x) * (b.y - p.y) >= 0.0
}

#[test]
fn test_inside_triangle() {
   let tests = &[
      (true, vec2(1.0, 0.5)),
      (true, vec2(0.5, 0.4)),
      (false, vec2(0.5, 0.6)),
      (false, vec2(1.0, -0.1)),
   ];

   for &(expected, point) in tests {
      assert_eq!(
         expected,
         point_in_triangle(vec2(0.0, 0.0), vec2(2.0, 0.0), vec2(1.0, 1.0), point)
      );
   }
}

#[test]
fn test_is_ear() {
   let poly = &[vec2(0.0, 0.0), vec2(-1.0, 1.0), vec2(0.0, -1.0), vec2(1.0, 1.0)];

   let verts = &[0, 1, 2, 3];

   assert_eq!(true, is_ear(poly, verts, 0, 1, 2));
   assert_eq!(false, is_ear(poly, verts, 1, 2, 3));
   assert_eq!(true, is_ear(poly, verts, 2, 3, 0));
   assert_eq!(false, is_ear(poly, verts, 3, 0, 1));
}

#[test]
fn test_triangulate() {
   let poly = &[vec2(0.0, 0.0), vec2(-1.0, 1.0), vec2(0.0, -1.0), vec2(1.0, 1.0)];

   assert!(triangulate(poly).is_some());
}
