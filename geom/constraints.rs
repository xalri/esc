//! Sequential impulses constraint solver.

use super::math::*;
use super::components::*;
use super::collide::*;
use crate::storage::*;

const ALLOWED_PENETRATION: f32 = 0.2;
const BIAS_FACTOR: f32 = 0.1;

#[derive(Debug, Clone, Copy)]
pub struct ContactConstraint {
   pub contact: Contact,
   pub friction: f32,
   pub elasticity: f32,
   impulse_n: f32,
   impulse_t: f32,
   normal_mass: f32,
   tangent_mass: f32,
   bias: f32,
   ra: Vec2,
   rb: Vec2,
   ma: f32,
   mb: f32,
   ia: f32,
   ib: f32,
}

impl ContactConstraint {
   pub fn new(contact: Contact) -> ContactConstraint {
      ContactConstraint { 
         contact, 
         friction: 0.0,
         elasticity: 0.0,
         impulse_n: 0.0,
         impulse_t: 0.0,
         normal_mass: 0.0,
         tangent_mass: 0.0,
         bias: 0.0,
         ra: Vec2::zero(),
         rb: Vec2::zero(),
         ma: 0.0,
         mb: 0.0,
         ia: 0.0,
         ib: 0.0,
      }
   }

   pub fn pre_step(&mut self, transform: &mut Store<Transform>, vel: &mut Store<Velocity>, mass: &Store<Mass>) {
      let a = self.contact.a;
      let b = self.contact.b;
      let c = self.contact;
      assert!(a != b);

      self.ra = (c.contact_a - transform[a].position).perpendicular();
      self.rb = (c.contact_b - transform[b].position).perpendicular();

      let n = c.intersection.normal;
      let t = n.perpendicular();
      
      self.ma = mass[a].linear;
      self.mb = mass[b].linear;
      self.ia = mass[a].angular;
      self.ib = mass[b].angular;

      self.normal_mass = invert(
            self.ma + self.mb +
            self.ia * self.ra.dot(n).powi(2) +
            self.ib * self.rb.dot(n).powi(2)
      );

      self.tangent_mass = invert(
            self.ma + self.mb +
            self.ia * self.ra.dot(t).powi(2) +
            self.ib * self.rb.dot(t).powi(2)
      );

      self.bias = -BIAS_FACTOR * 0.0f32.min(c.intersection.t + ALLOWED_PENETRATION);

      let vpa = vel[a].linear + (self.ra * vel[a].angular);
      let vpb = vel[b].linear + (self.rb * vel[b].angular);
      let dv = (vpa - vpb).dot(n);

      if dv < -1.0 { self.bias += -self.elasticity * dv }

      if c.intersection.t > 0.0 {
         // Integrate up to the time of the collision
         let time_a = (c.intersection.t - vel[a].relative_time).max(0.0);
         let time_b = (c.intersection.t - vel[b].relative_time).max(0.0);
         
         transform[a].position += vel[a].linear * time_a;
         transform[b].position += vel[b].linear * time_b;

         transform[a].orientation += vel[a].angular * time_a;
         transform[b].orientation += vel[b].angular * time_b;

         transform[a].orientation = wrap_angle(transform[a].orientation);
         transform[b].orientation = wrap_angle(transform[b].orientation);

         vel[a].relative_time += time_a;
         vel[b].relative_time += time_b;
      }

      if !self.is_valid(transform, vel) { 
         self.tangent_mass = 0.0;
         self.normal_mass = 0.0;
         return;
      }
   }

   // Check if the objects still collide
   pub fn is_valid(&self, transform: &Store<Transform>, vel: &Store<Velocity>) -> bool {
      let a = self.contact.a;
      let b = self.contact.b;
      let c = self.contact;

      let data_a = CollisionData {
         poly: c.poly_a,
         position: transform[a].position,
         axis_transform: Mat2::rotate(transform[a].orientation) * Mat2::scale(transform[a].scale),
         offset: vel[a].linear,
      };

      let data_b = CollisionData {
         poly: c.poly_b,
         position: transform[b].position,
         axis_transform: Mat2::rotate(transform[b].orientation) * Mat2::scale(transform[b].scale),
         offset: vel[b].linear,
      };

      Intersection::test(data_a, data_b).is_some()
   }

   pub fn step(&mut self, vel: &mut Store<Velocity>) {
      let a = self.contact.a;
      let b = self.contact.b;
      let c = self.contact;
      let n = c.intersection.normal;
      let t = n.perpendicular();

      // Relative velocity
      let dv = vel[a].linear + (self.ra * vel[a].angular)
             - vel[b].linear - (self.rb * vel[b].angular);

      let vn = dv.dot(n);                               // Relative velocity along the normal
      let djn = self.normal_mass * (-vn + self.bias);   // New impulse
      let impulse_n0 = self.impulse_n;                  // Previous impulse
      self.impulse_n = 0.0f32.max(impulse_n0 + djn);    // Clamp total impulse to be positive

      let vt = dv.dot(t);
      let djt = self.tangent_mass * vt;
      let max_jt = self.friction * self.impulse_n;
      let impulse_t0 = self.impulse_t;
      self.impulse_t = clamp(impulse_t0 + djt, -max_jt, max_jt);

      // Apply the change in impulse
      let impulse = n * (self.impulse_n - impulse_n0) - t * (self.impulse_t - impulse_t0);
      debug_assert!(impulse.valid());
      if impulse.valid() {
         vel[a].linear += impulse * self.ma;
         vel[a].angular += self.ra.dot(impulse) * self.ia;
         vel[b].linear -= impulse * self.mb;
         vel[b].angular -= self.rb.dot(impulse) * self.ib;
      }
   }
}
