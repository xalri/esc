use std::marker::Unsize;
use std::ops;

/// A trait for a tuple of references `(&A, &mut B, ...)`.
/// Can be constructed from any type which implements `AsMut` for all types
/// referenced by the tuple.
pub trait RefTuple<'a> {
   type Resources: ?Sized;
   /// Fetch the references from some struct.
   /// The caller must ensure the returned references remain valid & unique for
   /// their lifetime.
   fn fetch<'b: 'a>(res: &'b mut (impl Unsize<Self::Resources> + 'static)) -> Self;
}

/// A trait for casting mutable references to either mutable or shared references
pub trait Ref<Target> { fn into(self) -> Target; }
impl<'a, A> Ref<&'a A> for &'a mut A { fn into(self) -> &'a A { self } }
impl<'a, A> Ref<&'a mut A> for &'a mut A { fn into(self) -> &'a mut A { self } }

macro_rules! impl_ref_tuple {
    ($store:ident, $(($t:ident, $tref:ident)),*) => {
      pub trait $store<$($t: 'static),*>: $(AsMut<$t>+)* { }
      impl<$($t: 'static),*, T: $(AsMut<$t>+)*> $store<$($t),*> for T { }

      impl<'a, $($t,)* $($tref),*> RefTuple<'a> for ($($tref,)*)
      where
         $($t: 'static,)*
         $(&'a mut $t: Ref<$tref>, $tref: ops::Deref<Target=$t>),*
      {
         type Resources = dyn $store<$($t),*>;
         fn fetch<'b: 'a>(res: &'b mut (impl Unsize<Self::Resources> + 'static)) -> ($($tref,)*) {
            let res = &mut *(res as &mut Self::Resources) as *mut Self::Resources;
            (
               $( Ref::<$tref>::into((unsafe{&mut *res}: &'static mut _).as_mut(): &'a mut $t), )*
            )
         }
      }
    };
}

impl_ref_tuple!(AsMut1, (A, RefA));
impl_ref_tuple!(AsMut2, (A, RefA), (B, RefB));
impl_ref_tuple!(AsMut3, (A, RefA), (B, RefB), (C, RefC));
impl_ref_tuple!(AsMut4, (A, RefA), (B, RefB), (C, RefC), (D, RefD));
impl_ref_tuple!(AsMut5, (A, RefA), (B, RefB), (C, RefC), (D, RefD), (E, RefE));
impl_ref_tuple!(AsMut6, (A, RefA), (B, RefB), (C, RefC), (D, RefD), (E, RefE), (F, RefF));
impl_ref_tuple!(AsMut7, (A, RefA), (B, RefB), (C, RefC), (D, RefD), (E, RefE), (F, RefF), (G, RefG));
impl_ref_tuple!(AsMut8, (A, RefA), (B, RefB), (C, RefC), (D, RefD), (E, RefE), (F, RefF), (G, RefG), (H, RefH));
impl_ref_tuple!(AsMut9, (A, RefA), (B, RefB), (C, RefC), (D, RefD), (E, RefE), (F, RefF), (G, RefG), (H, RefH), (I, RefI));
impl_ref_tuple!(AsMut10, (A, RefA), (B, RefB), (C, RefC), (D, RefD), (E, RefE), (F, RefF), (G, RefG), (H, RefH), (I, RefI), (J, RefJ));
impl_ref_tuple!(AsMut11, (A, RefA), (B, RefB), (C, RefC), (D, RefD), (E, RefE), (F, RefF), (G, RefG), (H, RefH), (I, RefI), (J, RefJ), (K, RefK));
impl_ref_tuple!(AsMut12, (A, RefA), (B, RefB), (C, RefC), (D, RefD), (E, RefE), (F, RefF), (G, RefG), (H, RefH), (I, RefI), (J, RefJ), (K, RefK), (L, RefL));
impl_ref_tuple!(AsMut13, (A, RefA), (B, RefB), (C, RefC), (D, RefD), (E, RefE), (F, RefF), (G, RefG), (H, RefH), (I, RefI), (J, RefJ), (K, RefK), (L, RefL), (M, RefM));
impl_ref_tuple!(AsMut14, (A, RefA), (B, RefB), (C, RefC), (D, RefD), (E, RefE), (F, RefF), (G, RefG), (H, RefH), (I, RefI), (J, RefJ), (K, RefK), (L, RefL), (M, RefM), (N, RefN));
impl_ref_tuple!(AsMut15, (A, RefA), (B, RefB), (C, RefC), (D, RefD), (E, RefE), (F, RefF), (G, RefG), (H, RefH), (I, RefI), (J, RefJ), (K, RefK), (L, RefL), (M, RefM), (N, RefN), (O, RefO));
