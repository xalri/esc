use std::ops::*;
use glium::{uniform, Program, DrawParameters, Blend};
use derivative::*;
use serde::*;
use crate::geom::*;
use crate::render::*;

/// A material for solid colour objects.
/// Expects vertex attributes `pos: vec2` & `uv: vec2`.
#[derive(Clone, Derivative)]
#[derive(Serialize, Deserialize)]
#[derivative(Default)]
pub struct SolidColour {
   /// The colour to draw
   #[derivative(Default(value = "Colour::white()"))]
   pub colour: Colour,
   /// An additive HSV (hue, saturation & value) modifier
   pub hsv: HSV,
   /// A clip-space position multiplier for 2D parallax effects.
   #[derivative(Default(value = "vec2(1.0, 1.0)"))]
   pub parallax: Vec2,
   /// Scene depth.
   pub depth: u32,
}

/// Shader for the `Solid` material; implement's `Default`, load after the openGL context is created.
pub struct SolidShader(pub Option<Program>);

impl Deref for SolidShader {
   type Target = Program;
   fn deref(&self) -> &Program {
      self.0.as_ref().expect("Missing SolidShader (was there a rendering context when the shader was loaded?)")
   }
}

impl Default for SolidShader {
   fn default() -> Self {
      if has_facade() {
         SolidShader(Some(ProgramSource {
            vertex_shader: include_str!("../../glsl/parallax.vert"),
            fragment_shader: include_str!("../../glsl/solid.frag"),
            geometry_shader: None,
         }.load().unwrap()))
      } else {
         SolidShader(None)
      }
   }
}

impl Material for &SolidColour {
   type Program = SolidShader;
   type Vertex = Vertex2;
   type Uniforms = uniforms_type!(u32, Vec2, Colour, Mat3);

   fn uniforms(self, transform: Mat3) -> Self::Uniforms {
      uniform! {
         transform: transform,
         colour: self.colour,
         parallax: self.parallax,
         depth: self.depth,
      }
   }

   fn params(self) -> DrawParameters<'static> {
      DrawParameters {
         blend: Blend::alpha_blending(),
         ..Default::default()
      }
   }
}

