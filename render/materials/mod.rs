mod solid;
mod textured;
mod tile;

pub use self::solid::*;
pub use self::textured::*;
pub use self::tile::*;

use std::ops::Deref;
use glium::DrawParameters;
use crate::geom::Mat3;
use serde::*;

pub trait Material {
   /// The program to use
   type Program: Deref<Target=glium::Program>;

   /// The vertex type expected by the program.
   type Vertex: glium::Vertex + Serialize + for<'a> Deserialize<'a>;

   /// The type of the uniform block to pass to the program (usually declared as existential
   /// with the actual type being defined by the `uniform!` macro).
   type Uniforms: glium::uniforms::Uniforms;

   /// Get the uniform block for this instance of the material.
   fn uniforms(self, transform: Mat3) -> Self::Uniforms;

   /// Get the draw parameters for this instance of the material.
   fn params(self) -> DrawParameters<'static>;
}

/// Get the type created by glium's `uniforms` macro, used to define the associated type of things
/// implementing the `Material` trait.
pub macro uniforms_type {
   ($t:ty) => {
      glium::uniforms::UniformsStorage<'static, $t, glium::uniforms::EmptyUniforms>
   },
   ($t:ty, $($rest:ty),*) => {
      glium::uniforms::UniformsStorage<'static, $t, uniforms_type!($($ rest),*) >
   }
}
