//! Input mapping.

use std::collections::HashMap;
use serde::*;
use maplit::*;
use crate::storage::{Store, EntitySet};
use crate::window::Window;
use glium::glutin::*;
use crate::util::time::GameTime;
use crate::util::buffer::*;

#[derive(Default, Debug)]
pub struct InputSource {
  pub name: String,
  pub button_pressed: HashMap<u32, u64>, // Key code -> Frame pressed
  pub button_released: HashMap<u32, u64>, // Key code -> Frame released
  pub axis: HashMap<u32, Buffer<f64>>, // Values for n most recent frames
  pub button_name: HashMap<String, u32>, // Name -> Keycode
  pub axis_name: HashMap<String, u32>, // Name -> Axis code
}

#[derive(Default, Debug, Clone)]
#[derive(Serialize, Deserialize)]
pub struct Controller {
   #[serde(default)]
   pub buttons: HashMap<String, (u64, u64)>, // name -> (pressed, released),
   #[serde(default)]
   pub axes: HashMap<String, f64>, // name -> value
   pub map: HashMap<String, Vec<InputMapping>>, // name -> mapping,
}

#[derive(Default, Debug, Clone)]
#[derive(Serialize, Deserialize)]
pub struct InputMapping { 
   #[serde(default)]
   pub controller: Option<String>, // If controller is None, matches any controller
   pub input_name: String 
}

impl Controller {
   /// Check if a button is currently held.
   /// Every button press lasts at least one frame.
   pub fn is_pressed(&self, name: &str, tick: u64) -> bool {
      self.buttons.get(name).map(|(pressed, released)| {
         *pressed == (tick - 1) || pressed > released
      }).unwrap_or(false)
   }

   pub fn axis(&self, name: &str) -> f32 {
      *self.axes.get(name).unwrap_or(&0.0) as f32
   }
}

/// Updates controllers based on their mappings
pub fn map_controls(source: &Store<InputSource>, controller: &mut Store<Controller>) {
   for (_, controller) in controller.iter_mut() {
      for (name, maps) in &controller.map {
         for map in maps {
            for (_, source) in source.iter() {
               if map.controller.is_some() && map.controller.as_ref().unwrap() != &source.name {
                  continue;
               }
               for (button_name, button) in &source.button_name {
                  if button_name == &map.input_name {
                     let pressed = *source.button_pressed.get(button).unwrap_or(&0);
                     let released = *source.button_released.get(button).unwrap_or(&0);
                     log::trace!("Mapping button {} ({},{})", name, pressed, released);
                     controller.buttons.insert(name.clone(), (pressed, released));
                  }
               }
               for (axis_name, axis) in &source.axis_name {
                  if axis_name == &map.input_name {
                     let value = *source.axis[axis].last().unwrap_or(&0.0);
                     log::trace!("Mapping axis {} {}", name, value);
                     controller.axes.insert(name.clone(), value);
                  }
               }
            }
         }
      }
   }
}

/// Create input sources from keyboard & mouse events from the windows
pub fn update_window_input_sources(
   entities: &mut EntitySet, 
   window: &Window, 
   time: &GameTime, 
   source: &mut Store<InputSource>,
) {
   let tick = time.tick;
   let mut mouse = None;
   let mut keyboard = None;

   for (id, source) in source.iter() {
      if source.name == "keyboard" { keyboard = Some(id) }
      if source.name == "mouse" { mouse = Some(id) }
   }

   let keyboard = keyboard.unwrap_or_else(|| {
      let e = entities.create();
      source.insert(e, InputSource { name: "keyboard".into(), .. Default::default() });
      e
   });

   let mouse = mouse.unwrap_or_else(|| {
      let e = entities.create();
      source.insert(e, InputSource { 
         name: "mouse".into(), 
         axis: hashmap!{ 0 => Buffer::with_capacity(128), 1 => Buffer::with_capacity(128) },
         axis_name: hashmap!{ 
            "CursorX".into() => 0, 
            "CursorY".into() => 1,
         },
         button_name: hashmap!{ 
            "MouseLeft".into() => 0,
            "MouseRight".into() => 1,
            "MouseMiddle".into() => 2,
         },
         .. Default::default()
      });
      e
   });

   for ev in &window.window_events {
      match *ev {
         WindowEvent::CursorMoved { position, .. } => {
            source[mouse].axis.get_mut(&0).unwrap().add(position.x / window.dimensions.x as f64);
            source[mouse].axis.get_mut(&1).unwrap().add(position.y / window.dimensions.y as f64);
         }
         WindowEvent::MouseInput { state: s, button, .. } => {
            let idx = mouse_button_idx(button);
            let pressed = s == ElementState::Pressed;
            set_button(&mut source[mouse], idx, pressed, tick);
         }
         WindowEvent::KeyboardInput { input, .. } => {
            let pressed = input.state == ElementState::Pressed;
            if let Some(code) = input.virtual_keycode {
               let name = format!("{:?}", code);
               log::debug!("Mapping {} -> {:?}", input.scancode, name);
               source[keyboard].button_name.insert(name, input.scancode);
            }
            set_button(&mut source[keyboard], input.scancode, pressed, tick);
         }
         _ => (),
      }
   }
}

fn set_button(source: &mut InputSource, idx: u32, pressed: bool, tick: u64) {
   log::debug!("Setting button {}:{}, {} at {}", source.name, idx, pressed, tick);
   if pressed {
      source.button_pressed.insert(idx, tick);
   } else {
      source.button_released.insert(idx, tick);
   }
}

fn mouse_button_idx(button: MouseButton) -> u32 {
   match button {
      MouseButton::Left => 0,
      MouseButton::Right => 1,
      MouseButton::Middle => 2,
      MouseButton::Other(x) => x as u32,
   }
}
