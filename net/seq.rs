use crate::encode::*;
use crate::error::*;
use std::ops::Deref;

/// Each guaranteed transmission is attributed a sequence number.
#[derive(Clone, Copy, Hash, PartialOrd, PartialEq, Ord, Eq, Debug)]
pub struct Seq(pub u32);

pub struct SeqSmallRule;

/// Sequence numbers are often wrapped into 16 bits for transmission.
#[derive(Clone, Copy, Hash, PartialOrd, PartialEq, Ord, Eq, Debug, Encode)]
#[rule("SeqSmallRule")]
pub struct SeqSmall(#[rule("BitableRule")] pub u16);

impl Deref for Seq {
   type Target = u32;
   fn deref(&self) -> &u32 {
      &self.0
   }
}

impl Seq {
   /// Truncate to a `SeqSmall`, reducing the range from 32 to 16 bits.
   pub fn into_small(self) -> SeqSmall {
      SeqSmall(*self as u16)
   }
}

impl Deref for SeqSmall {
   type Target = u16;
   fn deref(&self) -> &u16 {
      &self.0
   }
}

impl SeqSmall {
   pub fn new(seq: i64) -> SeqSmall {
      SeqSmall(seq as u16)
   }

   /// The full sequence number can be inferred from a `SeqSmall` if
   /// we have the last used `Seq`.
   pub fn into_full(self, prev: Seq) -> Seq {
      let mut seq = *self as u32;
      let a = 1 << 16;
      while seq < *prev {
         seq += a
      }
      if seq > *prev + a / 2 {
         seq -= a
      }
      Seq(seq)
   }

   pub fn get(self, prev: i64) -> i64 {
      let mut seq = *self as i64;
      let a = 1 << 16;
      while seq < prev {
         seq += a
      }
      if seq > prev + a / 2 {
         seq -= a
      }
      seq
   }
}

#[test]
fn seq_truncate() {
   assert_eq!(0x0, *Seq(0x0).into_small());
   assert_eq!(0x1, *Seq(0x1).into_small());
   assert_eq!(0xFFFF, *Seq(0xFFFF).into_small());
   assert_eq!(0x0, *Seq(0x10000).into_small());
   assert_eq!(0xFFFF, *Seq(0x1FFFF).into_small());
}

#[test]
fn seq_into_full() {
   assert_eq!(0x0, *SeqSmall(0x0).into_full(Seq(0x0)));
   assert_eq!(0x0, *SeqSmall(0x0).into_full(Seq(0x7FFF)));
   assert_eq!(0x10000, *SeqSmall(0x0).into_full(Seq(0x8000)));

   assert_eq!(0x1, *SeqSmall(0x1).into_full(Seq(0x0)));
   assert_eq!(0x1, *SeqSmall(0x1).into_full(Seq(0x8000)));
   assert_eq!(0x10001, *SeqSmall(0x1).into_full(Seq(0x8001)));
}
