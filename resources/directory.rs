use std::path::*;
use std::io::{Read, BufReader};
use std::fs::File;
use log::*;
use crate::resources::Source;
use crate::error::*;
use walkdir::WalkDir;

/// A resource directory.
pub struct Directory {
   root: PathBuf,
}

impl Source for Directory {
   /// Load a file into memory.
   fn load(&self, path: &str) -> Result<Vec<u8>> {
      let path = self.root.join(Path::new(path));
      debug!("Loading file {:?}", path);
      let mut vec = vec![];
      let mut reader = BufReader::new(File::open(path)?);
      reader.read_to_end(&mut vec)?;
      Ok(vec)
   }
}

impl Directory {
   pub fn open<P: AsRef<Path>>(path: P) -> Directory {
      Directory {
         root: path.as_ref().to_owned(),
      }
   }

   /// Get a list of the files in the source
   pub fn walk(&self) -> Vec<String> {
      let root = self.root.clone();
      WalkDir::new(&root)
         .into_iter()
         .filter_map(|e| e.ok())
         .map(|e| e.path().strip_prefix(&root).unwrap().to_str().unwrap().to_string())
         .collect()
   }
}
