//! Various things relating to the passage of time.

pub use time::precise_time_ns;
use derivative::*;

pub fn now() -> u64 { precise_time_ns() }

/// The number of nanoseconds in one second.
pub const NS_PER_S: u64 = 1_000_000_000;

/// The number of nanoseconds in one millisecond.
pub const NS_PER_MS: u64 = 1_000_000;

/// Convert a value in seconds to nanoseconds.
pub fn seconds_to_ns(s: f64) -> u64 {
   (s * NS_PER_S as f64) as u64
}

/// Convert a value in milliseconds to nanoseconds.
pub fn ms_to_ns(ms: f64) -> u64 {
   (ms * NS_PER_MS as f64) as u64
}

/// Convert a value in nanoseconds to seconds.
pub fn ns_to_seconds(ns: u64) -> f64 {
   ns as f64 / NS_PER_S as f64
}

/// Convert a value in nanoseconds to milliseconds.
pub fn ns_to_ms(ns: u64) -> f64 {
   ns as f64 / NS_PER_MS as f64
}

/// A simple countdown timer.
#[derive(PartialEq, Debug, Clone, Copy, Default)]
pub struct Delay {
   pub initial: f32,
   pub remaining: f32,
   pub expired: bool,
}

impl Delay {
   /// Create a new delay with the given duration in ticks.
   pub fn new(duration: f32) -> Delay {
      Delay {
         initial: duration,
         remaining: duration,
         expired: false,
      }
   }

   /// Advance the timer by one tick.
   pub fn tick(&mut self) {
      if !self.expired {
         self.remaining -= 1.0;
         if self.remaining <= 0.0 {
            self.remaining = 0.0;
            self.expired = true;
         }
      }
   }

   /// The fraction of the time which is remaining
   pub fn fraction_remaining(&self) -> f32 {
      self.remaining / self.initial
   }

   /// The fraction of the time which has already expired.
   pub fn fraction_expired(&self) -> f32 {
      1.0 - self.fraction_remaining()
   }

   /// Restart the delay.
   pub fn reset(&mut self) {
      self.expired = false;
      self.remaining = self.initial;
   }

   /// Set the remaining time.
   pub fn set_remaining(&mut self, duration: f32) {
      self.expired = false;
      self.remaining = duration;
   }
}

#[derive(Derivative)]
#[derivative(Default)]
pub struct GameTime {
   /// The time scale for the simulation
   #[derivative(Default(value = "1.0"))]
   pub scale: f64,
   /// The time in seconds since the world was created.
   pub real: f64,
   /// The time in seconds of the simulation.
   pub sim: f64,
   /// Number of ticks since the start of the simulation.
   pub tick: u64,
   /// The time in seconds since the previous interval.
   /// In System::update, the interval is between updates.
   /// In System::tick, the interval is the fixed time-step at which the simulation runs.
   pub delta: f64,
   /// The time in seconds which has elapsed in real-time but not yet been simulated.
   /// Due to time scaling this is not equivalent to `real - sim`.
   pub pending: f64,
   /// Asserts that `tick` is only called when `pending_tick` is true
   #[derivative(Default(value = "true"))]
   pub realtime: bool,
   /// The time the game was started.
   #[derivative(Default(value = "now()"))]
   pub start: u64,
   /// The time the last frame started.
   #[derivative(Default(value = "now()"))]
   pub frame_start: u64,
   /// The real time at which the last tick started.
   #[derivative(Default(value = "now()"))]
   pub tick_start: u64,
   /// Time which has elapsed but has not yet been simulated.
   pub rollover: i64,
   /// Set to true to stop running game ticks
   pub paused: bool,
   /// The length in nanoseconds of a game tick
   #[derivative(Default(value = "1_000_000_000 / 30"))]
   pub tick_step_ns: u64,
}

impl GameTime {
   /// Create a game timer with a specific length of tick in nanoseconds.
   pub fn with_rate(tick_step_ns: u64) -> Self {
      GameTime { tick_step_ns, .. Default::default() }
   }

   /// Try to start a fixed time-step game tick.
   /// Returns true if a tick is ready to run, false otherwise.
   pub fn tick(&mut self) -> bool {
      if self.paused { return false; }
      let time = now();
      let simulation_time = self.tick_start as i64 - self.rollover;

      let pending_tick = (time as i64 - simulation_time) >= self.tick_step_ns as i64;
      if self.realtime && !pending_tick { return false }

      self.tick += 1;
      let diff = time as i64 - (self.tick_start + self.tick_step_ns) as i64;
      if self.realtime {
         assert!(self.rollover + diff >= 0, "World::tick called while Time::pending_tick was false");
         self.rollover += diff;
      } else {
         // Allow simulating faster than real-time
         self.rollover = i64::max(0, self.rollover + diff);
      }
      self.tick_start = time;
      if self.realtime { 
         // Assert that simulation time has advanced by `TICK_STEP_NS`.
         assert!((self.tick_start as i64 - self.rollover) == simulation_time + self.tick_step_ns as i64) 
      }

      // Update derived times
      self.delta = ns_to_seconds(self.tick_step_ns) * self.scale;
      self.sim += self.delta;
      self.pending = 0.0;

      true
   }

   /// Start a non-fixed iteration (for interpolation & rendering).
   /// Can be called as often as possible, sets `delta` to the time since the 
   /// last tick and `pending` to the un-simulated time in seconds
   pub fn update(&mut self) {
      let time = now();

      self.delta = ns_to_seconds(time - self.frame_start);
      self.real = ns_to_seconds(time - self.start);
      self.pending = ns_to_seconds(time - (self.tick_start as i64 - self.rollover) as u64) * self.scale;
      self.frame_start = time;
   }
}

