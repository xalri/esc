use std::ops::*;
use serde::*;

/// Depth of an object in a 2D scene.
/// Higher values are further from the screen.
/// Converted to a 24bit unsigned integer for the depth map.
#[derive(Default, Copy, Clone, Debug)]
#[derive(Serialize, Deserialize)]
pub struct Depth {
   /// Layer, 8 bits of precision
   pub layer: i16,
   /// Depth of the object within the layer, 12 bits of precision
   pub object: i16,
   /// Depth of a part of an object within an object, 4 bits of precision
   pub local: i16,
}

impl Depth {
   /// Create a new `Depth`, asserts that the values are in bounds for a 24bit depth buffer.
   pub fn new(layer: i16, object: i16, local: i16) -> Depth {
      assert!(layer + 128 < 0xFF);
      assert!(object + 2048 < 0xFFF);
      assert!(local + 8 < 0xF);
      Depth { layer, object, local }
   }

   /// Convert the depth to a single u32
   pub fn to_u32(&self) -> u32 {
      ((self.layer as i32 + 128) as u32) << 16
         | (((self.object as i32 + 2048) & 0xFFF) as u32) << 4
         | (((self.local as i32 + 8) & 0xF) as u32) << 0
   }

   /// Convert the depth to a normalized float
   pub fn to_f32(&self) -> f32 {
      self.to_u32() as f32 / 16777215.0
   }
}

impl Add<Depth> for Depth {
   type Output = Depth;
   fn add(self, rhs: Depth) -> Depth {
      Depth::new(self.layer + rhs.layer, self.object + rhs.object, self.local + rhs.local)
   }
}

/// Shorthand constructor for a `Depth`.
pub fn depth(layer: i16, object: i16, local: i16) -> Depth {
   Depth::new(layer, object, local)
}

#[test]
fn test_depth() {
   assert!(depth(0, 0, 0).to_u32() > depth(0, 0, -1).to_u32());
   assert!(depth(0, 0, 0).to_u32() < depth(0, 0, 1).to_u32());

   assert!(depth(0, 0, 0).to_u32() > depth(0, -1, 0).to_u32());
   assert!(depth(0, 0, 0).to_u32() < depth(0, 1, 0).to_u32());

   assert!(depth(0, 0, 0).to_u32() > depth(-1, 0, 0).to_u32());
   assert!(depth(0, 0, 0).to_u32() < depth(1, 0, 0).to_u32());
}
