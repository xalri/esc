use crate::geom::*;
use crate::resources::*;
use crate::storage::*;
use crate::error::*;
use super::*;

use std::borrow::Cow;
use glium::{implement_vertex, DrawParameters, Frame, Surface};
use glium::vertex::*;
use glium::index::*;
use serde::*;

pub use glium::index::PrimitiveType;

/// Basic 2D vertex type.
#[derive(Clone, Copy, Debug)]
#[derive(Serialize, Deserialize)]
pub struct Vertex2 { pub pos: Vec2, pub uv: Vec2 }
implement_vertex!(Vertex2, pos, uv);

/// Create a line strip vertex buffer for a poly
pub fn outline_verts(p: &PolyVec) -> Vec<Vertex2> {
   let bounds = p.aabb();
   p.iter()
      .map(|&p| Vertex2 { pos: p, uv: bounds.interpolate(p), })
      .collect::<Vec<_>>()
}

/// Vertex data
#[derive(Debug)]
pub struct Vertices<V: Copy> {
   pub data: Vec<V>
}

impl<V> Resource for Vertices<V> where V: 'static + Copy + Serialize, for<'a> V: Deserialize<'a> {
   type Descriptor = Vec<V>;
   fn load(_source: &dyn Source, data: Vec<V>) -> Result<Self> {
      Ok(Vertices{ data })
   }
}

/// Vertex & index data for a drawable thing.
#[derive(Serialize, Deserialize)]
#[derive(Debug)]
#[serde(bound = "")]
pub struct Mesh<V: Copy> where
   V: 'static + Copy + Serialize,
   for<'a> V: Deserialize<'a>
{
   pub vertices: Res<Vertices<V>>,
   #[serde(with="detail::PrimitiveTypeDef")]
   pub primitives: PrimitiveType,
   pub mask: u64,
}

impl<V: Copy> Clone for Mesh<V> where
   V: 'static + Copy + Serialize,
   for<'a> V: Deserialize<'a>
{
   fn clone(&self) -> Self {
      Mesh { vertices: self.vertices.clone(), primitives: self.primitives, mask: self.mask }
   }
}

/// A view into the world. Meshes are drawn for every view where the id
/// matches the meshes mask.
#[derive(Serialize, Deserialize, Debug)]
pub struct View {
   /// The area in world-space to draw.
   pub world: Bounds,
   /// The view's ID. Entities are only drawn if the drawable's mask matches this ID.
   pub id: u64,
   /// The target aspect ratio (width / height) for optional letterboxing
   pub target_ratio: Option<f32>,
   /// The surface to draw to.
   #[serde(skip, default = "std::ptr::null_mut")]
   pub surface: *mut Frame,
   /// The size of the surface in pixels (derived)
   #[serde(skip)]
   pub surface_size: Vec2i,
   /// The size of the window on the surface in pixels (derived)
   #[serde(skip)]
   pub view_size: Vec2i,
   /// The offset to the start of the window in pixels (derived)
   #[serde(skip)]
   pub view_offset: Vec2i,
}

impl View {
   /// Create a view which fills the render target with a constant aspect ratio
   pub fn fill(width: i32, height: i32, surface: *mut Frame) -> View {
      assert!(width > 0);
      assert!(height > 0);
      let mut view = View {
         world: bounds((0.0, width as f32), (0.0, height as f32)),
         id: 1,
         target_ratio: Some(width as f32 / height as f32),
         surface_size: vec2i(0, 0),
         view_size: vec2i(0, 0),
         view_offset: vec2i(0, 0),
         surface,
      };
      view.update_dimensions();
      view
   }

   /// Update view transforms for resized surfaces.
   pub fn update_dimensions(&mut self) {
      let (w, h) = unsafe { self.surface().get_dimensions() };

      self.surface_size = vec2i(w as i32, h as i32);

      if let Some(target) = self.target_ratio {
         let ratio = w as f32 / h as f32;

         let mut scale_x = 1.0;
         let mut scale_y = 1.0;
         let mut offset_x = 0;
         let mut offset_y = 0;

         if ratio > target {
            scale_x = target / ratio;
            offset_x = (w as f32 * (1.0 - scale_x) / 2.0) as i32;
         } else {
            scale_y = ratio / target;
            offset_y = (h as f32 * (1.0 - scale_y) / 2.0) as i32;
         }

         self.view_size.x = (w as f32 * scale_x) as i32;
         self.view_size.y = (h as f32 * scale_y) as i32;
         self.view_offset.x = offset_x;
         self.view_offset.y = offset_y;
      } else {
         self.view_size = self.surface_size;
         self.view_offset = vec2i(0, 0);
      }
   }

   /// Get the view's target surface. Assumes the pointer is valid 
   pub unsafe fn surface(&self) -> &mut Frame {
      assert!(!self.surface.is_null());
      &mut *self.surface
   }

   /// Get the OpenGL viewport rect for the view.
   pub fn viewport(&self) -> glium::Rect {
      glium::Rect {
         left: self.view_offset.x as u32,
         bottom: self.view_offset.y as u32,
         width: self.view_size.x as u32,
         height: self.view_size.y as u32,
      }
   }

   /// Get the transform from normalized device coordinates to view-space. 
   pub fn screen_to_view(&self) -> Mat3 {
      let scale = self.surface_size.to_f32() / self.view_size.to_f32();
      let offset = self.view_offset.to_f32() / self.view_size.to_f32();
      Mat3::translate(-offset) * Mat3::scale(scale)
   }

   /// Get the transform from normalized device coordinates to world-space. 
   pub fn screen_to_world(&self) -> Mat3 {
     Mat3::translate(self.world.min()) * 
        Mat3::scale(self.world.size()) * 
        Mat3::translate(vec2(0.0, 1.0)) * Mat3::flip_y() * 
        self.screen_to_view() 
   }
}

/// Recalculate view dimensions from the surface size.
pub fn update_view_dimensions(views: &mut Store<View>) {
   for v in views { v.update_dimensions() }
}

/// Draw all the meshes with some material.
pub fn draw_meshes<M: Clone>(
   entities: &mut Subscriptions,
   transform: &Store<Transform>,
   material: &Store<M>,
   mesh: &Store<Mesh<<&'static M as Material>::Vertex>>,
   view: &Store<View>,
   program: &<&'static M as Material>::Program,
) where for<'b> &'b M: Material
{
   let views: Vec<&View> = view.iter().map(|(_,v)| v).collect();

   for e in entities.iter::<(Transform, M, Mesh<_>)>((transform, material, mesh)) {
      Drawable::borrowed(transform[e].to_matrix(), &material[e], &mesh[e])
         .draw(&views, program);
   }
}

pub struct Drawable<'a, 'b, M: Clone + 'static> where for<'m> &'m M: Material {
   pub transform: Mat3,
   pub material: Cow<'a, M>,
   pub mesh: Cow<'b, Mesh<<&'static M as Material>::Vertex>>,
}

impl<M: Clone> Drawable<'static, 'static, M> where for<'m> &'m M: Material {
   pub fn owned(transform: Mat3, material: M, mesh: Mesh<<&'static M as Material>::Vertex>) -> Self {
      Drawable {
         transform,
         material: Cow::Owned(material),
         mesh: Cow::Owned(mesh)
      }
   }
}

impl<'a, 'b, M: Clone> Drawable<'a, 'b, M> where for<'m> &'m M: Material {
   pub fn borrowed(transform: Mat3, material: &'a M, mesh: &'b Mesh<<&'static M as Material>::Vertex>) -> Self {
      Drawable {
         transform,
         material: Cow::Borrowed(material),
         mesh: Cow::Borrowed(mesh)
      }
   }

   pub fn draw(&self, views: &[&View], program: &<&'static M as Material>::Program) {
      let buf = VertexBuffer::new(facade(), &self.mesh.vertices.data).unwrap();

      for view in views {
         if self.mesh.mask & view.id == 0 {
            continue;
         }
         let uniforms = self.material.uniforms(Mat3::view(view.world) * self.transform);
         let params = DrawParameters {
            viewport: Some(view.viewport()),
            .. self.material.params()
         };
         let idx = IndicesSource::NoIndices { primitives: self.mesh.primitives };
         unsafe {
            view.surface().draw(&buf, idx, program, &uniforms, &params).unwrap();
         }
      }
   }
}

mod detail {
   use serde::*;
   use glium::index::PrimitiveType;

   #[derive(Serialize, Deserialize)]
   #[serde(remote = "PrimitiveType")]
   pub enum PrimitiveTypeDef {
      Points,
      LinesList,
      LinesListAdjacency,
      LineStrip,
      LineStripAdjacency,
      LineLoop,
      TrianglesList,
      TrianglesListAdjacency,
      TriangleStrip,
      TriangleStripAdjacency,
      TriangleFan,
      Patches { vertices_per_patch: u16, },
   }
}
