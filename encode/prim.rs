//! `Encoding`s for primitive types.

use crate::encode::*;
use crate::error::*;
use log::*;
use std::convert::*;

/// Calculate the number of bits needed to store a given value.
pub fn bits(v: u32) -> u64 {
   if v == 0 {
      return 1;
   }
   32 - (v - 1).leading_zeros() as u64
}

/// A rule to encode a String, using a certain numeric type to predeclare its length.
pub struct StringRule {
   is_long: bool,
}

impl StringRule {
   pub fn long() -> StringRule {
      StringRule { is_long: true }
   }
   pub fn short() -> StringRule {
      StringRule { is_long: false }
   }
}

impl Encoding<String> for StringRule {
   fn read(&self, reader: &mut BitReader) -> Result<String> {
      let len = need_data(if self.is_long {
         reader.read::<u32>()
      } else {
         reader.read::<u16>().map(|x| x as u32)
      })?;

      let s = ((0..len).map(|_| need_data(reader.read::<u8>())).collect(): Result<Vec<u8>>)?;
      Ok(String::from_utf8(s)?)
   }
   fn write(&self, writer: &mut BitWriter, value: &String) -> Result<()> {
      if self.is_long {
         writer.write::<u32>(value.len() as u32);
      } else {
         writer.write::<u16>(value.len() as u16);
      }
      for b in value.bytes() {
         writer.write(b)
      }
      Ok(())
   }
}

/// Rule to encode a `Bitable` value through its `Bitable` impl.
pub struct BitableRule;

impl<T> Encoding<T> for BitableRule
where
   T: Bitable,
{
   fn read(&self, reader: &mut BitReader) -> Result<T> {
      need_data(reader.read())
   }
   fn write(&self, writer: &mut BitWriter, value: &T) -> Result<()> {
      Ok(writer.write(*value))
   }
}

/// Rule to encode a primitive using a given number of bits.
pub struct BitsRule(pub u64);

macro_rules! bits_rule_impl {
   ($t:ty) => {
      impl Encoding<$t> for BitsRule {
         fn read(&self, reader: &mut BitReader) -> Result<$t> {
            Ok(need_data(reader.read_raw(self.0))? as $t)
         }
         fn write(&self, writer: &mut BitWriter, value: &$t) -> Result<()> {
            Ok(writer.write_raw(self.0, *value as u64))
         }
      }
   };
}

bits_rule_impl!(usize);
bits_rule_impl!(u8);
bits_rule_impl!(u16);
bits_rule_impl!(u32);
bits_rule_impl!(u64);

// TODO: different impl for signed types which can fail properly?
bits_rule_impl!(isize);
bits_rule_impl!(i8);
bits_rule_impl!(i16);
bits_rule_impl!(i32);
bits_rule_impl!(i64);

/// Rule for reading all remaining data in a reader, and writing that exact data.
pub struct AllDataRule;

impl Encoding<(Vec<u8>, u64)> for AllDataRule {
   fn read(&self, reader: &mut BitReader) -> Result<(Vec<u8>, u64)> {
      let len = (reader.len() as u64 * 8) - reader.pos() as u64;
      let data = ExactDataRule(len).read(reader)?;
      Ok((data, len))
   }
   fn write(&self, writer: &mut BitWriter, value: &(Vec<u8>, u64)) -> Result<()> {
      ExactDataRule(value.1).write(writer, &value.0)
   }
}

/// Rule for encoding some data (`Vec<u8>`) using an exact number of bits.
pub struct ExactDataRule(pub u64);

impl Encoding<Vec<u8>> for ExactDataRule {
   fn read(&self, reader: &mut BitReader) -> Result<Vec<u8>> {
      let mut data = Vec::<u8>::new();
      let mut bits = self.0;
      while bits > 8 {
         data.push(BitableRule.read(reader)?);
         bits -= 8;
      }
      if bits > 0 {
         data.push(need_data(reader.read_raw(bits))? as u8);
      }
      Ok(data)
   }

   fn write(&self, writer: &mut BitWriter, value: &Vec<u8>) -> Result<()> {
      let mut i = 0;
      let mut bits = self.0;
      while bits > 8 {
         if i > value.len() {
            bail!("Expected more data, but none remains to be read")
         }
         writer.write_raw(8, value[i] as u64);
         bits -= 8;
         i += 1;
      }
      if bits > 0 {
         if i > value.len() {
            bail!("Expected more data, but none remains to be read")
         }
         writer.write_raw(bits, value[i] as u64);
      }
      Ok(())
   }
}

/// Rule to encode an integer with a certain range. Uses the minimum bits necessary.
#[derive(Clone, Copy)]
pub struct IntRule {
   pub min: i32,
   pub max: i32,
}

pub fn ranged_int(min: i32, max: i32) -> IntRule {
   assert!(min < max);
   IntRule::new(min, max)
}

impl IntRule {
   pub fn new(min: i32, max: i32) -> IntRule {
      IntRule { min, max }
   }
   pub fn bits(&self) -> u64 {
      bits((self.max - self.min) as u32 + 1)
   }
   fn assert_range(&self) {
      assert!(self.max >= self.min, "Bad IntRule range.");
   }
   fn check_value(&self, value: i32) {
      if value < self.min || value > self.max {
         warn!("RangedInt {} out of range {}..{}", value, self.min, self.max);
      }
   }
}

impl<T> Encoding<T> for IntRule
where
   T: Copy,
   T: TryFrom<i32>,
   T: TryInto<i32>,
   <T as TryInto<i32>>::Error: ::std::fmt::Debug,
   <T as TryFrom<i32>>::Error: ::std::fmt::Debug,
{
   fn read(&self, reader: &mut BitReader) -> Result<T> {
      self.assert_range();
      let v: u32 = BitsRule(self.bits()).read(reader)?;
      let v = v as i32 + self.min;
      self.check_value(v);
      let v: T = v.try_into().unwrap();
      Ok(v)
   }
   fn write(&self, writer: &mut BitWriter, value: &T) -> Result<()> {
      self.assert_range();
      let v: i32 = (*value).try_into().unwrap();
      self.check_value(v);
      BitsRule(self.bits()).write(writer, &((v - self.min) as u32))?;
      Ok(())
   }
}

/// Rule to encode a floating point value as an integer with a certain scale and
/// range. Uses the minimum bits necessary.
#[derive(Clone, Copy)]
pub struct FloatRule {
   pub min: f32,
   pub max: f32,
   pub scale: f32,
}

pub fn ranged_float(min: f32, max: f32, scale: f32) -> FloatRule {
   assert!(min < max);
   FloatRule::new(min, max, scale)
}

pub fn angle_rule(scale: f32) -> FloatRule {
   ranged_float(-180.0, 180.0, scale)
}

impl FloatRule {
   pub fn new(min: f32, max: f32, scale: f32) -> FloatRule {
      FloatRule { min, max, scale }
   }
   fn check_value(&self, value: f32) {
      if value < self.min || value > self.max {
         warn!("RangedFloat {} out of range {}..{}", value, self.min, self.max);
      }
   }
   fn int_rule(&self) -> IntRule {
      IntRule::new((self.min * self.scale) as i32, (self.max * self.scale) as i32)
   }
}

impl Encoding<f32> for FloatRule {
   fn read(&self, reader: &mut BitReader) -> Result<f32> {
      let v: i32 = self.int_rule().read(reader)?;
      let v = v as f32 / self.scale;
      self.check_value(v);
      Ok(v)
   }
   fn write(&self, writer: &mut BitWriter, v: &f32) -> Result<()> {
      self.check_value(*v);
      self.int_rule().write(writer, &((*v * self.scale) as i32))
   }
}

#[test]
/// String encoding works.
fn strings() {
   let rule = StringRule::short();
   let vec = encode(&rule, &"hey".to_owned()).unwrap();
   assert_eq!(vec.len(), 2 + 3); // 2 the i16, 3 for the 3 u8 characters
   assert_eq!(decode(&rule, &vec).unwrap(), "hey")
}
