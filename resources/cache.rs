use std::collections::HashMap;
use std::collections::BTreeMap;
use std::sync::Arc;
use std::path::*;
use antidote::Mutex;
use serde_value::Value;
use ron::de::from_bytes;
use log::*;

use crate::resources::*;
use crate::error::*;
use crate::util::any_map::*;

/// A cache of resources from a single source
pub struct ResCache {
   pub source: Arc<dyn Source>,
   pub map: Mutex<AnyMap<InnerCache<u8>>>,
   pub manifest: BTreeMap<String, (PathBuf, Value)>,
}

/// A subdirectory of a source.
pub struct SubDirectory<'a> {
   pub source: &'a dyn Source,
   pub root: std::path::PathBuf,
}

impl<'a> Source for SubDirectory<'a> {
   /// Load a file into memory.
   fn load(&self, path: &str) -> Result<Vec<u8>> {
      let path = self.root.join(std::path::Path::new(path));
      self.source.load(path.to_str().unwrap())
   }
}

type InnerCache<T> = HashMap<String, Res<T>>;

impl ResCache {
   /// Create a new `ResCache` with a given source.
   pub fn with_source(source: impl Source + 'static) -> ResCache {
      ResCache {
         source: Arc::new(source),
         map: Mutex::new(AnyMap::default()),
         manifest: BTreeMap::new(),
      }
   }

   /// Attempt to load a manifest file at the given path.
   pub fn load_manifest(&mut self, path: impl AsRef<Path>) -> Result<()> {
      let path = path.as_ref();
      debug!("Loading manifest {:?}", path);
      let directory = path.parent().expect("Invalid manifest path").to_owned();
      let data = self.source.load(&path.to_string_lossy())?;
      let new: BTreeMap<String, Value> = from_bytes(&data)?;

      for (k, v) in new {
         self.manifest.insert(k, (directory.clone(), v));
      }

      if let Some((_, v)) = self.manifest.remove("include") {
         let files: Vec<String> = v.deserialize_into()?;
         for include in &files {
            let path = directory.join(include);
            self.load_manifest(&path)?;
         }
      }

      Ok(())
   }

   /// Attempt to load a resource.
   /// Returns `None` if the resource isn't in the manifest.
   /// Returns `Some(Err(..))` if the resource failed to load.
   pub fn get<T: Resource + 'static>(&self, id: &str) -> Option<Result<Res<T>>> {
      // Check if the value is already in the cache.
      match self.with_cache(|c| c.get(id).cloned()) {
         Some(value) => {
            // If so, return it.
            Some(Ok(value))
         },
         None => {
            debug!("Loading resource '{}'", id);

            // If not, look in manifest.
            if let Some((root, v)) = self.manifest.get(id) {
               // Parse the descriptor
               let descriptor = match v.clone().deserialize_into() {
                  Ok(v) => v,
                  Err(e) => return Some(Err(e.into())),
               };
               // Get the subdirectory of the manifest file which contains
               // the resource to load files relative to the manifest.
               let source = SubDirectory {
                  source: &*self.source,
                  root: root.clone(),
               };
               // Attempt to load the resource
               let value = match T::load(&source, descriptor) {
                  Ok(v) => v,
                  Err(e) => return Some(Err(e)),
               };
               let ptr = Res(Arc::new((value, id.to_string())));
               // Insert into the cache.
               self.with_cache(|c| c.insert(id.to_string(), ptr.clone()));
               Some(Ok(ptr))
            } else {
               // Not in the manifest.
               None
            }
         }
      }
   }

   /// Load some resource using the given descriptor rather than searching the manifest.
   pub fn get_with<T: Resource + 'static>(&self, id: &str, descriptor: T::Descriptor) -> Result<Res<T>> {
      match self.with_cache(|c| c.get(id).cloned()) {
         Some(value) => Ok(value),
         None => {
            let ptr = Res(Arc::new((T::load(&*self.source, descriptor)?, id.to_string())));
            self.with_cache(|c| c.insert(id.to_string(), ptr.clone()));
            Ok(ptr)
         }
      }
   }

   /// Free unused resources.
   pub fn clean(&mut self) {
      unimplemented!();
   }

   /// Get the ResCache for type T
   fn with_cache<T: 'static, R, F: FnMut(&mut InnerCache<T>) -> R>(&self, mut f: F) -> R {
      let mut map = self.map.lock();
      if let Some(item) = map.get::<InnerCache<T>>() {
         return f(item);
      }
      let item = map.insert(InnerCache::<T>::default());
      f(item)
   }
}
