//! A framework for 2D networked games.

#![feature(type_ascription)]
#![feature(specialization)]
#![feature(test)]
#![feature(nll)]
#![feature(decl_macro)]
#![feature(const_fn)]
#![feature(core_intrinsics)]
#![feature(ptr_offset_from)]
#![feature(trait_alias)]
#![feature(type_alias_impl_trait)]
#![feature(maybe_uninit_ref)]
#![feature(maybe_uninit_extra)]
#![feature(unsize)]
#![feature(coerce_unsized)]
#![feature(trace_macros)]
#![feature(unsized_locals)]
#![feature(fn_traits)]
#![feature(unboxed_closures)]
#![feature(optin_builtin_traits)]

pub mod encode;
pub mod geom;
pub mod render;
pub mod resources;
pub mod storage;
pub mod input;
pub mod util;
pub mod window;
pub mod net;
//pub mod ui;

pub use esc_codegen as codegen;

pub mod error {
   //! Re-exports `failure` and aliases `Result<T>` to `Result<T, failure::Error>`.
   pub use failure::*;
   pub type Result<T> = ::std::result::Result<T, Error>;
}
