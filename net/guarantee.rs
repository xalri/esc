//! Vocabulary of types used in the guarantee system.

use crate::encode::*;
use crate::error::*;
use crate::net::seq::*;
use std::fmt::Debug;
use std::net::*;

/// A guaranteed data type.
#[derive(Clone, Debug, PartialEq)]
pub struct Guaranteed<T>
where
   T: Debug,
{
   pub addr: SocketAddr,
   pub guarantee: GuaranteeLevel,
   pub guarantee_data: Option<GuaranteeData>,
   pub data: Option<T>,
}

impl<T> Guaranteed<T>
where
   T: Debug,
{
   /// Create a new message with the given delivery guarantee
   pub fn new_guarantee(addr: SocketAddr, guarantee: GuaranteeLevel, data: T) -> Guaranteed<T> {
      Guaranteed {
         addr,
         guarantee,
         data: Some(data),
         guarantee_data: None,
      }
   }

   /// Create a new message with no delivery guarantees
   pub fn new(addr: SocketAddr, data: T) -> Guaranteed<T> {
      Guaranteed::new_guarantee(addr, GuaranteeLevel::None, data)
   }

   /// Create a new message with guaranteed delivery
   pub fn new_deliver(addr: SocketAddr, data: T) -> Guaranteed<T> {
      Guaranteed::new_guarantee(addr, GuaranteeLevel::Delivery, data)
   }

   /// Create a message with the guarantee that the recipient will get the message
   /// in the order it was sent relative to other ordered messages.
   pub fn new_order(addr: SocketAddr, data: T) -> Guaranteed<T> {
      Guaranteed::new_guarantee(addr, GuaranteeLevel::Order, data)
   }

   /// Create a new ping message
   pub fn new_ping(addr: SocketAddr, data: T) -> Guaranteed<T> {
      Guaranteed::new_guarantee(addr, GuaranteeLevel::Ping, data)
   }

   /// Create a new message in reply to this one.
   pub fn reply(&self, data: T) -> Guaranteed<T> {
      Guaranteed {
         addr: self.addr,
         guarantee: self.guarantee,
         guarantee_data: None,
         data: Some(data),
      }
   }

   pub fn internal_respond(&self, data: GuaranteeData) -> Guaranteed<T> {
      Guaranteed {
         addr: self.addr,
         guarantee: self.guarantee,
         guarantee_data: Some(data),
         data: None,
      }
   }

   /// Returns true if the message has guaranteed delivery
   pub fn is_guaranteed(&self) -> bool {
      self.guarantee == GuaranteeLevel::Delivery || self.guarantee == GuaranteeLevel::Order
   }

   /// Get the message's address (recipient for outgoing messages, sender for
   /// incoming messages)
   pub fn addr(&self) -> SocketAddr {
      self.addr
   }

   /// Get the message's data.
   pub fn data(&self) -> &T {
      self.data.as_ref().unwrap()
   }
}

pub struct GuaranteeLevelRule;

/// The type of guarantee a packet's transmission requires.
#[derive(Debug, Clone, Copy, PartialEq, Encode)]
#[rule("GuaranteeLevelRule")]
pub enum GuaranteeLevel {
   /// No guarantee.
   None,
   /// Make sure it arrives.
   Delivery,
   /// Make sure it arrives in the order it was sent relative to other packets with this flag.
   Order,
   /// For pings I guess.
   Ping,
}

pub struct GuaranteeDataRule;

/// Protocol flag sent with every packet.
#[derive(Debug, Clone, Copy, PartialEq, Encode)]
#[rule("GuaranteeDataRule")]
pub enum GuaranteeData {
   Open(#[rule("OpenDataRule")] OpenData),
   OpenAck(#[rule("BitableRule")] i32),
   Data(#[rule("SeqSmallRule")] SeqSmall),
   DataAck(#[rule("SeqSmallRule")] SeqSmall),
}

pub struct OpenDataRule;

/// Data sent as part of a request to open a connection
#[derive(Debug, Clone, Copy, PartialEq, Encode)]
#[rule("OpenDataRule")]
pub struct OpenData {
   #[rule("BitableRule")]
   pub id: i32,
   #[rule("BitableRule")]
   pub is_open: bool,
   #[rule("SeqSmallRule")]
   pub seq: SeqSmall,
}

/// The `GuaranteeData` doesn't exist if the `GuaranteeLevel` doesn't require
/// controlled transmission. The part of the message that holds the
/// `Option<GuaranteeData>` is, therefore, read conditionally.
pub struct GuaranteeDataOptionRule(pub GuaranteeLevel);

impl Encoding<Option<GuaranteeData>> for GuaranteeDataOptionRule {
   fn read(&self, reader: &mut BitReader) -> Result<Option<GuaranteeData>> {
      Ok(match self.0 {
         GuaranteeLevel::Delivery | GuaranteeLevel::Order => Some(GuaranteeDataRule.read(reader)?),
         _ => None,
      })
   }
   fn write(&self, writer: &mut BitWriter, value: &Option<GuaranteeData>) -> Result<()> {
      if let Some(ref data) = *value {
         GuaranteeDataRule.write(writer, data)?;
      }
      Ok(())
   }
}

#[derive(Clone, Copy)]
pub struct GuaranteedRule<T> {
   pub data_rule: T,
}

impl<T: Debug, E> Encoding<Guaranteed<T>> for GuaranteedRule<E>
where
   E: Encoding<T>,
{
   fn read(&self, reader: &mut BitReader) -> Result<Guaranteed<T>> {
      let level = GuaranteeLevelRule.read(reader)?;
      let header = GuaranteeDataOptionRule(level).read(reader)?;
      let has_data = if let Some(ref h) = header {
         match h {
            GuaranteeData::Data(_) | GuaranteeData::Open(_) => true,
            _ => false,
         }
      } else {
         true
      };

      let data = if has_data {
         Some(self.data_rule.read(reader)?)
      } else {
         None
      };

      Ok(Guaranteed {
         addr: SocketAddr::new(IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0)), 0),
         guarantee: level,
         guarantee_data: header,
         data: data,
      })
   }
   fn write(&self, writer: &mut BitWriter, value: &Guaranteed<T>) -> Result<()> {
      GuaranteeLevelRule.write(writer, &value.guarantee)?;
      GuaranteeDataOptionRule(value.guarantee).write(writer, &value.guarantee_data)?;
      if let Some(ref data) = value.data {
         self.data_rule.write(writer, data)?;
      }
      Ok(())
   }
}
