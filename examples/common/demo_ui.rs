
use esc::geom::*;
use esc::input::*;
use esc::ui::*;
use esc::window::*;
use glium::*;
use nuklear::nk_string;
use std::collections::HashMap;

#[derive(Clone, Copy, Debug)]
pub struct Property {
   min: f32, max: f32, value: f32,
}

pub struct DemoUI {
   ui_controller: Controller<UIAction, UIInputState>,
   convert_config: nuklear::ConvertConfig,
   ctx: nuklear::Context,
   atlas: nuklear::FontAtlas,
   font_idx: usize,
   properties: HashMap<String, Property>,
   drawer: Drawer,
}

impl DemoUI {
   pub fn new(window: &mut Window) -> DemoUI {
      // ===================  Setup UI ======================

      let ui_controller = Controller::new::<UIInputMap>();
      ui_controller.connect(window, 0);

      let mut allo = nuklear::Allocator::new_vec();
      let mut atlas = nuklear::FontAtlas::new(&mut allo);
      let mut drawer = Drawer::new(
         &mut window.display,
         36,
         8 * 1024,
         64 * 1024,
         nuklear::Buffer::with_size(&mut allo, 64 * 1024),
      );

      let mut font_cfg = nuklear::FontConfig::with_size(0.0);
      font_cfg.set_oversample_h(3);
      font_cfg.set_oversample_v(3);
      font_cfg.set_glyph_range(nuklear::font_cyrillic_glyph_ranges());
      font_cfg.set_ttf(include_bytes!("../../examples/common/Roboto-Regular.ttf"));
      font_cfg.set_ttf_data_owned_by_atlas(false);
      font_cfg.set_size(20f32);
      let font_idx = atlas.add_font_with_config(&font_cfg).unwrap();

      let font_tex = {
         let (b, w, h) = atlas.bake(nuklear::FontAtlasFormat::Rgba32);
         drawer.add_texture(&mut window.display, b, w, h)
      };
      let mut null = nuklear::DrawNullTexture::default();
      atlas.end(font_tex, Some(&mut null));

      let ctx = nuklear::Context::new(&mut allo, atlas.font(font_idx).unwrap().handle());

      let mut config = nuklear::ConvertConfig::default();
      config.set_null(null.clone());
      config.set_circle_segment_count(22);
      config.set_curve_segment_count(22);
      config.set_arc_segment_count(22);
      config.set_global_alpha(1.0f32);
      config.set_shape_aa(nuklear::AntiAliasing::On);
      config.set_line_aa(nuklear::AntiAliasing::On);

      DemoUI {
         ui_controller,
         atlas,
         font_idx,
         ctx,
         drawer,
         convert_config: config,
         properties: HashMap::new(),
      }
   }

   pub fn update(&mut self) {
      self.ctx.clear();
      ui_input(&mut self.ui_controller, &mut self.ctx);

      self.ctx.style_set_font(self.atlas.font(self.font_idx).unwrap().handle());
      if self.ctx.begin(
         nk_string!("Parameters"),
         nuklear::Rect { x: 1000.0, y: 50.0, w: 250.0, h: 550.0 },
         nuklear::PanelFlags::Border as nuklear::Flags
            | nuklear::PanelFlags::Title as nuklear::Flags
            | nuklear::PanelFlags::Movable as nuklear::Flags,
      ) {

         for (name, Property{ min, max, value }) in &mut self.properties {
            self.ctx.layout_row(nuklear::LayoutFormat::Dynamic, 22.0, &[0.8f32, 0.20]);
            self.ctx.text(&name, nuklear::TextAlignment::Left as nuklear::Flags);
            let tmp = format!("{:.3}", value);
            self.ctx.text(&tmp, nuklear::TextAlignment::Right as nuklear::Flags);

            self.ctx.layout_row(nuklear::LayoutFormat::Dynamic, 23.0, &[0.05f32, 0.90, 0.05]);
            self.ctx.spacing(1);

            let bar = |ctx: &mut nuklear::Context, min, max, value| -> f32 {
               let from = [min, max];
               let to = [0.0, std::usize::MAX as f32];
               let mut temp = map(value as f32, from, to) as usize;
               ctx.progress(&mut temp, std::usize::MAX, true);
               map(temp as f32, to, from)
            };

            *value = bar(&mut self.ctx, *min, *max, *value);
         }
      }
      self.ctx.end();
   }

   pub fn draw(&mut self,
               res: &UIShader,
               view: Mat3,
               draw_params: DrawParameters,
               frame: &mut impl glium::Surface,
   ) {
      self.drawer.draw(res, view, draw_params, &mut self.ctx, &mut self.convert_config, frame);
   }

   pub fn float_property(&mut self, name: &'static str, range: (f32, f32), default: f32) -> f32 {
      match self.properties.entry(name.to_string()) {
         std::collections::hash_map::Entry::Occupied(mut e) => {
            let Property { min, max, value } = e.get_mut();
            *min = range.0;
            *max = range.1;
            *value = clamp(*value, *min, *max);
            *value
         },
         std::collections::hash_map::Entry::Vacant(e) => {
            e.insert(Property { min: range.0, max: range.1, value: clamp(default, range.0, range.1) });
            default
         }
      }
   }
}



