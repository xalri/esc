//! A protocol for reliable network connections over UDP.
//! Based on Altitude's protocol, but not binary compatible.

pub mod connection;
pub mod guarantee;
pub mod seq;
pub mod sequencer;
pub mod socket;
#[cfg(test)]
mod tests;

pub use crate::util::equiv::*;
