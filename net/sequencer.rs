//! Sequences game events.
//!
//! An event is a discrete change to the game world. Events are ordered, and
//! a sequence of events describe the progression of a world over time.
//! Events include things like players joining, chat messages being sent,
//! a plane using an ability, and so on.
//!
//! Contiguous blocks of events are grouped together into event groups where each
//! event group is (usually) the events created by some source in one tick
//! of the game loop. Event groups have a number, to keep them in order, their sequence
//! number, or seq for short. When clients and servers share event groups they attach
//! the seq to each event group.
//!
//! Each packet is made up of a sequence number for the packet itself, zero to
//! three event groups, and metadata for resending missing events.
//! For each message, the server adds all the new event groups to it's buffer
//! in their raw (non-decoded) form.
//! For each event group, events are read until there is no data left, and applied
//! to the simulation as they're decoded.

use fnv::FnvHashMap as HashMap;
use fnv::FnvHashSet as HashSet;
use log::*;
use std::cmp;
use std::collections::VecDeque;
use std::marker::PhantomData;

use crate::encode::*;
use crate::error::*;
use crate::net::seq::*;
use crate::util::buffer::*;

/// Rule for encoding sequential data.
pub struct SequentialDataRule;

/// A packet of discrete sequenced events.
#[derive(PartialEq, Debug, Clone, Encode)]
#[rule("SequentialDataRule")]
pub struct SequentialData {
   /// The global game counter, reset when the map changes. A server should
   /// discard any messages with a counter who's last bit doesn't match it's own.
   /// Only the least-significant bit is encoded, enough to distinguish
   /// between packets from the current map and previous map for the few seconds
   /// after a map change.
   #[rule("BitsRule(1)")]
   pub counter: u8,
   /// The sequencing number of this packet.
   #[rule("SeqSmallRule")]
   pub seq: SeqSmall,
   /// The most recent event group sequencing number.
   #[rule("SeqSmallRule")]
   pub group_seq: SeqSmall,
   /// Groups of events (at most 4 groups per packet)
   #[rule("VecRule::new(EvGroupRule, 3)")]
   pub ev_groups: Vec<EvGroup>,
   /// A list of missing message IDs (at most 4 IDs per packet)
   #[rule("VecRule::new(SeqSmallRule, 3)")]
   pub missing: Vec<SeqSmall>,
}

/// Rule for encoding event groups.
struct EvGroupRule;

/// A group of game events.
#[derive(PartialEq, Debug, Clone, Encode)]
#[rule("EvGroupRule")]
pub struct EvGroup {
   /// This event group's sequence number
   #[rule("SeqSmallRule")]
   pub seq: SeqSmall,
   /// The actual data
   #[rule("NestedRule")]
   pub data: NestedData,
}

/// A sequencer for the data in game messages
pub struct Sequencer<EvIn, EvOut> {
   // An immutable value which identifies which game this sequencer's
   // events belong to, any messages with an un-matching counter are ignored.
   counter: u8,

   // A buffer of all the event groups we've constructed
   out_buf: Buffer<EvGroup>,
   // A buffer of the event group IDs we sent in each message
   out_msg_buf: Buffer<Vec<i64>>,
   // The IDs of any unsent event groups
   out_ids: VecDeque<i64>,
   // The set of event groups we need to resend
   out_resend: Vec<i64>,
   // Builds the current event group
   out_builder: BitWriter,

   // The sequence of the next message
   out_msg_seq: i64,
   // The sequencing number of the event group we're building
   out_group_seq: i64,

   missing: Vec<Missing>,
   missing_groups: HashSet<i64>,
   last_group_missing: i64,

   // The event group we're dispatching and the current position in bits
   dispatch: Option<(EvGroup, usize)>,
   // A queue of event groups ready to be dispatched
   dispatch_buffer: VecDeque<EvGroup>,
   // The sequence number of the last message we received
   in_seq: i64,
   // The sequence number of the sender's most recently created event group.
   // This isn't necessarily the same as their most recently sent, since at
   // most 3 event groups fit into a single message.
   in_group_seq: i64,
   // The sequence number of the event group we most recently added to the
   // dispatch queue
   in_dispatched: i64,
   // An order buffer for incoming event groups
   in_buffer: HashMap<i64, EvGroup>,

   _phantom_in: PhantomData<EvIn>,
   _phantom_out: PhantomData<EvOut>,
}

/// Metadata about some missing event group, used to decide which missing groups to request in each
/// packet.
#[derive(Debug)]
struct Missing {
   msg: i64,
   group: i64,
   resend_reqs: u64,
   next_req: u64,
}

impl Missing {
   fn new(msg: i64, group: i64) -> Missing {
      Missing {
         msg,
         group,
         resend_reqs: 0,
         next_req: 0,
      }
   }

   fn send(&mut self) -> bool {
      if self.next_req == 0 {
         self.resend_reqs += 1;
         self.next_req = self.resend_reqs;
         true
      } else {
         self.next_req -= 1;
         false
      }
   }
}

impl<EvIn, EvOut> Sequencer<EvIn, EvOut> {
   /// Create a new sequencer with the given counter.
   pub fn new(counter: u8) -> Sequencer<EvIn, EvOut> {
      Sequencer {
         counter,

         out_buf: Buffer::with_capacity(810),
         out_msg_buf: Buffer::with_capacity(270),
         out_ids: VecDeque::with_capacity(10),
         out_resend: Vec::new(),
         out_builder: BitWriter::new(),
         out_msg_seq: 0,
         out_group_seq: 0,

         missing: Vec::new(),
         missing_groups: HashSet::default(),
         last_group_missing: -1,

         dispatch: None,
         dispatch_buffer: VecDeque::new(),
         in_seq: -1,
         in_group_seq: -1,
         in_dispatched: -1,
         in_buffer: HashMap::default(),

         _phantom_in: PhantomData,
         _phantom_out: PhantomData,
      }
   }

   /// Get the sequence number of the most recent message
   pub fn in_seq(&self) -> u64 {
      self.in_seq as u64
   }

   /// Decode the next incoming event.
   pub fn next_ev<R: Encoding<EvIn>>(&mut self, rule: R) -> Option<EvIn> {
      if let Some((ref group, ref mut pos)) = self.dispatch {
         if *pos < group.data.len as usize {
            let mut reader = BitReader::open(&group.data.data[..]);
            reader.seek(*pos);

            if *pos == 0 {
               let b: bool = BitableRule.read(&mut reader).unwrap_or(false);
               if b {
                  panic!("Got fragmented event group -- unimplemented.");
               }
            }

            match rule.read(&mut reader) {
               Ok(e) => {
                  *pos = reader.pos();
                  return Some(e);
               }
               Err(e) => warn!("Failed to read incoming event: {}", e),
            }
         }
      }
      // The block above returns if we have read the next event, and continues
      // to here if we've either finished reading the current group, or we
      // got an error trying to read an event, so next we attempt to call
      // ourselves again with the next group, returning `None` if there are no
      // more groups on the dispatch queue.

      if let Some(group) = self.dispatch_buffer.pop_front() {
         self.dispatch = Some((group, 0));
         self.next_ev(rule)
      } else {
         self.dispatch = None;
         None
      }
   }

   /// Handle an incoming packet, returns true if we have dispatched all the
   /// event groups the sender has sent (meaning it's safe to read real-time
   /// entity data as soon as the new events have been processed)
   pub fn receive(&mut self, data: &mut SequentialData) -> bool {
      if data.counter != self.counter {
         return false;
      }

      let msg_seq = data.seq.get(self.in_seq);
      let group_seq = data.group_seq.get(self.in_group_seq);

      if group_seq > self.in_dispatched {
         for i in (self.in_seq + 1)..msg_seq {
            self.missing.push(Missing::new(i, group_seq));
         }
      }

      self.in_seq = cmp::max(self.in_seq, msg_seq);
      self.in_group_seq = cmp::max(self.in_group_seq, group_seq);

      for group in data.ev_groups.drain(..) {
         let seq = group.seq.get(group_seq);
         let is_new = seq as i64 > self.last_group_missing;
         let was_missing = self.missing_groups.contains(&seq);

         if is_new {
            for i in (self.last_group_missing + 1)..seq {
               self.missing_groups.insert(i);
            }
            self.last_group_missing = seq;
         } else if was_missing {
            self.missing_groups.remove(&seq);
         }

         if is_new || was_missing {
            if seq == self.in_dispatched + 1 {
               self.in_dispatched = seq;
               self.dispatch_buffer.push_back(group);
            } else {
               self.in_buffer.insert(seq, group);
            }

            let mut i = self.in_dispatched + 1;
            while let Some(group) = self.in_buffer.remove(&i) {
               self.dispatch_buffer.push_back(group);
               self.in_dispatched = i;
               i += 1;
            }
         }
      }

      for s in data.missing.drain(..) {
         let mut seq = s.get(self.out_msg_seq);
         debug!("Got resend request, truncated_seq: {:?}, full_seq: {:?}", s, seq);
         loop {
            // Find the closest message which contained some new events, and
            // add those events to the resend queue.
            if let Some(groups) = self.out_msg_buf.get(seq as usize) {
               if !groups.is_empty() {
                  for x in groups {
                     self.out_resend.push(*x);
                  }
                  self.out_resend.sort();
                  self.out_resend.dedup();
                  break;
               }
               seq -= 1;
            } else {
               error!("Got a negative ack for message out of window, {:?}", seq);
               break;
            }
         }
      }
      self.in_dispatched >= group_seq as i64
   }

   /// Build the current event group, adding it to the buffer and incrementing
   /// the event group sequence number
   fn next_group(&mut self) -> Result<()> {
      let group = EvGroup {
         seq: SeqSmall::new(self.out_group_seq),
         data: NestedData::from_writer(&mut self.out_builder),
      };
      debug!(
         "Sending event group: seq: {}, group_seq: {:?}",
         self.out_group_seq, group.seq
      );

      self.out_buf.add(group);
      self.out_ids.push_back(self.out_group_seq);
      self.out_group_seq += 1;

      Ok(())
   }

   /// Add a new event to be sent in the next message
   pub fn add_event<E: Encoding<EvOut>>(&mut self, rule: E, ev: &EvOut) -> Result<()> {
      // Encode the event
      let mut writer = BitWriter::new();
      rule.write(&mut writer, ev)?;
      let pos = writer.pos();
      let data = writer.into_inner();

      // If the current event group is too large to add this event, create a
      // new group and add the current one to the buffer
      if self.out_builder.pos() + pos > 2047 {
         self.next_group()?;
      }

      if self.out_builder.pos() == 0 {
         // each event groups starts with a bool -- true if this event is the
         // start of a segmented event group, which we never create.
         BitableRule.write(&mut self.out_builder, &false)?;
      }

      ExactDataRule(pos as u64).write(&mut self.out_builder, &data)?;
      Ok(())
   }

   /// Create a new message to send.
   pub fn make_msg(&mut self) -> Result<SequentialData> {
      if self.out_builder.pos() > 0 {
         self.next_group()?;
      }

      // Add any event groups we need to resend
      while self.out_ids.len() < 3 && !self.out_resend.is_empty() {
         self.out_ids.push_front(self.out_resend.pop().unwrap());
      }

      // The event groups to send in this message
      let mut ev_groups = Vec::new();
      let mut ev_ids = Vec::new();

      while ev_groups.len() < 3 && !self.out_ids.is_empty() {
         let id = self.out_ids.pop_front().unwrap();

         match self.out_buf.get(id as usize) {
            Some(group) => {
               ev_groups.push(group.clone());
               ev_ids.push(id);
            }
            None => warn!("Can't send event group, not in buffer: group_id: {}", id),
         }
      }

      // Request that the client resend the events which were in messages
      // we're missing.
      let mut missing = Vec::new();
      let mut i = 0;
      while i < self.missing.len() && missing.len() < 4 {
         if self.missing[i].group <= self.in_dispatched {
            debug!("No longer needed missing {:?}", self.missing[i]);
            self.missing.remove(i);
         } else {
            if self.missing[i].send() {
               debug!(
                  "Requesting missing (last_disp {}), {:?}",
                  self.in_dispatched, self.missing[i]
               );
               missing.push(SeqSmall::new(self.missing[i].msg));
            }
            i += 1;
         }
      }

      let group_seq = SeqSmall::new(self.out_group_seq - 1);

      let data = SequentialData {
         counter: self.counter,
         seq: SeqSmall::new(self.out_msg_seq),
         group_seq,
         ev_groups,
         missing,
      };

      self.out_msg_seq += 1;
      self.out_msg_buf.add(ev_ids);
      Ok(data)
   }
}
