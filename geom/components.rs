use super::poly::*;
use super::math::*;
use crate::resources::Res;
use serde::*;

/// A 2D transformation
#[derive(Debug, Clone, Copy)]
#[derive(Serialize, Deserialize)]
#[serde(default)]
pub struct Transform {
   pub position: Vec2,
   pub orientation: f32,
   pub scale: Vec2,
}

impl Transform {
   /// Create a transformation matrix (scale -> rotate -> translate)
   pub fn to_matrix(&self) -> Mat3 {
      Mat3::transform(self.position, self.orientation, self.scale)
   }

   /// Get the combined transformation from first applying `self` then applying `other`
   pub fn then(&self, other: &Transform) -> Transform {
      let a = self.to_matrix();
      let b = other.to_matrix();
      let (position, orientation, scale) = (b * a).decompose();

      Transform { position, scale, orientation }
   }
}

impl Default for Transform {
   fn default() -> Self {
      Transform {
         position: vec2(0.0, 0.0),
         scale: vec2(1.0, 1.0),
         orientation: 0.0,
      }
   }
}

/// A change in position each tick.
#[derive(Debug, Clone, Copy, Default)]
#[derive(Serialize, Deserialize)]
#[serde(default)]
pub struct Velocity {
   pub linear: Vec2,
   pub angular: f32,
   /// Fraction of a tick which has already been integrated in collision response.
   pub relative_time: f32,
}

/// A change in velocity each tick.
#[derive(Debug, Clone, Copy, Default)]
#[derive(Serialize, Deserialize)]
pub struct Acceleration {
   pub linear: Vec2,
   pub angular: f32,
   pub linear_damping_threshold: f32,
   pub linear_damping_power: f32,
   pub angular_damping_threshold: f32,
   pub angular_damping_power: f32,
}

/// A flag to indicate whether to apply dynamics to a body.
/// Static bodies behave the same as bodies with infinite mass &
/// inertia, adding this component allows optimisations in the
/// physics systems.
#[derive(Debug, Clone, Copy, Default)]
#[derive(Serialize, Deserialize)]
pub struct Static;

/// The inverse mass of an entity.
#[derive(Debug, Clone, Copy, Default)]
#[derive(Serialize, Deserialize)]
pub struct Mass {
   pub linear: f32,
   pub angular: f32,
}

impl Mass {
   pub fn from_poly(poly: &Poly, density: f32) -> Mass {
      assert!(density > 0.0);
      let mass = density * poly.area();
      let inertia = mass * poly.inertia();
      Mass { linear: 1.0 / mass, angular: 1.0 / inertia }
   }
}

/// A thing which can collide.
#[derive(Debug, Clone, Default)]
#[derive(Serialize, Deserialize)]
pub struct CollisionMask {
   pub id: u32,
   pub mask: u32,
}

/// Collidable geometry
#[derive(Debug, Clone)]
#[derive(Serialize, Deserialize)]
pub struct CollisionPoly {
   pub hull: Res<Poly>,
}

/// A rigid body, for which collisions are automatically solved.
#[derive(Debug, Clone, Copy)]
#[derive(Serialize, Deserialize)]
#[serde(default)]
pub struct RigidBody {
   pub friction: f32,
   pub elasticity: f32,
}

impl Default for RigidBody {
   fn default() -> Self {
      RigidBody { friction: 0.5, elasticity: 0.5 }
   }
}
