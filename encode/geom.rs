use super::*;
use crate::error::*;
use crate::geom::*;

/// Rule to encode a Vec2 with components in a given range and accuracy
pub struct Vec2Rule {
   pub x: FloatRule,
   pub y: FloatRule,
}

/// Construct a `Vec2Rule` with the same range & scale for both components
pub fn ranged_vec(min: f32, max: f32, scale: f32) -> Vec2Rule {
   Vec2Rule {
      x: ranged_float(min, max, scale),
      y: ranged_float(min, max, scale),
   }
}

pub fn position_vec(size: Vec2, padding: f32, scale: f32) -> Vec2Rule {
   Vec2Rule {
      x: ranged_float(-padding, size.x + padding, scale),
      y: ranged_float(-padding, size.y + padding, scale),
   }
}

impl Encoding<Vec2> for Vec2Rule {
   fn read(&self, reader: &mut BitReader) -> Result<Vec2> {
      Ok(vec2(self.x.read(reader)?, self.y.read(reader)?))
   }
   fn write(&self, writer: &mut BitWriter, v: &Vec2) -> Result<()> {
      self.x.write(writer, &v.x)?;
      self.y.write(writer, &v.y)
   }
}
