use crate::geom::*;
use approx::*;
use derive_more::*;
use serde::*;
use std::cmp::*;
use std::fmt;
use std::ops::*;

#[repr(C)]
#[derive(Clone, Copy, PartialEq, Default)]
#[derive(Neg, Add, Sub, Mul, Div, AddAssign, MulAssign, SubAssign, DivAssign)]
#[derive(Serialize, Deserialize)]
pub struct Vec2 {
   pub x: f32,
   pub y: f32,
}

/// Shorthand Vec2 constructor
pub const fn vec2(x: f32, y: f32) -> Vec2 {
   Vec2 { x, y }
}

/// Compute the scalar cross product of a set of points -- positive when the points
/// are counter-clockwise.
pub fn cross(poly: &[Vec2]) -> f32 {
   let mut p = &poly[poly.len() - 1];
   poly
      .iter()
      .map(|q| {
         let area = p.x * q.y - q.x * p.y;
         p = q;
         area
      })
      .sum()
}

impl Vec2 {
   pub fn zero() -> Vec2 {
      Vec2 { x: 0.0, y: 0.0 }
   }

   pub fn one() -> Vec2 {
      Vec2 { x: 1.0, y: 1.0 }
   }

   /// Check if the components are valid finite floats.
   pub fn valid(&self) -> bool {
      self.x.is_finite() && self.x.abs() < 100_000_000_000.0 &&
      self.y.is_finite() && self.y.abs() < 100_000_000_000.0
   }

   /// Component-wise minimum
   pub fn min(&self, other: Vec2) -> Vec2 {
      vec2(self.x.min(other.x), self.y.min(other.y))
   }

   /// Component-wise maximum
   pub fn max(&self, other: Vec2) -> Vec2 {
      vec2(self.x.max(other.x), self.y.max(other.y))
   }

   /// Component-wise clamp
   pub fn clamp(&self, min: Vec2, max: Vec2) -> Vec2 {
      vec2(clamp(self.x, min.x, max.x), clamp(self.y, min.y, max.y))
   }

   /// Construct a unit vector facing the given angle
   pub fn from_angle(a: f32) -> Vec2 {
      vec2(a.cos(), a.sin())
   }

   /// The squared magnitude of the vector
   pub fn length_sq(&self) -> f32 {
      self.x * self.x + self.y * self.y
   }

   /// The magnitude of the vector
   pub fn length(&self) -> f32 {
      self.length_sq().sqrt()
   }

   /// Get the angle of the vector from the positive x axis in radians.
   pub fn angle(&self) -> f32 {
      if self.x == 0.0 && self.y == 0.0 { return 0.0 }
      self.y.atan2(self.x)
   }

   /// Get a unit vector pointing in the same direction.
   pub fn normalize(&self) -> Vec2 {
      let l = self.length();
      vec2(self.x / l, self.y / l)
   }

   /// Get a unit vector pointing in the same direction
   pub fn normalize_exact(&self) -> Vec2 {
      let len = self.length();
      if len == 0.0 {
         Vec2::zero()
      } else {
         Vec2 {
            x: self.x / len,
            y: self.y / len,
         }
      }
   }

   /// Get the tangent to this vector
   pub fn perpendicular(&self) -> Vec2 {
      Vec2 { x: -self.y, y: self.x }
   }

   /// The inner product (dot product) of two vectors.
   pub fn dot(&self, rhs: Vec2) -> f32 {
      (self.x * rhs.x) + (self.y * rhs.y)
   }

   /// The outer product (perpendicular dot product) of two vectors.
   pub fn perp_dot(&self, rhs: Vec2) -> f32 {
      (self.x * rhs.y) - (self.y * rhs.x)
   }

   /// Invert the vector's y component
   pub fn inv_y(&self) -> Vec2 {
      Vec2 { x: self.x, y: -self.y }
   }

   /// Convert to an array
   pub fn to_arr(&self) -> [f32; 2] {
      [self.x, self.y]
   }

   pub fn recip(&self) -> Vec2 {
      vec2(1.0 / self.x, 1.0 / self.y)
   }
}

#[repr(C)]
#[derive(Clone, Copy, PartialEq, Default)]
#[derive(Neg, Add, Sub, Mul, Div, AddAssign, MulAssign, SubAssign, DivAssign)]
#[derive(Serialize, Deserialize)]
pub struct Vec2i {
   pub x: i32,
   pub y: i32,
}

impl Vec2i {
   /// Convert to an array
   pub fn to_arr(self) -> [i32; 2] {
      [self.x, self.y]
   }

   /// Cast to a Vec2
   pub fn to_f32(self) -> Vec2 {
      Vec2 { x: self.x as f32, y: self.y as f32 }
   }
}

pub fn vec2i(x: i32, y: i32) -> Vec2i {
   Vec2i { x, y }
}

/// Implements basic operators for Vec
macro impl_ops($t:ident, $p:ident) {
   impl Add<$p> for $t {
      type Output = $t;
      fn add(self, rhs: $p) -> $t {
         $t {
            x: self.x + rhs,
            y: self.y + rhs,
         }
      }
   }
   impl AddAssign<$p> for $t {
      fn add_assign(&mut self, rhs: $p) {
         self.x += rhs;
         self.y += rhs
      }
   }

   impl Sub<$p> for $t {
      type Output = $t;
      fn sub(self, rhs: $p) -> $t {
         $t {
            x: self.x - rhs,
            y: self.y - rhs,
         }
      }
   }
   impl SubAssign<$p> for $t {
      fn sub_assign(&mut self, rhs: $p) {
         self.x -= rhs;
         self.y -= rhs
      }
   }

   impl Mul<$t> for $t {
      type Output = $t;
      fn mul(self, rhs: $t) -> $t {
         $t {
            x: self.x * rhs.x,
            y: self.y * rhs.y,
         }
      }
   }
   impl MulAssign<$t> for $t {
      fn mul_assign(&mut self, rhs: $t) {
         self.x *= rhs.x;
         self.y *= rhs.y
      }
   }

   impl Div<$t> for $t {
      type Output = $t;
      fn div(self, rhs: $t) -> $t {
         $t {
            x: self.x / rhs.x,
            y: self.y / rhs.y,
         }
      }
   }
   impl DivAssign<$t> for $t {
      fn div_assign(&mut self, rhs: $t) {
         self.x /= rhs.x;
         self.y /= rhs.y
      }
   }

   // Formatting
   impl fmt::Display for $t {
      fn fmt(&self, f: &mut fmt::Formatter) -> ::std::result::Result<(), fmt::Error> {
         //write!(f, "({:.2}, {:.2})", self.x, self.y)
         write!(f, "({}, {})", self.x, self.y)
      }
   }

   impl fmt::Debug for $t {
      fn fmt(&self, f: &mut fmt::Formatter) -> ::std::result::Result<(), fmt::Error> {
      write!(f, "({}, {})", self.x, self.y)
      }
   }

   // Conversion
   impl From<&$t> for $t {
      fn from(&v: &$t) -> $t {
         v
      }
   }

   impl From<[$p; 2]> for $t {
      fn from([x, y]: [$p; 2]) -> $t {
         $t { x, y }
      }
   }

   impl From<&[$p; 2]> for $t {
      fn from(&[x, y]: &[$p; 2]) -> $t {
         $t { x, y }
      }
   }

   impl From<($p, $p)> for $t {
      fn from((x, y): ($p, $p)) -> $t {
         $t { x, y }
      }
   }

   impl From<&($p, $p)> for $t {
      fn from(&(x, y): &($p, $p)) -> $t {
         $t { x, y }
      }
   }
}

impl_ops!(Vec2, f32);
impl_ops!(Vec2i, i32);

impl From<Vec2i> for Vec2 {
   fn from(x: Vec2i) -> Vec2 {
      vec2(x.x as f32, x.y as f32)
   }
}

impl AbsDiffEq for Vec2 {
   type Epsilon = <f32 as AbsDiffEq>::Epsilon;

   fn default_epsilon() -> Self::Epsilon {
      f32::default_epsilon()
   }

   fn abs_diff_eq(&self, other: &Self, epsilon: Self::Epsilon) -> bool {
      self.x.abs_diff_eq(&other.x, epsilon) && self.y.abs_diff_eq(&other.y, epsilon)
   }
}

impl RelativeEq for Vec2 {
   fn default_max_relative() -> Self::Epsilon {
      f32::default_max_relative()
   }

   fn relative_eq(&self, other: &Self, epsilon: Self::Epsilon, max_relative: Self::Epsilon) -> bool {
      f32::relative_eq(&self.x, &other.x, epsilon, max_relative)
         && f32::relative_eq(&self.y, &other.y, epsilon, max_relative)
   }
}

impl UlpsEq for Vec2 {
   fn default_max_ulps() -> u32 {
      f32::default_max_ulps()
   }

   fn ulps_eq(&self, other: &Self, epsilon: Self::Epsilon, max_ulps: u32) -> bool {
      f32::ulps_eq(&self.x, &other.x, epsilon, max_ulps) && f32::ulps_eq(&self.y, &other.y, epsilon, max_ulps)
   }
}

impl Eq for Vec2 {}

impl PartialOrd for Vec2 {
   fn partial_cmp(&self, other: &Vec2) -> Option<Ordering> {
      if self == other {
         return Some(Ordering::Equal);
      }
      if self.x == other.x {
         return self.y.partial_cmp(&other.y);
      }
      self.x.partial_cmp(&other.x)
   }
}

impl Ord for Vec2 {
   fn cmp(&self, other: &Vec2) -> Ordering {
      self.partial_cmp(other).unwrap_or(Ordering::Equal)
   }
}

#[cfg(test)]
mod test {
   use super::*;

   #[test]
   fn construction() {
      let v = vec2(1.0, 2.0);
      assert_eq!(1.0, v.x);
      assert_eq!(2.0, v.y);
   }

   #[test]
   fn vec_cross() {
      assert_eq!(
         8.0,
         cross(&[vec2(-1.0, -1.0), vec2(1.0, -1.0), vec2(1.0, 1.0), vec2(-1.0, 1.0),])
      );

      assert_eq!(
         -8.0,
         cross(&[vec2(-1.0, -1.0), vec2(-1.0, 1.0), vec2(1.0, 1.0), vec2(1.0, -1.0),])
      );
   }
}
