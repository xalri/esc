use proc_macro2::*;
use syn::*;

pub fn derive_encode_impl(input: ::proc_macro::TokenStream) -> ::proc_macro::TokenStream {
   let ast: DeriveInput = parse(input).unwrap();

   let name = ast.ident;
   let generics = ast.generics;
   let rule = attr(&ast.attrs).unwrap_or("()".to_string());
   let rule: Ident = parse_str(&rule).unwrap();

   match ast.data {
      Data::Struct(s) => encode_struct(s, name, generics, rule),
      Data::Enum(e) => encode_enum(e, name, generics, rule),
      _ => unimplemented!(),
   }
}

// e.g. To implement `Encoding<PlaneState>` for `PlaneStateRule` :
//
//     pub struct PlaneStateRule{ map_size: Vec2 }
//
//     #[derive(Encode)]
//     #[rule("PlaneStateRule")]
//     pub struct PlaneState {
//        #[rule("ranged_float(0.0, 1000.0, 1.0)")]
//        health: f32,
//        #[rule("ranged_vec(-50.0, 50.0, 10.0)")]
//        velocity: Vec2,
//        ...
//    }
pub fn encode_struct(s: DataStruct, name: Ident, generics: Generics, rule: Ident) -> ::proc_macro::TokenStream {
   let read = gen_read(&s.fields, &name);
   let write = gen_write(&s.fields, &quote!(&v.));

   let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();

   quote!(
        impl #impl_generics Encoding<#name #ty_generics> for #rule #where_clause {
            fn read(&self, reader: &mut BitReader) -> Result<#name #ty_generics> {
                let rule = self;
                Ok(#read)
            }
            fn write(&self, writer: &mut BitWriter, v: &#name #ty_generics) -> Result<()> {
                let rule = self;
                #write
                Ok(())
            }
        }
    )
   .into()
}

//    pub struct EventRule;
//
//    #[derive(Encode)]
//    #[rule(EventRule)]
//    enum Event {
//        EvA(#[rule(MyRule)] A, #[rule(SomeRule)] B),
//        EvB {
//            #[rule(AnotherRule)]
//           inner: Inner,
//       },
//       EvC,
//    }
pub fn encode_enum(e: DataEnum, name: Ident, generics: Generics, rule: Ident) -> ::proc_macro::TokenStream {
   let n_variants = e.variants.len();
   let det_bits = bits(n_variants as u32);
   let det_rule = quote!(BitsRule(#det_bits));

   let read_variants = e.variants.iter().enumerate().map(|(i, v)| {
      let read = gen_read(&v.fields, &v.ident);
      quote!(#i => #name::#read)
   });

   let write_variants = e.variants.iter().enumerate().map(|(i, v)| {
      let v_name = &v.ident;
      let write = gen_write(&v.fields, &quote!(f_));
      let pattern = match v.fields {
         Fields::Named(ref fields) => {
            let iter = fields.named.iter().map(|field| {
               let f_name = field.ident.as_ref().unwrap();
               let bound_name: Ident = parse_str(&format!("f_{}", f_name)).unwrap();
               quote!( #f_name: ref #bound_name )
            });
            quote!( #name::#v_name { #(#iter),* } )
         }
         Fields::Unnamed(ref fields) => {
            let iter = fields.unnamed.iter().enumerate().map(|(i, _field)| {
               let bound_name: Ident = parse_str(&format!("f_{}", i)).unwrap();
               quote!( ref #bound_name )
            });
            quote!( #name::#v_name(#(#iter),*) )
         }
         Fields::Unit => quote!( #name::#v_name ),
      };
      quote!(#pattern => {
            #det_rule.write(writer, &#i)?;
            #write
        })
   });

   let read = quote!(
        match #det_rule.read(reader)? {
            #(#read_variants),*,
            x => bail!("Invalid determinant '{}' for '{}'", x, stringify!(#name)),
        }
    );

   let write = quote!(
        match *v {
            #(#write_variants),*
        }
    );

   let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();

   quote!(
        impl #impl_generics Encoding<#name #ty_generics> for #rule #where_clause {
            fn read(&self, reader: &mut BitReader) -> Result<#name #ty_generics> {
                let rule = self;
                Ok(#read)
            }
            fn write(&self, writer: &mut BitWriter, v: &#name #ty_generics) -> Result<()> {
                let rule = self;
                #write
                Ok(())
            }
        }
    )
   .into()
}

/// The number of bits required to encode the determinant of a sum type with `v` variants.
fn bits(v: u32) -> u64 {
   if v == 0 {
      1
   } else {
      32 - (v - 1).leading_zeros() as u64
   }
}

// Generates:
// StructName{ field: rule_a.read(reader)?, ... }
// StructName(rule_a.read(reader)?, ...)
// StructName
// Where rules are taken from field attributes
fn gen_read(fields: &Fields, struct_name: &Ident) -> TokenStream {
   let rule_exprs = rule_exprs(fields);

   match fields {
      Fields::Named(ref fields) => {
         let iter = fields.named.iter().zip(rule_exprs.iter()).map(|(field, rule_expr)| {
            let f_name = field.ident.as_ref().unwrap();
            quote!( #f_name: (#rule_expr).read(reader)? )
         });
         quote!( #struct_name { #(#iter),* } )
      }
      Fields::Unnamed(_) => {
         let iter = rule_exprs.iter().map(|rule_expr| quote!( (#rule_expr).read(reader)? ));
         quote!( #struct_name(#(#iter),*) )
      }
      Fields::Unit => quote!( #struct_name ),
   }
}

// Generates:
// rule_a.write(writer, &v.field)?; ...
// rule_a.write(writer, &v.0)?; ...
// Where rules are taken from field attributes
fn gen_write(fields: &Fields, var_prefix: &TokenStream) -> TokenStream {
   let rule_exprs = rule_exprs(fields);

   match fields {
      Fields::Named(ref fields) => {
         let iter = fields.named.iter().zip(rule_exprs.iter()).map(|(field, rule_expr)| {
            let f_name = field.ident.as_ref().unwrap();
            let derived_name: Expr = parse_str(&format!("{}{}", var_prefix, f_name)).unwrap();
            quote!( (#rule_expr).write(writer, #derived_name)?; )
         });
         quote!( #(#iter)* )
      }
      Fields::Unnamed(_) => {
         let iter = rule_exprs.iter().enumerate().map(|(i, rule_expr)| {
            let derived_name: Expr = parse_str(&format!("{}{}", var_prefix, i)).unwrap();
            quote!( (#rule_expr).write(writer, #derived_name)?; )
         });
         quote!( #(#iter)* )
      }
      Fields::Unit => quote!(),
   }
}

// Gets rule expressions from field attributes
fn rule_exprs(fields: &Fields) -> Vec<Expr> {
   match *fields {
      Fields::Named(ref fields) => fields.named.iter().map(|field| get_rule(&field.attrs)).collect(),
      Fields::Unnamed(ref fields) => fields.unnamed.iter().map(|field| get_rule(&field.attrs)).collect(),
      Fields::Unit => vec![],
   }
}

fn attr(attrs: &[Attribute]) -> Option<String> {
   for a in attrs {
      if a.path.segments.len() != 1 {
         continue;
      }
      let ident = &a.path.segments.iter().next().unwrap().ident;
      if ident == "rule" {
         match a.interpret_meta() {
            Some(Meta::List(list)) => {
               if list.nested.len() != 1 {
                  panic!("Invalid attribute")
               }
               match list.nested.into_iter().next().unwrap() {
                  NestedMeta::Literal(Lit::Str(s)) => return Some(s.value()),
                  _ => panic!("Invalid attribute, expected string literal"),
               }
            }
            _ => panic!("Invalid attribute"),
         }
      }
   }
   None
}

fn get_rule(a: &[Attribute]) -> Expr {
   match attr(a) {
      Some(s) => parse_str(&s).unwrap(),
      None => parse_str("rule").unwrap(),
   }
}
