use darling::*;
use proc_macro2::Span;
use syn::*;

#[derive(FromDeriveInput)]
#[darling(attributes(equiv))]
pub struct EquivOpts {
   remote: String,
}

pub fn derive_equiv_impl(input: ::proc_macro::TokenStream) -> ::proc_macro::TokenStream {
   let ast: DeriveInput = parse(input).unwrap();

   let opts = EquivOpts::from_derive_input(&ast).unwrap();
   let name = ast.ident;
   let remote: Ident = parse_str(&opts.remote).unwrap();

   match ast.data {
      Data::Struct(s) => equiv_struct(s, name, remote),
      Data::Enum(e) => equiv_enum(e, name, remote),
      _ => unimplemented!(),
   }
}

pub fn equiv_enum(e: DataEnum, name: Ident, remote: Ident) -> ::proc_macro::TokenStream {
   for v in &e.variants {
      match v.fields {
         Fields::Unit => (),
         _ => unimplemented!(),
      }
   }

   let variants = e.variants.iter().map(|v| &v.ident).collect::<Vec<_>>();

   let into = variants.iter().map(|var| quote!{ #name::#var => #remote::#var });
   let from = variants.iter().map(|var| quote!{ #remote::#var => #name::#var });

   quote!(
        impl Equiv for #name {
            type Remote = #remote;
            fn into(self) -> #remote { match self { #(#into),* } }
            fn from(other: #remote) -> #name { match other { #(#from),* } }
        }
    )
   .into()
}

pub fn equiv_struct(s: DataStruct, name: Ident, remote: Ident) -> ::proc_macro::TokenStream {
   let fields = match s.fields {
      Fields::Named(fields) => {
         let iter = fields.named.iter().map(|field| {
            let name = field.ident.as_ref().unwrap();
            quote!{ #name: other.#name }
         });
         quote!{ { #(#iter),* } }
      }
      Fields::Unnamed(fields) => {
         let iter = fields.unnamed.iter().enumerate().map(|(i, _field)| {
            let i = Ident::new(&format!("{}", i), Span::call_site());
            quote! { other.#i }
         });
         quote!{ (#(#iter),*) }
      }
      Fields::Unit => quote!(),
   };

   quote!(
        impl Equiv for #name {
           type Remote = #remote;
           fn into(self) -> Self::Remote {
              let other = self;
              #remote #fields
           }
           fn from(other: Self::Remote) -> #name { #name #fields }
        }
     )
   .into()
}
