//! `Encoding`s for container types.

use super::*;
use crate::error::*;
use std::sync::Arc;

type Result<T> = ::std::result::Result<T, Error>;

/// Rule to encode a Box<T>, given an encoding for T.
pub struct BoxRule<E>(pub E);

impl<T, E> Encoding<Box<T>> for BoxRule<E>
where
   E: Encoding<T>,
{
   fn read(&self, reader: &mut BitReader) -> Result<Box<T>> {
      Ok(Box::new(self.0.read(reader)?))
   }
   fn write(&self, writer: &mut BitWriter, value: &Box<T>) -> Result<()> {
      self.0.write(writer, value)
   }
}

/// Some encoded data nested inside another message
#[derive(Debug, Clone, PartialEq)]
pub struct NestedData {
   /// The exact length in bits
   pub len: u16,
   /// The actual data
   pub data: Vec<u8>,
}

impl NestedData {
   /// Move the content of a bitwriter into a new `NestedData`
   pub fn from_writer(w: &mut BitWriter) -> NestedData {
      NestedData {
         len: w.pos() as u16,
         data: w.clear(),
      }
   }

   pub fn empty() -> NestedData {
      NestedData::from_writer(&mut BitWriter::new())
   }

   /// Transform into a reader
   pub fn into_reader<'a>(&'a self) -> BitReader<'a> {
      BitReader::open(&self.data)
   }
}

/// Rule for encoding some nested message with an exact size in bits
pub struct NestedRule;

impl Encoding<NestedData> for NestedRule {
   fn read(&self, reader: &mut BitReader) -> Result<NestedData> {
      let len: u16 = BitableRule.read(reader)?;
      let data = ExactDataRule(len as u64).read(reader)?;
      Ok(NestedData { len, data })
   }

   fn write(&self, writer: &mut BitWriter, value: &NestedData) -> Result<()> {
      BitableRule.write(writer, &value.len)?;
      ExactDataRule(value.len as u64).write(writer, &value.data)
   }
}

/// Rule to encode an Option<T>, given an encoding for T.
pub struct OptionRule<E> {
   encoding: E,
   invert: bool,
}

pub fn optional<E>(rule: E) -> OptionRule<E> {
   OptionRule::new(rule)
}

impl<E> OptionRule<E> {
   pub fn new(encoding: E) -> OptionRule<E> {
      OptionRule {
         encoding: encoding,
         invert: false,
      }
   }
   pub fn new_invert(encoding: E) -> OptionRule<E> {
      OptionRule {
         encoding: encoding,
         invert: true,
      }
   }
}

impl<T, E> Encoding<Option<T>> for OptionRule<E>
where
   E: Encoding<T>,
{
   fn read(&self, reader: &mut BitReader) -> Result<Option<T>> {
      let b: bool = BitableRule.read(reader)?;
      if !b ^ self.invert {
         Ok(Some(self.encoding.read(reader)?))
      } else {
         Ok(None)
      }
   }
   fn write(&self, writer: &mut BitWriter, value: &Option<T>) -> Result<()> {
      BitableRule.write(writer, &(!value.is_some() ^ self.invert))?;
      if let Some(ref x) = *value {
         self.encoding.write(writer, x)?;
      }
      Ok(())
   }
}

/// A rule for encoding an Arc<T> given an encoding for T
pub struct ArcRule<E>(pub E);

impl<T, E> Encoding<Arc<T>> for ArcRule<E>
where
   E: Encoding<T>,
{
   fn read(&self, reader: &mut BitReader) -> Result<Arc<T>> {
      Ok(Arc::new(self.0.read(reader)?))
   }
   fn write(&self, writer: &mut BitWriter, value: &Arc<T>) -> Result<()> {
      self.0.write(writer, &**value)?;
      Ok(())
   }
}

/// A rule to encode a pair of values
pub struct PairRule<A, B>(pub A, pub B);

impl<X, Y, A, B> Encoding<(X, Y)> for PairRule<A, B>
where
   A: Encoding<X>,
   B: Encoding<Y>,
{
   fn read(&self, reader: &mut BitReader) -> Result<(X, Y)> {
      Ok((self.0.read(reader)?, self.1.read(reader)?))
   }
   fn write(&self, writer: &mut BitWriter, value: &(X, Y)) -> Result<()> {
      self.0.write(writer, &value.0)?;
      self.1.write(writer, &value.1)?;
      Ok(())
   }
}

/// A rule to encode a 3-tuple
pub struct TripleRule<A, B, C>(pub A, pub B, pub C);

impl<X, Y, Z, A, B, C> Encoding<(X, Y, Z)> for TripleRule<A, B, C>
where
   A: Encoding<X>,
   B: Encoding<Y>,
   C: Encoding<Z>,
{
   fn read(&self, reader: &mut BitReader) -> Result<(X, Y, Z)> {
      Ok((self.0.read(reader)?, self.1.read(reader)?, self.2.read(reader)?))
   }
   fn write(&self, writer: &mut BitWriter, value: &(X, Y, Z)) -> Result<()> {
      self.0.write(writer, &value.0)?;
      self.1.write(writer, &value.1)?;
      self.2.write(writer, &value.2)?;
      Ok(())
   }
}
