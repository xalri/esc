use smallvec::*;
use crate::geom::wrap_max;

/// A channel of animation with arbitrary data.
#[derive(Debug, Clone)]
pub struct Anim<T> {
   /// Animation frames.
   pub frames: SmallVec<[(f32, T); 8]>,
   /// Current frame
   pub current_frame: usize,
   /// Time accumulator
   pub timer: f32,
}

impl<T> Anim<T> {
   /// Create an animation
   pub fn new(frames: impl Into<SmallVec<[(f32, T);8]>>) -> Anim<T> {
      Anim {
         frames: frames.into(),
         current_frame: 0,
         timer: 0.0,
      }
   }

   /// Get the animations's current frame
   pub fn frame(&self) -> &T {
      &self.frames[self.current_frame].1
   }

   /// Move to a new frame.
   pub fn set_frame(&mut self, frame: usize) {
      self.current_frame = wrap_max(frame, self.frames.len());
      self.timer = 0.0;
   }

   /// Advance the animation by the given time in milliseconds.
   pub fn update(&mut self, msec: f32) {
      let delay = self.frames[self.current_frame].0;
      if delay == 0.0 {
         return;
      }
      self.timer += msec;
      if self.timer >= delay {
         self.timer -= delay;

         let idx = wrap_max(self.current_frame + 1, self.frames.len());
         self.current_frame = idx;
      }
   }
}
