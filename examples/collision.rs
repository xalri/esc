#![feature(unboxed_closures)]
#![feature(unsize)]
#![feature(fn_traits)]

use esc_codegen::*;
use esc::storage::*;
use esc::geom::*;
use esc::resources::*;
use esc::window::*;
use esc::render::*;
use esc::util::hprof;
use esc::util::ref_tuple::*;

use std::f32::consts::PI;

//mod common;
//use self::common::demo_ui::*;
//use esc::ui::UIShader;

/// Component storage.
#[derive(Default, AsRef, AsMut)]
pub struct Storage {
   pub transform: Store<Transform>,
   pub velocity: Store<Velocity>,
   pub mass: Store<Mass>,
   pub acceleration: Store<Acceleration>,
   pub collision_mask: Store<CollisionMask>,
   pub collision_poly: Store<CollisionPoly>,
   pub body: Store<RigidBody>,
   pub bounds: Store<Bounds>,
   pub view: Store<View>,

   pub entities: EntitySet,
   pub subscriptions: Subscriptions,
   pub broadphase: Broadphase,
   pub collision: Collision,
   pub constraints: Constraints,

   pub solid_shader: SolidShader,
   pub tile_shader: TileShader,
   pub debug_font: DebugFont,
   //pub ui_shader: UIShader,
}

impl Storage {
   pub fn clean(&mut self) {
      for &e in &self.entities.released {
         self.transform.remove(e);
         self.velocity.remove(e);
         self.mass.remove(e);
         self.acceleration.remove(e);
         self.collision_poly.remove(e);
         self.collision_mask.remove(e);
         self.body.remove(e);
         self.bounds.remove(e);
         self.view.remove(e);
      }
      self.entities.propegate_released();
   }

   pub fn run<'a, 'b: 'a, Args: RefTuple<'a>, F: Fn<Args>>(&'b mut self, f: F) -> F::Output
      where Self: std::marker::Unsize<<Args as RefTuple<'a>>::Resources>,
   {
      let args = Args::fetch(self);
      Fn::call(&f, args)
   }
}

fn main() {
   let mut window = Window::new("CollisionDemo", vec2i(1280, 720));
   //let mut ui = DemoUI::new(&mut window);
   let mut s = Storage::default();

   // ===================  Setup World ======================

   let view = s.entities.create();
   s.view.insert(view, View::fill(1280, 720, &mut window.frame));

   let pellet_poly = Res::new(Poly::ellipse(7, 20.0, 10.0));

   let floor = s.entities.create();
   s.transform.emplace(floor);
   s.mass.emplace(floor);
   s.body.emplace(floor);
   s.velocity.emplace(floor);
   s.collision_poly.insert(floor, CollisionPoly { hull: Res::new(Poly::rect(600.0, 40.0, true)) });
   s.collision_mask.insert(floor, CollisionMask { id: 0b1, mask: 0b0 });
   s.transform[floor].position = vec2(400.0, 270.0);

   // Counters for spawning pellets
   let mut accum = 0.5;
   let mut count = 0;


   // ===================  Main Loop ======================

   loop {
      hprof::start_frame(); // start profiling
      window.poll_events();
      if window.close_requested { break; }

      // ====================  Update UI =======================

      let pellets_per_frame = 0.5;
      let floor_angle = PI;
      let floor_friction = 0.7;
      let floor_elasticity = 0.1;
      let pellet_scale = 1.0;
      let pellet_friction = 0.8;
      let pellet_elasticity = 0.3;
      /*
      let pellets_per_frame = ui.float_property("Spawn rate", (0.0, 2.0), 0.5);
      let floor_angle = ui.float_property("Floor angle", (0.0, 2.0 * PI), PI);
      let floor_friction = ui.float_property("Floor friction", (0.0, 1.0), 0.7);
      let floor_elasticity = ui.float_property("Floor elasticity", (0.0, 1.0), 0.1);
      let pellet_scale = ui.float_property("Pellet scale", (0.01, 5.0), 1.0);
      let pellet_friction = ui.float_property("Pellet friction", (0.0, 1.0), 0.8);
      let pellet_elasticity = ui.float_property("Pellet elasticity", (0.0, 1.0), 0.3);

      //ui.update();
      */

      s.body[floor].elasticity = floor_elasticity;
      s.body[floor].friction = floor_friction;
      s.transform[floor].orientation = floor_angle;

      // ===================  Update World ======================

      // Spawn new pellets:
      accum += pellets_per_frame;
      while accum > 1.0 {
         accum -= 1.0;
         count += 1;
         if count > 20 {
            count = 0
         }
         let rel = (count as f32 / 5.0 * PI).sin();

         let p = s.entities.create();
         s.transform.emplace(p);
         s.mass.insert(p, Mass::from_poly(&pellet_poly, pellet_scale));
         s.body.emplace(p);
         s.velocity.emplace(p);
         s.acceleration.emplace(p);
         s.collision_poly.insert(p, CollisionPoly { hull: pellet_poly.clone() });
         s.collision_mask.insert(p, CollisionMask { id: 0b01, mask: 0b11 });

         s.transform[p].position     = vec2(400.0 + rel * 100.0, 800.0);
         s.velocity[p].linear        = vec2(-rel * 2.0, -10.0);
         s.transform[p].orientation  = -20.0 + count as f32 * 5.0;
         s.transform[p].scale        = Vec2::one() * pellet_scale * (1.0 + rel * 0.3);
         s.body[p].elasticity        = pellet_elasticity;
         s.body[p].friction          = pellet_friction;
         s.acceleration[p].linear.y  = -0.15;
      }

      // Update world:
      s.run(&integrate_acceleration);
      s.run(&calculate_bounds);
      s.run(&update_broadphase);
      s.run(&find_pairs);
      s.run(&find_contacts);
      s.run(&solve_rigid_body_contacts);
      s.run(&solve_constraints);
      s.run(&integrate_velocity);

      // Remove out of bounds objects
      for e in s.subscriptions.iter::<(Bounds,)>((&mut s.bounds,)) {
         if !s.bounds[e].intersects(bounds((-500.0, 1800.0), (-500.0, 1300.0))) {
            s.entities.release(e);
         }
      }
      s.clean();

      // ===================  Draw ======================

      s.run(&update_view_dimensions);
      s.run(&draw_colliders);

      let font_transform = Mat3::transform(vec2(60.0, 180.0), 0.0, vec2(0.3, 0.3)) * Mat3::flip_y();
      s.debug_font.0
         .build(&hprof::to_string(), font_transform)
         .draw(&[&s.view[view]], &s.tile_shader);

      //ui.draw(&s.ui_shader, Mat3::view(bounds((0.0, window.width()), (0.0, window.height()))), DrawParameters::default(), window.frame());

      window.flip();
      hprof::end_frame();
   }
}
