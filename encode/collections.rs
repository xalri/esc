//! `Encoding`s for uniform collection types.

use super::*;
use crate::error::*;
use log::*;

type Result<T> = ::std::result::Result<T, Error>;

/// Encoding for a variable sized array of values using a given encoding.
pub struct VecRule<E> {
   encoding: E,
   /// The number of bits to use to encode the length of the vec
   bits: u64,
}

impl<E> VecRule<E> {
   pub fn new(encoding: E, bits: u64) -> VecRule<E> {
      VecRule { encoding, bits }
   }
}

impl<T, E> Encoding<Vec<T>> for VecRule<E>
where
   E: Encoding<T>,
{
   fn read(&self, reader: &mut BitReader) -> Result<Vec<T>> {
      let mut vec = Vec::new();
      let len: usize = BitsRule(self.bits).read(reader)?;
      trace!("VecRule::read bits: {}, length: {}", self.bits, len);
      for _ in 0..len {
         let x = match self.encoding.read(reader) {
            Ok(x) => x,
            Err(e) => {
               error!("Array read failed at element {}", vec.len());
               return Err(e);
            }
         };
         vec.push(x);
      }
      Ok(vec)
   }
   fn write(&self, writer: &mut BitWriter, value: &Vec<T>) -> Result<()> {
      BitsRule(self.bits).write(writer, &value.len())?;
      for x in value.iter() {
         self.encoding.write(writer, x)?;
      }
      Ok(())
   }
}

/// Rule to encode a fixed number of values with an arbitrary encoding.
/// TODO: when we have type level integers, make this generic over the length
/// and use a real array.
pub struct ArrayRule<E> {
   encoding: E,
   length: usize,
}

impl<E> ArrayRule<E> {
   pub fn new(encoding: E, length: usize) -> ArrayRule<E> {
      ArrayRule { encoding, length }
   }
}

impl<E, V> Encoding<Vec<V>> for ArrayRule<E>
where
   E: Encoding<V>,
{
   fn read(&self, reader: &mut BitReader) -> Result<Vec<V>> {
      let mut vec = Vec::new();
      for _ in 0..self.length {
         vec.push(self.encoding.read(reader)?);
      }
      Ok(vec)
   }
   fn write(&self, writer: &mut BitWriter, value: &Vec<V>) -> Result<()> {
      if value.len() != self.length {
         bail!(
            "Incorrect array length for ArrayRule ({}, expected {})",
            value.len(),
            self.length
         );
      }

      Ok(for v in value {
         self.encoding.write(writer, v)?
      })
   }
}
