use esc::storage::*;
use esc::window::*;
use esc::render::*;
use esc::input::*;
use esc::resources::*;
use esc::geom::*;
use esc::util::time::*;
use maplit::*;

fn main() {
   simple_logger::init_with_level(log::Level::Debug).unwrap();

   let mut s = Storage::default();

   let view = s.create();
   let surface = s.run(Window::surface);
   s.insert(view, View::fill(1280, 720, surface));
   s.run(|w: &mut Window| w.set_title("breakout"));

   s.run(create_world);

   loop {
      s.run(Window::poll_events);
      if s.run(Window::is_close_requested) { break }
      s.run(update_window_input_sources);

      while s.run(GameTime::tick) {
         s.run(map_controls);
         s.run(move_paddle);
         s.run(integrate_acceleration);
         s.run(calculate_bounds);
         s.run(update_broadphase);
         s.run(find_pairs);
         s.run(find_contacts);
         s.run(solve_rigid_body_contacts);
         s.run(game_physics);
         s.run(solve_constraints);
         s.run(integrate_velocity);

         if s.run(is_dead) {
            s.run(clear_world);
            s.run(create_world);
         }

         s.clean();
      }

      s.run(GameTime::update);
      s.run(update_view_dimensions);
      s.run(draw_meshes::<SolidColour>);
      s.run(draw_colliders);
      s.run(draw_lines);
      s.run(clear_debug_lines);
      s.run(Window::flip);
   }
}

pub struct Block { health: u32 }
pub struct Ball { speed: f32 }
pub struct Paddle { speed: f32 }
pub struct Death {}

fn is_dead(
   constraints: &mut Constraints,
   ball: &mut Store<Ball>,
   death: &mut Store<Death>,
) -> bool {
   for c in &mut constraints.contact {
      if ball.has(c.contact.a) && death.has(c.contact.b) {
         return true;
      }
   }
   false
}

fn game_physics(
   constraints: &mut Constraints,
   ball: &mut Store<Ball>,
   transform: &mut Store<Transform>,
   velocity: &mut Store<Velocity>,
   block: &mut Store<Block>,
   paddle: &mut Store<Paddle>,
   colour: &mut Store<SolidColour>,
   entities: &mut EntitySet,
) {
   for c in &mut constraints.contact {
      let a = c.contact.a;
      let b = c.contact.b;
      if ball.has(a) {
         if paddle.has(b) {
            let relative_contact = c.contact.contact_a - transform[b].position;
            let n = &mut c.contact.intersection.normal;
            *n = (*n + vec2(relative_contact.x / 200.0, 0.0)).normalize();
         }

         if block.has(b) {
            if block[b].health == 0 {
               entities.release(b);
            } else {
               block[b].health -= 1;
               colour[b].colour = block_colour(block[b].health as f32 / 10.0);
            }
         }
      }
   }

   // Set the ball to constant speed
   for (id, b) in ball.iter() {
      velocity[id].linear = velocity[id].linear.normalize() * b.speed;
   }
}

fn move_paddle(
   subscription: &mut Subscriptions,
   velocity: &mut Store<Velocity>,
   paddle: &mut Store<Paddle>,
   controller: &mut Store<Controller>,
   time: &GameTime,
) {
   for e in subscription.iter::<(Paddle,Controller)>((paddle, controller)) {
      let speed = paddle[e].speed;
      velocity[e].linear.x = 0.0;
      if controller[e].is_pressed("left", time.tick) {
         velocity[e].linear.x = -speed;
      }
      if controller[e].is_pressed("right", time.tick) {
         velocity[e].linear.x = speed;
      }
   }
}

fn clear_world(
   e: &mut EntitySet,
   transform: &Store<Transform>,
) {
   for (id, _) in transform.iter() { e.release(id) }
}

fn create_world(
   e: &mut EntitySet,
   body: &mut Store<RigidBody>,
   mass: &mut Store<Mass>,
   velocity: &mut Store<Velocity>,
   transform: &mut Store<Transform>,
   mesh: &mut Store<Mesh<Vertex2>>,
   colour: &mut Store<SolidColour>,
   block: &mut Store<Block>,
   ball: &mut Store<Ball>,
   death: &mut Store<Death>,
   paddle: &mut Store<Paddle>,
   controller: &mut Store<Controller>,
   collider: &mut Store<CollisionPoly>,
   collision_mask: &mut Store<CollisionMask>,
) {

   let block_mesh = Res::new(Vertices { data: vec![
      Vertex2 { pos: vec2(-1.0, -1.0), uv: vec2(0.0,0.0) },
      Vertex2 { pos: vec2(1.0, -1.0), uv: vec2(0.0,0.0) },
      Vertex2 { pos: vec2(-1.0, 1.0), uv: vec2(0.0,0.0) },
      Vertex2 { pos: vec2(1.0, 1.0), uv: vec2(0.0,0.0) },
   ]});

   let block_poly = Res::new(Poly::new(vec![
      vec2(-1.0, -1.0),
      vec2(1.0, -1.0), 
      vec2(1.0, 1.0),  
      vec2(-1.0, 1.0), 
   ]));

   let mut create_object = |position: Vec2, scale: Vec2, col: Colour, col_id: u32, mask: u32| {
      let id = e.create();
      transform.insert(id, Transform { position, scale, orientation: 0.0 });
      mesh.insert(id, Mesh {
         vertices: block_mesh.clone(),
         primitives: PrimitiveType::TriangleStrip,
         mask: 1,
      });
      colour.insert(id, SolidColour { colour: col, depth: 0, .. Default::default() });
      collider.insert(id, CollisionPoly { hull: block_poly.clone(), });
      collision_mask.insert(id, CollisionMask { id: col_id, mask });
      velocity.insert(id, Velocity::default());
      mass.insert(id, Mass{ linear: 0.0, angular: 0.0 });
      body.insert(id, RigidBody { friction: 0.0, elasticity: 0.0 });
      id
   };

   // Create blocks
   for x in 0..10 {
      for y in 0..10 {
         let position = vec2(190.0 + x as f32 * 100.0, 440.0 + y as f32 * 20.0);
         let scale = vec2(50.0, 10.0);
         let colour = block_colour(y as f32 / 10.0);
         let id = create_object(position, scale, colour, 0b100, 0b0);
         block.insert(id, Block { health: y });
      }
   }

   // Create bounds
   create_object(vec2(1290.0, 360.0), vec2(10.0, 360.0), Colour::white(), 0b1, 0b0);
   create_object(vec2(-10.0, 360.0),  vec2(10.0, 360.0), Colour::white(), 0b1, 0b0);
   create_object(vec2(1280.0 / 2.0, 730.0), vec2(1280.0 / 2.0, 10.0), Colour::white(), 0b1, 0b0);
   let floor = create_object(vec2(1280.0 / 2.0, -20.0), vec2(1280.0 / 2.0, 10.0), Colour::white(), 0b1, 0b0);
   death.insert(floor, Death{});

   // Create paddle
   let paddle = {
      let position = vec2(640.0, 70.0);
      let scale = vec2(60.0, 14.0);
      let colour = Colour::new(0.9,0.9,0.9,1.0);
      let id = create_object(position, scale, colour, 0b1, 0b1);
      paddle.insert(id, Paddle { speed: 12.0 });
      controller.insert(id, Controller {
         map: hashmap! {
            "left".to_string() => vec![InputMapping { controller: None, input_name: "Left".to_string() }],
            "right".to_string() => vec![InputMapping { controller: None, input_name: "Right".to_string() }],
         },
         .. Default::default()
      });
      id
   };

   // Create ball
   {
      let position = vec2(640.0, 270.0);
      let scale = vec2(8.0, 8.0);
      let colour = Colour::new(0.9,0.9,0.9,1.0);
      let id = create_object(position, scale, colour, 0b10, 0b101);
      mass[id].linear = 1000000.0;
      body[id].elasticity = 2.0;
      velocity[id].linear = vec2(4.0, -10.0);
      ball.insert(id, Ball { speed: 12.0 });
   }

   mass[paddle].linear = 0.00000001;
}

pub fn block_colour(health: f32) -> Colour {
   use rand::prelude::*;
   let range = rand::distributions::Uniform::new(0.85, 0.9);
   let mut rng = rand::thread_rng();

   let s: f32 = range.sample(&mut rng);
   let v: f32 = range.sample(&mut rng);

   HSV::new(health, s, v).to_rgb()
}
