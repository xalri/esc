pub mod decomp;
pub mod triangulate;

use std::f32;
use std::f32::consts::PI;
use std::iter::*;
use std::ops::*;
use std::slice;

use smallvec::*;

use crate::resources::*;
use crate::error::*;
use crate::geom::*;
use super::math::Range;

/// A vector of points with a wrapping indexing operation.
/// All methods assume a clockwise polygon.
#[derive(Default, Debug, Clone, PartialEq, Eq)]
pub struct PolyVec(pub SmallVec<[Vec2; 16]>);

/// An immutable simple 2D polygon.
#[derive(Debug, Clone, PartialEq)]
pub struct Poly {
   inner: ConvexPoly,
   decomposition: Vec<ConvexPoly>,
}

impl Resource for Poly {
   type Descriptor = Vec<Vec2>;
   fn load(_: &dyn Source, vec: Self::Descriptor) -> Result<Self> {
      Ok(Poly::new(vec))
   }
}

/// An immutable convex polygon.
#[derive(Debug, Clone, PartialEq)]
pub struct ConvexPoly {
   points: PolyVec,
   /// Unique normalised separating axes
   pub separating_axes: SmallVec<[Vec2; 16]>,
   /// The poly's axis-aligned bounding box
   pub aabb: Bounds,
   /// The center of the poly's bounding circle
   pub centroid: Vec2,
   /// The radius of the poly's bounding circle
   pub radius: f32,
}

impl Poly {
   /// Create a new `Poly` from an iterator of points, panics if less than
   /// three points are given
   pub fn new<P: Into<Vec2> + Copy>(points: impl IntoIterator<Item = P>) -> Poly {
      let mut points: PolyVec = points.into_iter().map(|p| p.into()).collect();
      if points.len() < 3 {
         panic!("Poly::new requires at least three points")
      }
      if cross(&points) > 0.0 {
         points.reverse();
      }
      points.simplify(0.0001); // Remove collinear points

      unsafe { Poly::new_unchecked(points) }
   }

   /// Create a new `Poly`. Assumed to have at least three points, be right-handed, and have no
   /// adjacent collinear segments.
   pub unsafe fn new_unchecked(points: PolyVec) -> Poly {
      if points.is_convex() {
         Poly {
            inner: ConvexPoly::new(points),
            decomposition: vec![],
         }
      } else {
         Poly {
            decomposition: decomp::convex_partition(&points)
               .into_iter()
               .map(|idx| ConvexPoly::new(idx.into_iter().map(|i| points[i]).collect()))
               .collect(),
            inner: ConvexPoly::new(points),
         }
      }
   }

   /// Create a rectangle with the given dimensions.
   pub fn rect(width: f32, height: f32, center: bool) -> Poly {
      if center {
         let w = width / 2.0;
         let h = height / 2.0;
         Poly::new(&[[w, h], [w, -h], [-w, -h], [-w, h]])
      } else {
         let w = width;
         let h = height;
         Poly::new(&[[w, h], [w, 0.0], [0.0, 0.0], [0.0, h]])
      }
   }

   /// Create a poly from bounds
   pub fn from_bounds(bounds: Bounds) -> Poly {
      Poly::new(&[
         [bounds.x.max, bounds.y.max],
         [bounds.x.max, bounds.y.min],
         [bounds.x.min, bounds.y.min],
         [bounds.x.min, bounds.y.max],
      ])
   }

   /// Approximate an ellipse with the given vertex count & dimensions
   pub fn ellipse(v: u32, width: f32, height: f32) -> Poly {
      if v < 3 {
         panic!("Poly::ellipse requires at least three points")
      }
      Poly::new(
         (0..v)
            .map(|i| (PI / v as f32) + (2.0 * PI * i as f32 / v as f32))
            .map(|angle| (angle.cos() * width / 2.0, angle.sin() * height / 2.0)),
      )
   }

   /// Approximate a sector of a circle with the given radius, angles, and vertex count.
   pub fn sector(v: u32, radius: f32, arc_angle: f32, facing_angle: f32) -> Poly {
      let min_angle = facing_angle - (arc_angle / 2.0);
      Poly::new(
         (0..v)
            .map(|i| min_angle + (i as f32 * (arc_angle / v as f32)))
            .map(|angle| (angle.cos() * radius, angle.sin() * radius))
            .chain(once((0.0, 0.0))),
      )
   }

   /// Create a copy of the poly with the given transformation applied.
   pub fn transformed(&self, t: Mat3) -> Poly {
      Poly::new(self.iter().map(|&point| t * point))
   }

   /// Returns an iterator over the convex components of the poly
   pub fn convex_iter(&self) -> impl Iterator<Item=&ConvexPoly> {
      if self.decomposition.is_empty() {
         ConvexIter::Convex(Some(&self.inner))
      } else {
         ConvexIter::Compound(self.decomposition.iter())
      }
   }

   /// Test if the polygon is convex
   pub fn is_convex(&self) -> bool {
      self.decomposition.is_empty()
   }
}

pub enum ConvexIter<'a> {
   Convex(Option<&'a ConvexPoly>),
   Compound(slice::Iter<'a, ConvexPoly>),
}

impl<'a> Iterator for ConvexIter<'a> {
   type Item = &'a ConvexPoly;
   fn next(&mut self) -> Option<&'a ConvexPoly> {
      match *self {
         ConvexIter::Convex(ref mut opt) => opt.take(),
         ConvexIter::Compound(ref mut i) => i.next(),
      }
   }
}

impl ConvexPoly {
   fn new(mut points: PolyVec) -> ConvexPoly {
      points.simplify(0.0001); // Remove collinear points

      if points.intersects_self() {
         panic!("ConvexPoly::new self-intersection")
      }

      let mut axes = SmallVec::new();
      for a in points.surface_normals() {
         let a = a.normalize();
         let a = if a.y < 0.0 { -a } else { a };
         axes.push(a);
      }
      axes.sort();
      axes.dedup();

      ConvexPoly {
         aabb: Bounds::containing(&points),
         centroid: points.centroid(),
         radius: points.radius(),
         separating_axes: axes,
         points,
      }
   }

   /// The poly's world-space bounding box
   pub fn bounds(&self, t: &Transform) -> Bounds {
      if t.orientation == 0.0 {
         self.aabb * t.scale + t.position
      } else {
         let c = t.position + Mat3::rotate(t.orientation) * (self.centroid * t.scale);
         let r = self.radius * t.scale.x.max(t.scale.y);
         Bounds {
            x: Range { min: c.x - r, max: c.x + r },
            y: Range { min: c.y - r, max: c.y + r },
         }
      }
   }
}

impl Deref for Poly {
   type Target = ConvexPoly;
   fn deref(&self) -> &ConvexPoly {
      &self.inner
   }
}

impl Deref for ConvexPoly {
   type Target = PolyVec;
   fn deref(&self) -> &PolyVec {
      &self.points
   }
}

impl PolyVec {
   /// Test if the poly is self-intersecting..
   pub fn intersects_self(&self) -> bool {
      for i in 0..self.len() {
         let dist_sq = (self[i] - self[i + 1]).length_sq();
         for j in 0..self.len() {
            if j == i || wrap_max(j + 1, self.len()) == i {
               continue;
            }

            if let Some(p) = intersection_point((self[i], self[i + 1]), (self[j], self[j + 1])) {
               if (p - self[i]).length_sq() < dist_sq - 0.00001 {
                  return true;
               }
            }
         }
      }
      false
   }

   /// An iterator over the polygon's segments
   pub fn segments<'a>(&'a self) -> impl Iterator<Item = (Vec2, Vec2)> + 'a {
      (0..self.len()).map(move |i| (self.0[i as usize], self[i + 1]))
   }

   /// An iterator over the surface normals
   pub fn surface_normals<'a>(&'a self) -> impl Iterator<Item = Vec2> + 'a {
      self.segments().map(|(a, b)| vec2(a.y - b.y, b.x - a.x))
   }

   /// An iterator over segments of length 3, with the n'th segment having the n'th vertex in the
   /// center.
   pub fn tris<'a>(&'a self) -> impl Iterator<Item = (Vec2, Vec2, Vec2)> + 'a {
      (0..self.len()).map(move |i| (self[i - 1], self.0[i as usize], self[i + 1]))
   }

   /// An iterator over the vertex normals
   pub fn vertex_normals<'a>(&'a self) -> impl Iterator<Item = Vec2> + 'a {
      // TODO: test
      self
         .tris()
         .map(|(a, b, c)| ((a - b) + (c - b)) * if cross(&[a, b, c]) > 0.0 { 1.0 } else { -1.0 })
   }

   /// Get the index of the start of the poly's longest segment
   pub fn longest_segment(&self) -> isize {
      let mut idx = 0;
      let mut len_sq = 0.0;
      for i in 0..self.len() {
         let x = (self[i] - self[i + 1]).length_sq();
         if x > len_sq {
            len_sq = x;
            idx = i
         }
      }
      idx
   }

   /// Simplify the polygon by removing points which are close to the line connecting their
   /// adjacent points. Panics if epsilon is negative.
   fn simplify(&mut self, e: f32) {
      let len = self.len();
      self.douglas_peucker(0, len - 1, e);
      // Remove the first point if it's collinear with it's adjacent points
      if self.len() > 2 && perpendicular_distance(self[-1], self[1], self[0]) < e {
         self.0.remove(0);
      }
      // Remove the last point if it's collinear with it's adjacent points
      if self.len() > 2 && perpendicular_distance(self[-2], self[0], self[-1]) < e {
         self.0.pop();
      }
      if self.len() < 3 {
         panic!("Poly::simplify removed all points..");
      }
   }

   /// Recursively simplify the polyline a->b using Douglas-Peucker.
   /// Panics if `e` is negative or if b > a.
   fn douglas_peucker(&mut self, a: isize, b: isize, e: f32) {
      if e.is_sign_negative() {
         panic!("douglas_peucker called with negative epsilon");
      }
      if a > b {
         panic!("douglas_peucker invalid range {} -> {}", a, b)
      }

      if b - a < 2 {
         return;
      }

      // Find the point furthest from the line a -> b
      let mut distance = 0.0;
      let mut index = a + 1;

      for i in a + 1..b {
         let d = perpendicular_distance(self[a], self[b], self[i]);
         if d > distance {
            index = i;
            distance = d;
         }
      }

      if distance < e {
         // If all points are all close to the line a -> b we remove them.
         for i in ((a as usize + 1)..(b as usize)).rev() {
            self.0.remove(i);
         }
      } else {
         // Recursively simplify the segments to the left and right of the point furthest
         // from the line. The right half is simplified first to preserve indices.
         self.douglas_peucker(index, b, e);
         self.douglas_peucker(a, index, e);
      }
   }

   /// The number of points.
   pub fn len(&self) -> isize {
      self.0.len() as isize
   }

   /// Project the points onto an axis.
   pub fn project(&self, axis: Vec2, t: Mat2) -> Range {
      assert!(self.len() > 0);
      let p = |p: Vec2| axis.x * (p.x * t.m00 + p.y * t.m10) + axis.y * (p.x * t.m01 + p.y * t.m11);
      let mut min = p(self[0]);
      let mut max = min;
      for i in 1..self.len() {
         let v = p(self[i]);
         if v > max { max = v }
         if v < min { min = v }
      }
      Range { min, max }
   }

   /// Get the intersection distance with the given ray
   pub fn raycast(&self, t: Mat3, pos: Vec2, dir: Vec2) -> f32 {
      let mut dist = std::f32::INFINITY;
      for s in self.segments() {
         let s = (t * s.0, t * s.1);

         if let Some(v) = intersection_point(s, (pos, pos + dir * 1000.0)) {
            dist = dist.min((pos - v).length())
         }
         //dist = dist.min(ray_segment(s, pos, dir));
      }
      dist
   }

   /// The average of the points.
   pub fn centroid(&self) -> Vec2 {
      self
         .iter()
         .fold(vec2(0.0, 0.0), |acc, x| vec2(acc.x + x.x, acc.y + x.y))
         / self.len() as f32
   }

   /// The radius of the bounding circle
   pub fn radius(&self) -> f32 {
      let c = self.centroid();
      let mut max = f32::MIN;
      for p in self {
         let m = (*p - c).length_sq();
         if m > max {
            max = m
         }
      }
      max.sqrt()
   }

   /// Axis-aligned boudning box
   pub fn aabb(&self) -> Bounds {
      Bounds::containing(&*self.0)
   }

   /// Test if the polygon is convex
   pub fn is_convex(&self) -> bool {
      if self.len() < 4 {
         return true;
      }
      for i in 0..self.len() {
         if cross(&[self[i], self[i + 1], self[i + 2]]) > 0.0 {
            return false;
         }
      }
      true
   }

   /// Calculate the relative rotational inertia.
   pub fn inertia(&self) -> f32 {
      let mut numerator = 0.0;
      let mut denominator = 0.0;
      for (u, v) in self.segments() {
         let a = u.perp_dot(v).abs();
         let b = u.x * (u.x + v.x) + u.y * (u.y + v.y) + v.x * v.x + v.y * v.y;
         numerator += a * b;
         denominator += a;
      }
      4.0 * numerator / (6.0 * denominator)
   }

   /// Calculate the area of the polygon
   pub fn area(&self) -> f32 {
      0.5 * self
         .segments()
         .map(|(a, b)| (a, b))
         .map(|(a, b)| a.perp_dot(b).abs())
         .sum::<f32>()
   }

   pub fn transformed(&self, t: Mat3) -> PolyVec {
      self.iter().map(|&point| t * point).collect()
   }

   /// Calculate the intersection of this poly with a _convex_ clipping poly
   pub fn convex_clip(&self, clip: &PolyVec) -> Option<Poly> {
      assert!(clip.is_convex());

      let mut _in = Vec::new();
      let mut out = self.0.clone();
      for i in 0..clip.len() {
         _in.extend(out.drain());
         let mut s = *_in.last().unwrap();
         for p in _in.drain(..) {
            if cross(&[clip[i], clip[i + 1], p]) < 0.0 {
               if let Some(v) = intersection_point((clip[i], clip[i + 1]), (s, p)) {
                  if *out.last().unwrap() != v {
                     out.push(v)
                  }
               }
               if *out.last().unwrap() != p {
                  out.push(p)
               }
            } else {
               if let Some(v) = intersection_point((clip[i], clip[i + 1]), (s, p)) {
                  if *out.last().unwrap() != v {
                     out.push(v)
                  }
               }
            }
            s = p;
         }
      }
      if out.len() > 2 {
         Some(Poly::new(out))
      } else {
         None
      }
   }

   /// Calculate the convex hull of a the points, returns indices.
   pub fn convex_hull(&self) -> Vec<isize> {
      let mut left = 0;
      for i in 0..self.len() {
         if self[i].x < self[left].x {
            left = i
         }
      }

      let mut u = wrap_max(left + 1, self.len());
      let mut v = wrap_max(u + 1, self.len());

      let mut ret = vec![left, u];

      while v != left {
         if ret.len() > 1 && cross(&[self[ret[ret.len() - 2]], self[ret[ret.len() - 1]], self[v]]) > 0.0 {
            ret.pop();
         } else {
            ret.push(v);
            u = wrap_max(u + 1, self.len());
            v = wrap_max(v + 1, self.len());
         }
      }

      ret
   }
}

impl Index<isize> for PolyVec {
   type Output = Vec2;
   fn index(&self, idx: isize) -> &Vec2 {
      let idx = wrap_max(idx, self.0.len() as isize) as usize;
      &self.0[idx]
   }
}

impl IndexMut<isize> for PolyVec {
   fn index_mut(&mut self, idx: isize) -> &mut Vec2 {
      let idx = wrap_max(idx, self.0.len() as isize) as usize;
      &mut self.0[idx]
   }
}

impl<'a> IntoIterator for &'a PolyVec {
   type Item = &'a Vec2;
   type IntoIter = slice::Iter<'a, Vec2>;
   fn into_iter(self) -> Self::IntoIter {
      (*self.0).iter()
   }
}

impl<'a> IntoIterator for &'a mut PolyVec {
   type Item = &'a mut Vec2;
   type IntoIter = slice::IterMut<'a, Vec2>;
   fn into_iter(self) -> Self::IntoIter {
      self.0.iter_mut()
   }
}

impl FromIterator<Vec2> for PolyVec {
   fn from_iter<T: IntoIterator<Item = Vec2>>(iter: T) -> Self {
      PolyVec(SmallVec::from_iter(iter))
   }
}

impl Deref for PolyVec {
   type Target = [Vec2];
   fn deref(&self) -> &Self::Target {
      &*self.0
   }
}

impl DerefMut for PolyVec {
   fn deref_mut(&mut self) -> &mut Self::Target {
      &mut *self.0
   }
}

// ========================================================================
// -------------------------------- Tests ---------------------------------
// ========================================================================

#[cfg(test)]
mod tests {
   use super::*;

   /// Creates a set of points from a slice `&[[x, y], [x, y], ..]`
   fn points(x: &[[f32; 2]]) -> Vec<Vec2> {
      x.iter().map(|&v| vec2(v[0], v[1])).collect()
   }

   #[test]
   fn convex_decomposition() {
      fn case(expected: Vec<Vec<Vec2>>, points: Vec<Vec2>) {
         let poly = Poly::new(&points);
         for p in poly.convex_iter() {
            assert!(p.is_convex())
         }
         let result: Vec<Vec<Vec2>> = poly.convex_iter().map(|p| (**p).to_vec()).collect();
         assert_eq!(expected, result);
      }

      case(
         vec![
            points(&[[0.0, 0.0], [0.0, -1.0], [-1.0, 1.0]]),
            points(&[[0.0, 0.0], [1.0, 1.0], [0.0, -1.0]]),
         ],
         points(&[[0.0, 0.0], [1.0, 1.0], [0.0, -1.0], [-1.0, 1.0]]),
      );

      case(
         vec![
            // reversed as source is ccw
            points(&[[1.0, -1.0], [-1.0, -1.0], [-1.0, 1.0], [1.0, 1.0]]),
         ],
         points(&[[1.0, 1.0], [-1.0, 1.0], [-1.0, -1.0], [1.0, -1.0]]),
      );

      case(
         vec![
            points(&[[1.0, -1.0], [-1.0, -1.0], [1.0, 1.0]]),
            points(&[[-1.0, -1.0], [-1.0, 1.0], [0.0, 0.0]]),
         ],
         points(&[[1.0, 1.0], [0.0, 0.0], [-1.0, 1.0], [-1.0, -1.0], [1.0, -1.0]]),
      );
   }

   #[test]
   fn project() {
      let poly = Poly::new(&points(&[[0.0, 0.0], [1.0, 1.0], [0.0, -1.0], [-1.0, 1.0]]));
      assert_eq!(range(-1.0, 1.0), poly.project(vec2(1.0, 0.0), Mat2::id()));
      assert_eq!(range(-1.0, 1.0), poly.project(vec2(-1.0, 0.0), Mat2::id()));
      assert_eq!(range(-1.0, 1.0), poly.project(vec2(0.0, 1.0), Mat2::id()));
   }

   #[test]
   fn surface_normals() {
      let poly = Poly::new(&points(&[[0.0, 0.0], [1.0, 1.0], [0.0, -1.0], [-1.0, 1.0]]));
      assert_eq!(
         vec![vec2(-1.0, 1.0), vec2(2.0, -1.0), vec2(-2.0, -1.0), vec2(1.0, 1.0)],
         poly.surface_normals().collect::<Vec<_>>()
      );
   }

   #[test]
   fn area() {
      let poly = Poly::new(&points(&[[0.0, 0.0], [1.0, 1.0], [0.0, -2.0], [-1.0, 1.0]]));
      assert_eq!(poly.area(), 2.0);
   }

   #[test]
   fn convex_decomp_a() {
      Poly::new(&[
         vec2(1.0, 1.0),
         vec2(2.0, 1.0),
         vec2(2.0, -1.0),
         vec2(1.0, -1.0),
         vec2(1.0, -2.0),
         vec2(-1.0, -2.0),
         vec2(-1.0, -1.0),
         vec2(-2.0, -1.0),
         vec2(-2.0, 1.0),
         vec2(-1.0, 1.0),
         vec2(-1.0, 2.0),
         vec2(1.0, 2.0),
      ]);
   }
}
