//! Graphics context & window.

use crate::geom::math::*;
use crate::render;

use glium::glutin::dpi::*;
use glium::glutin::Api;
use glium::glutin::*;
use glium::*;

/// A wrapper around a glium `Display` & glutin `EventLoop`.
/// `esc` keeps a static thread-local reference to the display for creating textures/programs/etc,
/// so only one window can be created per-thread.
pub struct Window {
   ev_loop: EventsLoop,
   /// Window events since the last update
   pub window_events: Vec<WindowEvent>,
   /// The glium display, containing the glutin window and opengl context.
   pub display: Display,
   /// Whether the user has requested the window close.
   pub close_requested: bool,
   /// Whether the window is focused.
   pub is_focused: bool,
   /// The dimensions in pixels of the window.
   pub dimensions: Vec2,
   /// The window's drawable surface.
   pub frame: Frame,
}

impl Default for Window {
   fn default() -> Self {
      Window::new("esc", vec2i(1280, 720))
   }
}

impl Window {
   /// Create a window.
   pub fn new(title: &str, target_size: Vec2i) -> Window {
      let ev_loop = EventsLoop::new();

      let display = Display::new(
         WindowBuilder::new()
            .with_dimensions(LogicalSize::new(target_size.x as f64, target_size.y as f64))
            .with_title(title),
         ContextBuilder::new()
            .with_gl_profile(GlProfile::Core)
            .with_vsync(true)
            .with_depth_buffer(24)
            .with_gl(GlRequest::Specific(Api::OpenGl, (3, 3))),
         &ev_loop,
      )
      .unwrap();

      render::init_facade(Box::new(display.clone()));
      let mut frame = display.draw();
      frame.clear_all((0.0, 0.0, 0.0, 0.0), 1.0, 0);

      Window {
         ev_loop,
         display,
         frame,
         close_requested: false,
         is_focused: true,
         dimensions: target_size.into(),
         window_events: vec![],
      }
   }

   pub fn flip(&mut self) {
      self.frame.set_finish().unwrap();
      self.frame = self.display.draw();
      self.frame.clear_all((0.0, 0.0, 0.0, 0.0), 1.0, 0)
   }

   pub fn is_close_requested(&self) -> bool {
      self.close_requested
   }

   pub fn surface(&mut self) -> *mut Frame {
      &mut self.frame
   }

   pub fn set_title(&mut self, title: &str) {
      self.display.gl_window().window().set_title(title);
   }

   /// Poll for & handle events
   pub fn poll_events(&mut self) {
      let events = &mut self.window_events;
      events.clear();
      self.ev_loop.poll_events(|ev|
         if let Event::WindowEvent { event, .. } = ev { events.push(event) }
      );

      for ev in events {
         match *ev {
            WindowEvent::CloseRequested => self.close_requested = true,
            WindowEvent::Focused(is_focused) => self.is_focused = is_focused,
            WindowEvent::Resized(s) => self.dimensions = vec2(s.width as f32, s.height as f32),
            _ => (),
         }
      }
   }

   pub fn width(&self) -> f32 {
      self.dimensions.x
   }

   pub fn height(&self) -> f32 {
      self.dimensions.y
   }
}

impl Drop for Window {
   fn drop(&mut self) {
      self.frame.set_finish().unwrap()
   }
}

impl From<PhysicalPosition> for Vec2 {
   fn from(pos: PhysicalPosition) -> Vec2 {
      vec2(pos.x as f32, pos.y as f32)
   }
}

impl From<PhysicalSize> for Vec2 {
   fn from(size: PhysicalSize) -> Vec2 {
      vec2(size.width as f32, size.height as f32)
   }
}
