//! The state of a connection and the protocol implementation.

use std::fmt::Debug;
use std::mem;
use std::time::*;

use fnv::FnvHashMap as HashMap;
use fnv::FnvHashSet as HashSet;
use log::*;
use rand;

use super::guarantee::*;
use super::seq::*;

/// The connection state associated with a single peer.
pub struct Connection<T>
where
   T: Debug,
{
   /// The ID of the receiving end of the connection
   rec_id: Option<i32>,
   /// The time we last received a message on this connection
   last_rec: Instant,
   /// The sequence number of the most recent sequenced packet we received
   rec_seq: Seq,
   /// The most recently dispatched message
   disp_seq: Seq,
   /// A set of sequences which we have not received
   missing: HashSet<Seq>,
   /// A buffer messages for which we're waiting on a missing packet before
   /// dispatching
   order_buf: HashMap<Seq, Guaranteed<T>>,

   /// The time we last sent a packet
   last_send: Instant,
   /// The ID of the sending part of this connection
   snd_id: Option<i32>,
   /// True if we've received an acknowledgement for our open request
   snd_open: bool,
   /// A list of message for which we're waiting for an acknowledgement
   snd_queue: HashMap<Seq, SendData<T>>,
   /// The sequence of the last message we send
   snd_seq: Seq,
}

/// The data associated with a single packet we're currently sending.
struct SendData<T>
where
   T: Debug,
{
   /// The time we last attempted to send this message
   last_send: Instant,
   /// The number of times we've tried to send this message
   attempts: i64,
   /// The message data itself
   message: Guaranteed<T>,
}

/// The status of a Connection
#[derive(PartialEq)]
pub enum Status {
   Sending,
   Idle,
   Expired,
}

impl<T> Connection<T>
where
   T: Debug,
{
   /// Create a new connection
   pub fn new(now: Instant) -> Connection<T> {
      Connection {
         rec_id: None,
         last_rec: now,
         rec_seq: Seq(0),
         disp_seq: Seq(0),
         missing: HashSet::default(),
         order_buf: HashMap::default(),

         last_send: now,
         snd_id: None,
         snd_open: false,
         snd_queue: HashMap::default(),
         snd_seq: Seq(0),
      }
   }

   /// Get the status of the connection. If the status is Idle, the connection
   /// doesn't have to be frequently updated, if it's expired, it should be
   /// deleted
   pub fn status(&self, now: Instant) -> Status {
      if now.duration_since(self.last_rec) > Duration::from_secs(20)
         && now.duration_since(self.last_send) > Duration::from_secs(20)
      {
         Status::Expired
      } else if !self.snd_queue.is_empty() {
         Status::Sending
      } else {
         // If we have acknowledgements for each of the packets we've sent,
         // we don't need to periodically re-send them, so the connection's
         // update method can be called less frequently
         Status::Idle
      }
   }

   /// Pass a received message to the connection for processing.
   /// Panics if the given message has no guarantee data.
   pub fn receive<D: FnMut(Guaranteed<T>), S: FnMut(Guaranteed<T>)>(
      &mut self,
      msg: Guaranteed<T>,
      now: Instant,
      dispatch: D,
      send: S,
   ) {
      match msg.guarantee_data.unwrap() {
         GuaranteeData::Open(data) => {
            self.handle_open(data, msg, now, dispatch, send);
         }
         GuaranteeData::OpenAck(id) => {
            self.handle_open_ack(id, now);
         }
         GuaranteeData::Data(seq) => {
            self.handle_data(seq, msg, now, dispatch, send);
         }
         GuaranteeData::DataAck(seq_short) => {
            let seq = seq_short.into_full(self.snd_seq);
            trace!("Got ack {:?}, removing message from queue", seq);
            // Try to remove the message -- fails silently if it's not there
            // anymore (we received multiple acknowledgements)
            self.snd_queue.remove(&seq);
         }
      }
   }

   /// Checks the messages currently being sent, and re-send them if needed.
   pub fn update<S: FnMut(&Guaranteed<T>)>(&mut self, now: Instant, mut send: S) {
      // If we've already tried to send more than 10 times (4 seconds),
      // without an acknowledgement, remove the message.
      self.snd_queue.retain(|_, m| m.attempts <= 10);

      for (seq, m) in &mut self.snd_queue {
         // If it's been 400ms since we last tried to send, send it again.
         let since_sent = now.duration_since(m.last_send);
         if since_sent > Duration::from_millis(400) {
            m.last_send = now;
            m.attempts += 1;
            trace!("Sending guaranteed data, seq: {:?}", seq);

            self.last_send = now;
            send(&m.message);
         }
      }
   }

   /// Send the given message. The message's guarantee data is set, including
   /// a request to open a connection if needed, the message is added to the
   /// queue, and a reference to it returned to be sent through a socket.
   pub fn send(&mut self, mut msg: Guaranteed<T>, now: Instant) -> &Guaranteed<T> {
      self.snd_seq = Seq(*self.snd_seq + 1);
      let seq = self.snd_seq.into_small();

      msg.guarantee_data = Some(if !self.snd_open {
         if self.snd_id.is_none() {
            // Generate a random ID, 0 to 16 are reserved
            let mut id = 0i32;
            while (id >= 0) && (id <= 16) {
               id = rand::random();
            }
            self.snd_id = Some(id);
         }
         GuaranteeData::Open(OpenData {
            id: self.snd_id.unwrap(),
            is_open: self.rec_id.is_some(),
            seq: seq,
         })
      } else {
         GuaranteeData::Data(seq)
      });
      self.last_send = now;
      self.snd_queue.insert(
         self.snd_seq,
         SendData {
            last_send: now,
            attempts: 1,
            message: msg,
         },
      );
      let s = self.snd_seq;
      trace!("Sending guaranteed data, seq: {:?}", s);
      &self.snd_queue[&s].message
   }

   fn handle_open<S: FnMut(Guaranteed<T>), D: FnMut(Guaranteed<T>)>(
      &mut self,
      data: OpenData,
      msg: Guaranteed<T>,
      now: Instant,
      dispatch: D,
      mut send: S,
   ) {
      let OpenData { id, is_open, seq } = data;
      debug!("Got open request id: {}, is_open: {}", id, is_open);

      if let Some(last_id) = self.rec_id {
         if last_id != id {
            self.reset_rec(now);
         }
      }
      if self.snd_open && !is_open {
         self.reset_snd(now);
      }

      // Set our ID and acknowledge the open
      self.rec_id = Some(id);
      trace!("Sending open ack {}", id);
      send(msg.internal_respond(GuaranteeData::OpenAck(id)));

      // Receive the data appended into the open message
      let mut msg = msg;
      msg.guarantee_data = Some(GuaranteeData::Data(seq));
      self.receive(msg, now, dispatch, send);
   }

   fn handle_open_ack(&mut self, id: i32, now: Instant) {
      // Connection IDs 0 to 16 are reserved as control codes.
      // An ID of `1` is a request to reset the connection.
      if id == 1 {
         debug!("Got OpenAck(1)");
         self.reset_snd(now);
      } else {
         match self.snd_id {
            Some(my_id) => {
               if id == my_id {
                  self.snd_open = true;
               } else {
                  warn!("Got unmatching openack");
                  self.reset_snd(now);
               }
            }
            None => {
               warn!("Got openack for unrequested connection");
            }
         }
      }
   }

   fn handle_data<S: FnMut(Guaranteed<T>), D: FnMut(Guaranteed<T>)>(
      &mut self,
      seq: SeqSmall,
      msg: Guaranteed<T>,
      now: Instant,
      mut dispatch: D,
      mut send: S,
   ) {
      if self.rec_id.is_none() {
         // Senders should send open requests with every message until
         // we acknowledge it -- if we get data without having opened
         // a connection, something's wrong -- we request that the sender
         // reset their connection, sending a new open request.
         debug!(
            "Got data without a connection, requesting that \
             the sender re-open the connection."
         );
         send(msg.internal_respond(GuaranteeData::OpenAck(1)));

         // When they reset their sender we need to start receiving at
         // seq 0 again, so we reset our receiver.
         self.reset_rec(now);
      } else {
         // Calculate the full sequence number from the truncated data.
         let seq_long = seq.into_full(self.rec_seq);

         let prev_rec = self.last_rec;
         self.last_rec = now;

         // Acknowledge the data.
         trace!("Sending data ack {:?}", seq_long);
         send(msg.internal_respond(GuaranteeData::DataAck(seq)));

         let is_new = seq_long > self.rec_seq;
         let was_missing = self.missing.remove(&seq_long);

         trace!(
            "Processing message rec_seq: {:?}, new: {}, was_missing: {}",
            self.rec_seq,
            is_new,
            was_missing
         );

         if is_new {
            // Check for sequence numbers we haven't seen yet.
            for i in *self.rec_seq + 1..*seq_long {
               trace!("Missing packet {}", i);
               self.missing.insert(Seq(i));
            }
            self.rec_seq = seq_long;
         }

         if is_new || was_missing {
            match msg.guarantee {
               // If the sender doesn't care about the message order, we
               // dispatch it.
               GuaranteeLevel::Delivery => dispatch(msg),
               // Otherwise, we might have to buffer it, or send currently
               // buffered messages
               GuaranteeLevel::Order => {
                  let waiting = now.duration_since(prev_rec);

                  // If this message is the next in the sequence, dispatch it,
                  // otherwise add it to the buffer
                  if *seq_long == *self.disp_seq + 1 {
                     dispatch(msg);
                     self.disp_seq = seq_long;
                  } else {
                     self.order_buf.insert(seq_long, msg);
                     // If we've been waiting for > 2s, the message(s) we're
                     // missing probably aren't important anymore, so ignore them.
                     if waiting > Duration::from_millis(2000) {
                        for x in self.missing.drain() {
                           if *self.disp_seq < *x {
                              self.disp_seq = x;
                           }
                        }
                     }
                  }

                  // Loop through the order buffer, dispatching in sequence
                  // as much data as we have
                  loop {
                     let next = Seq(*self.disp_seq + 1);
                     match self.order_buf.remove(&next) {
                        None => break,
                        Some(x) => {
                           dispatch(x);
                           self.disp_seq = next;
                        }
                     }
                  }
               }
               _ => {}
            }
         }
      }
   }

   fn reset_rec(&mut self, now: Instant) {
      debug!("Resetting receiver");
      self.rec_id = None;
      self.last_rec = now;
      self.rec_seq = Seq(0);
      self.disp_seq = Seq(0);
      self.missing.clear();
      self.order_buf.clear()
   }

   fn reset_snd(&mut self, now: Instant) {
      debug!("Resetting sender");
      self.last_send = now;
      self.snd_id = None;
      self.snd_open = false;
      self.snd_seq = Seq(0);

      // Attempt to resend any queued messages
      let mut snd_queue = mem::replace(&mut self.snd_queue, HashMap::default());
      for (_, x) in snd_queue.drain() {
         self.send(x.message, now);
      }
   }
}
