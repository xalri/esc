#version 150

in vec2 uv;
out vec4 fragColor;
uniform sampler2D tx;

void main() {
  ivec2 size = textureSize(tx, 0);
  fragColor = texture(tx, uv / vec2(size));
}
