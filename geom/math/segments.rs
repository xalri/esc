use crate::geom::*;

/// Test if the segments a and b intersect.
pub fn intersects(a: (Vec2, Vec2), b: (Vec2, Vec2)) -> bool {
   let o1 = cross(&[a.0, a.1, b.0]);
   let o2 = cross(&[a.0, a.1, b.1]);
   let o3 = cross(&[b.0, b.1, a.0]);
   let o4 = cross(&[b.0, b.1, a.1]);

   if (o1 > 0.0) != (o2 > 0.0) && (o3 > 0.0) != (o4 > 0.0) {
      return true;
   }

   false
}

/// Ray-Segment intersection
pub fn ray_segment((a, b): (Vec2, Vec2), o: Vec2, d: Vec2) -> f32 {
   let v1 = o - a;
   let v2 = b - a;
   let v3 = d.perpendicular();

   let t1 = v2.perp_dot(v1) / v2.dot(v3);
   let t2 = v1.perp_dot(v3) / v2.dot(v3);

   if t1 >= 0.0 && t2 >= 0.0 && t2 <= 1.0 {
      t1
   } else {
      std::f32::INFINITY
   }
}

/// Get the intersection point of segments a & b. Returns `None` if the lines don't intersect
/// or are collinear
pub fn intersection_point(a: (Vec2, Vec2), b: (Vec2, Vec2)) -> Option<Vec2> {
   if !intersects(a, b) {
      return None;
   }

   let a_ = a.1 - a.0;
   let b_ = b.1 - b.0;
   let cross = a_.perp_dot(b_);
   if cross == 0.0 {
      return None;
   }

   let diff = a.0 - b.0;
   let t = b_.perp_dot(diff) / cross;

   Some(vec2(a.0.x + (t * a_.x), a.0.y + (t * a_.y)))
}

/// Test if a diagonal a1->b is locally inside a clockwise polygon
/// (a0, a1, a2, ... b)
pub fn locally_inside(a0: Vec2, a1: Vec2, a2: Vec2, b: Vec2) -> bool {
   if cross(&[a0, a1, a2]) > 0.0 {
      right_of(a0, a1, b) || right_of(a1, a2, b)
   } else {
      right_of(a0, a1, b) && right_of(a1, a2, b)
   }
}

/// Test if point p is to the left of a -> b
pub fn left_of(a: Vec2, b: Vec2, p: Vec2) -> bool {
   (b - a).perp_dot(p - a) > 0.0
}

/// Test if point p is to the right of a -> b
pub fn right_of(a: Vec2, b: Vec2, p: Vec2) -> bool {
   (b - a).perp_dot(p - a) < 0.0
}

/// Calculate the perpendicular distance between the line a->b and the point p.
pub fn perpendicular_distance(a: Vec2, b: Vec2, p: Vec2) -> f32 {
   let c = p.x * (b.y - a.y) - p.y * (b.x - a.x) + b.x * a.y - b.y * a.x;
   let d = (b.y - a.y).powi(2) + (b.x - a.x).powi(2);
   c.abs() / d.sqrt()
}

/// Find the closest point on a segment to a point.
pub fn project_on_segment(v: Vec2, s: (Vec2, Vec2)) -> Vec2 {
   let av = v - s.0;
   let ab = s.1 - s.0;
   let x = ab.length_sq();
   let t = if x > 0.0 { clamp(av.dot(ab) / x, 0.0, 1.0) } else { 0.0 };
   s.0 + ab * t
}

/*
/// Find the closest point on a triangle to a point
pub fn closest_point_triangle(p: Vec2, (a, b, c): (Vec2, Vec2, Vec2)) -> Vec2 {
   let ab = b - a;
   let ac = c - a;
   let bc = c - b;

   let snom = (p - a).dot(ab);
   let sdenom = (p - b).dot(a - b);

   let tnom = (p - a).dot(ac);
   let tdenom = (p - c).dot(a - c);

   if snom <= 0.0 && tnom <= 0.0 { return a; }

   let unom = (p - b).dot(bc);
   let udenom = (p - c).dot(b - c);

   if sdenom <= 0.0 && unom <= 0.0 { return b; }
   if tdenom <= 0.0 && udenom <= 0.0 { return c; }

   let n = (b - a).cross(c - a);
   let vc = n.dot((a - b).cross(b - p));

   if vc <= 0.0 && snom >= 0.0 && sdenom >= 0.0 {
      return a + snom / (snom + sdenom) * ab;
   }

   let va = n.dot((b - p).cross(c - p));

   if va <= 0.p && unom >= 0.0 && udenom >= 0.0 {
      return b + unom / (unom + udenom) * bc;
   }

   let vb = n.dot((c - p).cross(a - p));

   if vb <= 0.0 && tnom >= 0.0 && tdenom >= 0.0 {
      return a + tnom / (tnom + tdenom) * ac;
   }

   let u = va / (va + vb + vc);
   let v = vb / (va + vb + vc);
   let w = 1.0 - u - v;

   (u * a) + (v * b) + (w * c)
}
*/

/// Get the distance between a point and some segment
pub fn segment_point_distance(v: Vec2, s: (Vec2, Vec2)) -> f32 {
   (v - project_on_segment(v, s)).length()
}

/// For collinear points a, b, and c, test if b lies on a->c.
pub fn in_segment(a: Vec2, b: Vec2, c: Vec2) -> bool {
   b.x <= f32::max(a.x, c.x) && b.x >= f32::min(a.x, b.x) && b.y <= f32::max(a.y, c.y) && b.y >= f32::min(a.y, b.y)
}

#[test]
fn test_intersection() {
   assert_eq!(
      None,
      intersection_point((vec2(0.0, 0.0), vec2(0.0, 1.0)), (vec2(0.0, 0.0), vec2(0.0, 1.0)))
   );

   assert_eq!(
      Some(vec2(0.5, 0.5)),
      intersection_point((vec2(0.0, 0.0), vec2(1.0, 1.0)), (vec2(1.0, 0.0), vec2(0.0, 1.0)))
   );
}

#[test]
fn test_intersects() {
   assert!(!intersects(
      (vec2(0.0, 0.0), vec2(0.0, 1.0)),
      (vec2(0.0, 0.0), vec2(0.0, 1.0))
   ));

   assert!(!intersects(
      (vec2(0.0, 0.0), vec2(0.0, 2.0)),
      (vec2(0.0, 0.0), vec2(0.0, 1.0))
   ));

   assert!(intersects(
      (vec2(0.0, 0.0), vec2(1.0, 1.0)),
      (vec2(0.0, 1.0), vec2(1.0, 0.0))
   ));

   assert!(intersects(
      (vec2(0.0, 0.0), vec2(1.0, 1.0)),
      (vec2(1.0, 0.0), vec2(0.0, 1.0))
   ));

   assert!(!intersects(
      (vec2(0.0, 0.0), vec2(0.0, 1.0)),
      (vec2(1.0, 0.0), vec2(1.0, 1.0))
   ));

   assert!(!intersects(
      (vec2(0.0, 0.0), vec2(0.0, 1.0)),
      (vec2(0.0, 1.0), vec2(0.0, 0.0))
   ))
}

#[test]
fn test_right_of() {
   assert!(!right_of(vec2(0.0, 0.0), vec2(0.0, 2.0), vec2(0.0, 1.0)));
   assert!(!right_of(vec2(0.0, 0.0), vec2(0.0, 1.0), vec2(-1.0, 0.0)));
   assert!(!right_of(vec2(0.0, 0.0), vec2(0.0, 1.0), vec2(-1.0, 2.0)));
   assert!(right_of(vec2(0.0, 0.0), vec2(0.0, 1.0), vec2(1.0, 0.0)));
   assert!(right_of(vec2(0.0, 0.0), vec2(0.0, 1.0), vec2(1.0, 2.0)));
}

#[test]
fn test_left_of() {
   assert!(!left_of(vec2(0.0, 0.0), vec2(0.0, 2.0), vec2(0.0, 1.0)));
   assert!(left_of(vec2(0.0, 0.0), vec2(0.0, 1.0), vec2(-1.0, 0.0)));
   assert!(left_of(vec2(0.0, 0.0), vec2(0.0, 1.0), vec2(-1.0, 2.0)));
   assert!(!left_of(vec2(0.0, 0.0), vec2(0.0, 1.0), vec2(1.0, 0.0)));
   assert!(!left_of(vec2(0.0, 0.0), vec2(0.0, 1.0), vec2(1.0, 2.0)));
}

#[test]
fn test_locally_inside() {
   assert!(locally_inside(
      vec2(1.0, -1.0),
      vec2(0.0, 0.0),
      vec2(1.0, 1.0),
      vec2(2.0, 0.0)
   ));

   assert!(locally_inside(
      vec2(1.0, 1.0),
      vec2(0.0, 0.0),
      vec2(1.0, -1.0),
      vec2(-2.0, 0.0)
   ));

   assert!(!locally_inside(
      vec2(0.0, 0.0),
      vec2(1.0, 1.0),
      vec2(0.0, -1.0),
      vec2(1.0, 1.0)
   ));

   assert!(!locally_inside(
      vec2(1.0, -1.0),
      vec2(0.0, 0.0),
      vec2(1.0, 1.0),
      vec2(-2.0, 0.0)
   ));

   assert!(!locally_inside(
      vec2(1.0, 1.0),
      vec2(0.0, 0.0),
      vec2(1.0, -1.0),
      vec2(2.0, 0.0)
   ));
}

#[test]
fn test_perpendicular_distance() {
   assert_eq!(
      1.0,
      perpendicular_distance(
         Vec2 { x: 0.0, y: 0.0 },
         Vec2 { x: 2.0, y: 0.0 },
         Vec2 { x: 0.0, y: 1.0 }
      )
   );
   assert_eq!(
      1.0,
      perpendicular_distance(
         Vec2 { x: 0.0, y: 0.0 },
         Vec2 { x: 2.0, y: 0.0 },
         Vec2 { x: 0.0, y: -1.0 }
      )
   );
   assert_eq!(
      1.0,
      perpendicular_distance(
         Vec2 { x: 0.0, y: 0.0 },
         Vec2 { x: 2.0, y: 0.0 },
         Vec2 { x: 2.0, y: -1.0 }
      )
   );
}

#[test]
fn test_project_on_segment() {
   assert_eq!(
      vec2(0.0, 0.0),
      project_on_segment(vec2(-1.0, 1.0), (vec2(-1.0, -1.0), vec2(1.0, 1.0)))
   );

   assert_eq!(
      vec2(0.0, 0.0),
      project_on_segment(vec2(-1.0, 0.0), (vec2(0.0, 0.0), vec2(1.0, 1.0)))
   );

   assert_eq!(
      vec2(0.0, 0.0),
      project_on_segment(vec2(-1.0, 0.0), (vec2(1.0, 1.0), vec2(0.0, 0.0)))
   );
}
