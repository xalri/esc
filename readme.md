# esc

A framework for 2D networked games.

## Modules

##### storage

An entity component system.

##### geom

Geometric primitives and algorithms, including:

 - Vectors, matricies, and transforms.
 - Segment intersections, projection, etc.
 - Dynamic bounding volume tree, ported from [Box2D](http://www.box2d.org).
 - 2D convex & concave polygons.
 - Polygon triangulation.
 - Minimal polygon convex decomposition, ported from [polypartition](https://github.com/ivanfratric/polypartition/).
 - Swept SAT collision detection & manifold generation.
 - Sequential impulses collision resolution.

##### encode

A high-level abstraction over bit-oriented message encoding & decoding.
e.g.

```rust
// Stateful en/decoding is possible by defining the encoding for some 'rule'
// object, which contains the information necessary to en/decode instances of
// the type.
#[derive(Encode)]
#[rule("EventRule")]
pub struct PlaneFlightNet {
   // For every field we give an expression which generates a rule which can
   // en/decode that field.
   #[rule("rule.position()")]
   pub pos_prev: Vec2,
   // The module contains built-in rules for encoding e.g. ranged floats and 
   // integers with the fewest bits possible
   #[rule("ranged_float(0.0, 1000.0, 0.5)")]
   pub throttle: f32,
   #[rule("ranged_vec(-30.0, 30.0, 5.0)")]
   pub linear_vel: Vec2,
   #[rule("ranged_float(-40.0, 40.0, 5.0)")]
   pub angular_vel: f32,
}

...

let component = PlaneFlightNet{ ... };
let data = net::encode(EventRule{...}, &component);

...
```

##### net

A semi-reliable message passing protocol on top of UDP.  Based off the protocol used in Altitude.
Provides two systems, one for in-game events & transient state and another for non-realtime
use (server lists, chat rooms, etc).

##### xml

XML decoding using [xml-rs](https://github.com/netvl/xml-rs).

##### window

Small wrapper around a [glutin](https://github.com/tomaka/glutin) window with 
input tracking & constant-aspect viewports.

##### input

Provides input mapping, buffered actions, etc..


TODO:

 - GJK collision detection
 - joints & other non-collision constraints
 - re-do rendering
 - Particle system
 - Scene graph for physics
 - JSON format for sprite etc prototypes
 - Editor
 - ...
