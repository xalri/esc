#version 150

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

in int v_tile[];
in int v_flags[];
in vec2 v_pos[];

out vec2 uv;

uniform mat3 transform;
uniform int pitch;
uniform ivec2 tile_size;
uniform int texture_pitch;
uniform uint depth;

void main() {
   float z = float(depth) / 16777215.0;

   vec2 pos = v_pos[0];
   int tile = v_tile[0];
   int flags = v_flags[0];

   int tile_u = (tile % texture_pitch) * tile_size.x;
   int tile_v = (tile / texture_pitch) * tile_size.y;

   bool flipped_horizontally = (flags & 0x8) != 0;
   bool flipped_vertically = (flags & 0x4) != 0;
   bool flipped_diagonally = (flags & 0x2) != 0;

   int uv_left = tile_u;
   int uv_right = tile_u + tile_size.x;
   int uv_top = tile_v;
   int uv_bottom = tile_v + tile_size.y;

   if (flipped_diagonally) {
      uv_top = tile_u;
      uv_bottom = tile_u + tile_size.x;
      uv_left = tile_v;
      uv_right = tile_v + tile_size.y;
   }
   if (flipped_vertically) {
      int tmp = uv_top;
      uv_top = uv_bottom;
      uv_bottom = tmp;
   }
   if (flipped_horizontally) {
      int tmp = uv_left;
      uv_left = uv_right;
      uv_right = tmp;
   }

   gl_Position = vec4((transform * vec3(pos.x, pos.y, 1.0)).xy, z, 1.0);
   uv = vec2(uv_left, uv_top);
   EmitVertex();

   gl_Position = vec4((transform * vec3(pos.x + tile_size.x, pos.y, 1.0)).xy, z, 1.0);
   uv = vec2(uv_right, uv_top);
   EmitVertex();

   gl_Position = vec4((transform * vec3(pos.x, pos.y + tile_size.y, 1.0)).xy, z, 1.0);
   uv = vec2(uv_left, uv_bottom);
   EmitVertex();

   gl_Position = vec4((transform * vec3(pos.x + tile_size.x, pos.y + tile_size.y, 1.0)).xy, z, 1.0);
   uv = vec2(uv_right, uv_bottom);
   EmitVertex();
}
