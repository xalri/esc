use glium::*;
use derive_deref::*;
use serde::*;
use crate::geom::*;
use crate::render::*;
use crate::resources::*;

/// Shader for drawing tiles.
#[derive(Deref)]
pub struct TileShader(pub Program);

impl Default for TileShader {
   fn default() -> Self {
      TileShader(ProgramSource {
         vertex_shader: include_str!("../../glsl/tiles.vert"),
         fragment_shader: include_str!("../../glsl/tiles.frag"),
         geometry_shader: Some(include_str!("../../glsl/tiles.geom")),
      }.load().unwrap())
   }
}

pub const FLIPPED_HORIZONTALLY: u8 = 0x8;
pub const FLIPPED_VERTICALLY: u8 = 0x4;
pub const FLIPPED_DIAGONALLY: u8 = 0x2;

/// Data for a single tile
#[derive(Clone, Copy, PartialEq, Eq)]
#[derive(Serialize, Deserialize)]
pub struct Tile {
   /// The tile ID.
   pub tile_id: u16,
   /// The position of the tile in the chunk.
   pub pos: u16,
   /// Flags for flipping the tile.
   pub flags: u8,
}

implement_vertex!(Tile, tile_id, pos, flags);

/// Uniform data for drawing a chunk of tiles
#[derive(Clone)]
#[derive(Serialize, Deserialize)]
pub struct TileMaterial {
   pub diffuse: Res<Texture>,
   pub tile_size: Vec2i,
   pub pitch: i32,
   pub depth: u32,
}

impl<'a> Material for &'a TileMaterial {
   type Program = TileShader;
   type Vertex = Tile;
   type Uniforms = uniforms_type!(i32, Vec2i, i32, Sampler<'a, Texture2d>, u32, Mat3);

   fn uniforms(self, transform: Mat3) -> Self::Uniforms {
      let tx_pitch = self.diffuse.size().x as i32 / self.tile_size.x;
      uniform! {
         transform: transform,
         depth: self.depth,
         tx: self.diffuse.sampled(),
         texture_pitch: tx_pitch,
         tile_size: self.tile_size,
         pitch: self.pitch,
      }
   }

   fn params(self) -> DrawParameters<'static> {
      DrawParameters {
         blend: Blend::alpha_blending(),
         ..Default::default()
      }
   }
}
