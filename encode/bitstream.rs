//! Bit-oriented reading and writing to and from raw data.
//!
//! Altitude's network message format is bit-oriented. `Bit{Reader,Writer}` let us read and write
//! in a bit-wise fashion, without giving up the comforts of home!

use std::cmp;

/// A sized value that can be written and read to and from bitstreams.
///
/// This should only be implemented for small, Copyable types. Decoding and encoding
/// of these types should never error.
pub trait Bitable: Sized + Copy {
   fn read_from(reader: &mut BitReader) -> Option<Self>;
   fn write_to(&self, writer: &mut BitWriter);
}

/// The state of anything moving through a bitstream.
/// The position indicated here corresponds to the next bit to be attempted a read or write at.
struct BitState {
   pos: usize,
   b_off: u64,
}

impl BitState {
   fn new() -> Self {
      BitState { pos: 0, b_off: 0 }
   }

   fn pos(&self) -> usize {
      self.pos * 8 + (self.b_off as usize)
   }
}

/// A bit-wise read operation through some raw data.
pub struct BitReader<'a> {
   data: &'a [u8],
   state: BitState,
}

impl<'a> BitReader<'a> {
   /// Open a read handle to a borrowed slice.
   pub fn open(data: &'a [u8]) -> Self {
      BitReader {
         data,
         state: BitState::new(),
      }
   }

   /// Read a raw value of a specified bit-width.
   /// When we have reached the end of the data, this returns None.
   pub fn read_raw(&mut self, size: u64) -> Option<u64> {
      assert!(size <= 64);
      if self.data.is_empty() {
         return None;
      }
      let mut ret = 0u64;
      let mut left = size;
      while left > 0 {
         if self.state.pos > self.data.len() - 1 {
            return None;
         }

         let x = cmp::min(left, 8 - self.state.b_off);
         ret |= (((self.data[self.state.pos] >> self.state.b_off) as u64) & !((-1i64 as u64) << x)) << (size - left);
         self.state.b_off += x;
         left -= x;

         if self.state.b_off == 8 {
            self.state.pos += 1;
            self.state.b_off = 0;
         }
      }
      Some(ret)
   }

   /// Read a `Bitable` value. This should never error.
   /// When we have reached the end of the data, this returns None.
   pub fn read<T: Bitable>(&mut self) -> Option<T> {
      T::read_from(self)
   }

   /// Seek to a position in bits.
   pub fn seek(&mut self, pos: usize) {
      self.state.pos = pos / 8;
      self.state.b_off = (pos % 8) as u64;
   }

   /// Get the number of bits we've traversed so far.
   pub fn pos(&self) -> usize {
      self.state.pos()
   }

   /// Get the total length of the slice we're reading.
   pub fn len(&self) -> usize {
      self.data.len()
   }

   /// Returns true if there is no data in the reader
   pub fn is_empty(&self) -> bool {
      self.len() == 0
   }

   /// Determine whether the reader is in a possible terminal state, in which the useful data has
   /// already been read. Returns false <=> there are no more bytes to read, and all bits that
   /// remain in the current byte are zero.
   pub fn is_terminal(&self) -> bool {
      self.state.pos == self.data.len()
         || (self.state.pos == self.data.len() - 1 && (self.data[self.state.pos] as u64) < (2 ^ (self.state.b_off + 2)))
   }
}

/// Bitstream that can be written to.
pub struct BitWriter {
   data: Vec<u8>,
   state: BitState,
}

impl BitWriter {
   /// Create a new writer.
   pub fn new() -> Self {
      BitWriter {
         data: vec![0; 1],
         state: BitState::new(),
      }
   }

   /// Write a raw value of a specified bit-width.
   pub fn write_raw(&mut self, size: u64, v: u64) {
      assert!(size <= 64);
      let mut left = size;
      while left > 0 {
         if self.data.len() != self.state.pos + 1 {
            self.data.push(0);
         }
         let x = cmp::min(left, 8 - self.state.b_off);
         self.data[self.state.pos] |= (((v >> (size - left)) & !((-1i64 as u64) << x)) << self.state.b_off) as u8;
         self.state.b_off += x;
         left -= x;
         if self.state.b_off == 8 {
            self.state.pos += 1;
            self.state.b_off = 0;
         }
      }
   }

   /// Write a `Bitable` value. This should never error.
   pub fn write<T: Bitable>(&mut self, v: T) {
      v.write_to(self)
   }

   /// Get the number of bits we've traversed so far.
   pub fn pos(&self) -> usize {
      self.state.pos()
   }

   /// Get the current length of the slice we're writing.
   pub fn len(&self) -> usize {
      self.data.len()
   }

   /// Returns true if there is no data in the writer
   pub fn is_empty(&self) -> bool {
      self.len() == 0
   }

   /// Consume the `BitReader`, returning its internal Vec<u8>.
   pub fn into_inner(self) -> Vec<u8> {
      self.data
   }

   /// Reset the `BitReader`, returning its internal Vec<u8>.
   pub fn clear(&mut self) -> Vec<u8> {
      let data = ::std::mem::replace(&mut self.data, vec![0; 1]);
      self.state = BitState::new();
      data
   }
}

impl Into<Vec<u8>> for BitWriter {
   fn into(self) -> Vec<u8> {
      self.data
   }
}

impl Bitable for bool {
   fn read_from(reader: &mut BitReader) -> Option<Self> {
      reader.read_raw(1).map(|x| x == 1)
   }
   fn write_to(&self, writer: &mut BitWriter) {
      writer.write_raw(1, if *self { 1 } else { 0 })
   }
}

impl Bitable for f32 {
   fn read_from(reader: &mut BitReader) -> Option<Self> {
      reader.read_raw(32).map(|x| f32::from_bits(x as u32))
   }
   fn write_to(&self, writer: &mut BitWriter) {
      writer.write_raw(32, self.to_bits() as u64)
   }
}

impl Bitable for f64 {
   fn read_from(reader: &mut BitReader) -> Option<Self> {
      reader.read_raw(64).map(f64::from_bits)
   }
   fn write_to(&self, writer: &mut BitWriter) {
      writer.write_raw(64, self.to_bits())
   }
}

macro_rules! simple_bitable {
   ($t:ty, $bits:expr) => {
      impl Bitable for $t {
         fn read_from(reader: &mut BitReader) -> Option<Self> {
            reader.read_raw($bits).map(|x| x as $t)
         }
         fn write_to(&self, writer: &mut BitWriter) {
            writer.write_raw($bits, *self as u64)
         }
      }
   };
}

simple_bitable!(u8, 8);
simple_bitable!(u16, 16);
simple_bitable!(u32, 32);
simple_bitable!(i8, 8);
simple_bitable!(i16, 16);
simple_bitable!(i32, 32);

// The words in 64 bit values are flipped, because altitude.
impl Bitable for u64 {
   fn read_from(reader: &mut BitReader) -> Option<Self> {
      reader.read_raw(64).map(|i| (i >> 32) | (i << 32))
   }
   fn write_to(&self, writer: &mut BitWriter) {
      let x = (self >> 32) | (self << 32);
      writer.write_raw(64, x)
   }
}

impl Bitable for i64 {
   fn read_from(reader: &mut BitReader) -> Option<Self> {
      reader.read_raw(64).map(|i| ((i as i64) >> 32) | ((i as i64) << 32))
   }
   fn write_to(&self, writer: &mut BitWriter) {
      let x = ((*self as u64) >> 32) | ((*self as u64) << 32);
      writer.write_raw(64, x)
   }
}

impl Bitable for u128 {
   fn read_from(reader: &mut BitReader) -> Option<Self> {
      let a = match reader.read_raw(64) {
         Some(v) => v,
         None => return None,
      };
      let b = match reader.read_raw(64) {
         Some(v) => v,
         None => return None,
      };
      Some((a as u128) << 64 | b as u128)
   }
   fn write_to(&self, writer: &mut BitWriter) {
      writer.write_raw(64, (*self >> 64) as u64);
      writer.write_raw(64, *self as u64);
   }
}

#[cfg(test)]
mod tests {
   use super::*;

   #[test]
   /// Writing and reading raw data works.
   fn read_write_raw() {
      let mut writer = BitWriter::new();
      writer.write_raw(10, 0x3B);
      writer.write_raw(5, 0x19);
      assert_eq!(writer.len(), 2);

      let data = writer.into_inner();
      let mut reader = BitReader::open(&data);
      assert_eq!(reader.read_raw(10).unwrap(), 0x3B);
      assert_eq!(reader.read_raw(5).unwrap(), 0x19);
   }

   #[test]
   /// Reading raw data works, and we can get position / data length information.
   fn read_raw() {
      let vec = vec![0x34, 0xB3, 0x2D];
      let mut reader = BitReader::open(&vec);

      assert_eq!(reader.len(), 3);
      assert_eq!(reader.read_raw(5).unwrap(), 0x14);
      assert_eq!(reader.read_raw(5).unwrap(), 0x19);

      assert_eq!(reader.pos(), 10);
      assert_eq!(reader.read_raw(6).unwrap(), 0x2c);
      assert_eq!(reader.read_raw(8).unwrap(), 0x2d);
   }

   #[test]
   /// We can determine when a BitReader is in a "terminal state" -- a state
   /// where it makes sense to stop reading.
   fn terminal() {
      let vec = vec![0x00, 0xB3, 0x04];
      let mut reader = BitReader::open(&vec);

      // We still have 3 bytes to read.
      assert!(!reader.is_terminal());
      reader.read_raw(6).unwrap();

      // We still have some bytes.
      assert!(!reader.is_terminal());
      reader.read_raw(10).unwrap();

      // We still have a whole byte to read.
      assert!(!reader.is_terminal());
      reader.read_raw(1).unwrap();

      // We can't treat the next 7 bytes as padding because we have one more non-zero bit.
      assert!(!reader.is_terminal());
      reader.read_raw(1).unwrap();

      // We can reasonably treat the next 6 bytes as padding.
      assert!(reader.is_terminal());
      reader.read_raw(6).unwrap();

      // Now it's all over.
      assert!(reader.is_terminal());
   }

   #[test]
   /// We can read and write Bitables.
   fn bitable() {
      let mut writer = BitWriter::new();
      writer.write(true);
      writer.write(15 as u8);
      writer.write(15.02 as f32);
      writer.write(-1000 as i64);

      let data = writer.into_inner();

      let mut reader = BitReader::open(&data);
      assert_eq!(reader.read::<bool>().unwrap(), true);
      assert_eq!(reader.read::<u8>().unwrap(), 15);
      assert_eq!(reader.read::<f32>().unwrap(), 15.02);
      assert_eq!(reader.read::<i64>().unwrap(), -1000);
   }
}
