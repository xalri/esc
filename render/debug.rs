use crate::geom::*;
use crate::storage::*;
use crate::resources::*;
use glium::index::PrimitiveType;
use super::*;

#[derive(Debug, Default)]
pub struct DebugLines {
   pub lines: Vec<(Vec2, Vec2)>
}

impl DebugLines {
   pub fn bounds(&mut self, b: &Bounds) {
      let min = b.min();
      let max = b.min() + b.size();
      self.lines.push((vec2(min.x, min.y), vec2(min.x, max.y)));
      self.lines.push((vec2(min.x, max.y), vec2(max.x, max.y)));
      self.lines.push((vec2(max.x, max.y), vec2(max.x, min.y)));
      self.lines.push((vec2(max.x, min.y), vec2(min.x, min.y)));
   }
}

pub fn clear_debug_lines(lines: &mut DebugLines) {
   lines.lines.clear();
}

pub fn draw_lines(
   subs: &mut Subscriptions,
   view: &Store<View>,
   lines: &mut DebugLines,
   program: &<&'static SolidColour as Material>::Program,
) {
   if lines.lines.is_empty() { return }
   let views: Vec<&View> = subs.iter::<(View,)>((view,))
      .map(|e| &view[e]).collect();

   let mut verts = vec![];
   for (a, b) in &lines.lines {
      verts.push(Vertex2 { pos: *a, uv: Vec2::zero() });
      verts.push(Vertex2 { pos: *b, uv: Vec2::zero() });
   }

   Drawable::owned(
      Mat3::id(),
      SolidColour {
         colour: Colour::new(1.0, 0.8, 0.9, 1.0),
         .. Default::default()
      },
      Mesh {
         vertices: Res::new(Vertices{ data: verts }),
         primitives: PrimitiveType::LinesList,
         mask: 1,
      },
   ).draw(&views, program);
}

/// Create a drawable for a poly outline
pub fn debug_poly(transform: Mat3, poly: &Poly, colour: Colour)
   -> Drawable<'static, 'static, SolidColour>
{
   Drawable::owned(
      transform,
      SolidColour {
         colour,
         .. Default::default()
      },
      Mesh {
         vertices: Res::new(Vertices{ data: outline_verts(poly) }),
         primitives: PrimitiveType::LineLoop,
         mask: 1,
      },
   )
}

pub fn draw_colliders(
   entities: &mut Subscriptions,
   transform: &Store<Transform>,
   collider: &Store<CollisionPoly>,
   view: &Store<View>,
   program: &<&'static SolidColour as Material>::Program,
) {
   let views: Vec<&View> = entities.iter::<(View,)>((view,))
      .map(|e| &view[e]).collect();

   for e in entities.iter::<(Transform, CollisionPoly)>((transform, collider)) {
      debug_poly(
         transform[e].to_matrix(),
         &collider[e].hull,
         Colour::new(0.2, 0.6, 0.7, 1.0)
      ).draw(&views, program)
   }
}
