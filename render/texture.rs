use std::path::*;

use crate::error::*;
use crate::geom::*;
use crate::render::facade;
use crate::resources::*;
use glium::texture::*;
use glium::uniforms::*;
use serde::*;
use image::*;

/*
use glium::framebuffer::SimpleFrameBuffer;
use rect_packer::*;
use glium::BlitTarget;

/// A dynamic texture atlas.
#[derive(Atlas)]
pub struct Atlas {
   pub width: u32,
   pub height: u32,
   pub layers: u32,
   pub filtered: bool,
   pub packer: Vec<Packer>,
   pub tx: Texture2dArray,
}

/// A section of a texture atlas
pub struct Tx {
   pub filtered: bool,
   pub layer: u32,
   pub x: u32,
   pub y: u32,
   pub width: u32,
   pub height: u32,
}

impl Default for Atlas {
   fn default() -> Atlas { Atlas::new(2048, 2048, 4, true) }
}

impl Atlas {
   pub fn new(width: u32, height: u32, layers: u32, filtered: bool) -> Atlas {
      let mut packer = Vec::new();

      let config = Config {
         width: width as i32,
         height: height as i32,
         border_padding: 1,
         rectangle_padding: 1,
      };

      packer.resize(layers as usize, Packer::new(config));

      let tx = Texture2dArray::empty_with_format(
            facade(),
            UncompressedFloatFormat::U8U8U8U8,
            MipmapsOption::NoMipmap,
            width,
            height,
            layers
         ).unwrap();

      Atlas { width, height, layers, filtered, packer, tx }
   }

   pub fn insert<P: PixelValue + Clone>(&mut self, image: RawImage2d<'_, P>) -> Tx {
      let mut layer = 0;
      while layer < self.layers {
         if let Some(rect) = self.packer[layer].pack(image.width, image.height, false) {
            let src = Texture2d::new(facade(), image).unwrap();
            let fb = SimpleFrameBuffer::new(facade(), self.tx.layer(layer).main_level());
            let target = BlitTarget { left: rect.x, bottom: rect.y, width: rect.width, height: rect.height };
            src.as_surface().blit_whole_color_to(fb, target, MagnifySamplerFilter::Nearest);

            return Tx {
               filtered: self.filtered,
               layer,
               x: rect.x,
               y: rect.y,
               width: rect.width,
               height: rect.height,
            };
         }
         layer += 1
      }
      panic!("No space for texture in atlas!");
   }
}
*/



/// A 2D texture & sampling behaviour
#[derive(Debug)]
pub struct Texture {
   pub tx: Texture2d,
   pub wrap: SamplerWrapFunction,
   pub minify: MinifySamplerFilter,
   pub magnify: MagnifySamplerFilter,
   pub anisotropy: u16,
}

/// A sprite in a texture atlas
#[derive(Debug)]
#[derive(Serialize, Deserialize)]
pub struct Sprite {
   pub tx: Res<Texture>,
   pub bounds: Bounds,
}

impl Sprite {
   /// Get a sampler for the texture.
   pub fn sampled(&self) -> Sampler<Texture2d> {
      self.tx.sampled()
   }
}

impl Texture {
   /// Create a texture from raw image data.
   pub fn new<P: PixelValue + Clone>(image: RawImage2d<'_, P>) -> Result<Texture> {
      Ok(Texture {
         tx: Texture2d::with_format(
            facade(),
            image,
            UncompressedFloatFormat::U8U8U8U8,
            MipmapsOption::NoMipmap,
         )?,
         wrap: SamplerWrapFunction::Clamp,
         minify: MinifySamplerFilter::Nearest,
         magnify: MagnifySamplerFilter::Nearest,
         anisotropy: 1,
      })
   }

   /// Create a texture from an `image::RgbaImage`.
   pub fn from_image(image: RgbaImage, flip: bool) -> Result<Texture> {
      let size = image.dimensions();
      Texture::new(if flip {
            RawImage2d::from_raw_rgba_reversed(&image.into_raw(), size)
         } else {
            RawImage2d::from_raw_rgba(image.into_raw(), size)
         })
   }

   /// Get the texture's size as a `Vec2`.
   pub fn size(&self) -> Vec2 {
      vec2(self.tx.width() as f32, self.tx.height() as f32)
   }

   /// Create `Bounds` from the texture's size.
   pub fn bounds(&self) -> Bounds {
      bounds((0.0, self.size().x as f32), (0.0, self.size().y as f32))
   }

   /// Get a sampler for the texture.
   pub fn sampled(&self) -> Sampler<Texture2d> {
      Sampler(&self.tx, SamplerBehavior {
         wrap_function: (self.wrap, self.wrap, self.wrap),
         minify_filter: self.minify,
         magnify_filter: self.magnify,
         max_anisotropy: self.anisotropy,
         depth_texture_comparison: None,
      })
   }
}

impl Resource for Texture {
   type Descriptor = String;

   fn load(source: &dyn Source, descriptor: String) -> Result<Self> {
      let path = Path::new(&descriptor);
      let data = source.load(&descriptor)?;
      let image = if let Some(format) = image_format_from_path(path) {
         load_from_memory_with_format(&data, format)?
      } else {
         load_from_memory(&data)?
      };
      Texture::from_image(image.to_rgba(), true)
   }
}

impl Resource for Sprite {
   type Descriptor = Sprite;
   fn load(_source: &dyn Source, descriptor: Sprite) -> Result<Self> { Ok(descriptor) }
}

/// Get the image format for a given file path
fn image_format_from_path(path: &Path) -> Option<ImageFormat> {
   let extention = path.extension()
      .and_then(|s| s.to_str())
      .map_or("".to_string(), |s| s.to_lowercase());

   match extention.as_str() {
      "jpg" | "jpeg" => Some(ImageFormat::JPEG),
      "png" => Some(ImageFormat::PNG),
      "gif" => Some(ImageFormat::GIF),
      "webp" => Some(ImageFormat::WEBP),
      "tif" | "tiff" => Some(ImageFormat::TIFF),
      "tga" => Some(ImageFormat::TGA),
      "bmp" => Some(ImageFormat::BMP),
      "ico" => Some(ImageFormat::ICO),
      "hdr" => Some(ImageFormat::HDR),
      "pbm" | "pam" | "ppm" | "pgm" => Some(ImageFormat::PNM),
      _ => None
   }
}
