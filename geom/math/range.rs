use super::mat::*;
use super::vec::*;
use approx::*;
use std::ops::*;
use serde::*;

/// A range.
#[derive(PartialEq, Debug, Clone, Copy, Default)]
#[derive(Serialize, Deserialize)]
pub struct Range {
   pub min: f32,
   pub max: f32,
}

#[derive(PartialEq, Debug, Clone, Copy, Default)]
#[derive(Serialize, Deserialize)]
pub struct IntRange {
   pub min: i32,
   pub max: i32,
}

/// A 2D axis-aligned bounding box
#[derive(PartialEq, Debug, Clone, Copy, Default)]
#[derive(Serialize, Deserialize)]
pub struct Bounds {
   pub x: Range,
   pub y: Range,
}

pub fn range(a: f32, b: f32) -> Range {
   if a < b {
      Range { min: a, max: b }
   } else {
      Range { min: b, max: a }
   }
}

pub fn int_range(min: i32, max: i32) -> IntRange {
   IntRange { min, max }
}

pub fn bounds((x_min, x_max): (f32, f32), (y_min, y_max): (f32, f32)) -> Bounds {
   Bounds {
      x: range(x_min, x_max),
      y: range(y_min, y_max),
   }
}

impl Range {
   /// Create a new `Range` from an iterator, returns `None` if the iterator
   /// is empty.
   pub fn from_iter<I: Iterator<Item = f32>>(mut iter: I) -> Option<Range> {
      let mut min = iter.next()?;
      let mut max = min;
      for x in iter {
         if x < min {
            min = x
         }
         if x > max {
            max = x
         }
      }
      Some(Range { min, max })
   }

   /// Test if a value is contained in the range.
   pub fn contains(&self, value: f32) -> bool {
      value >= self.min && value <= self.max
   }

   pub fn contains_range(&self, r: Range) -> bool {
      self.min <= r.min && self.max >= r.max
   }

   pub fn intersects(&self, other: Range) -> bool {
      !(self.min > other.max || self.max < other.min)
   }

   pub fn combine(&self, other: Range) -> Range {
      Range {
         min: f32::min(self.min, other.min),
         max: f32::max(self.max, other.max),
      }
   }

   pub fn expand(&self, distance: f32) -> Range {
      Range {
         min: self.min - distance,
         max: self.max + distance,
      }
   }

   pub fn width(&self) -> f32 {
      self.max - self.min
   }

   pub fn center(&self) -> f32 {
      self.min + (self.width() / 2.0)
   }

   pub fn interpolate(&self, value: f32) -> f32 {
      (value - self.min) / self.width()
   }
}

impl Add<f32> for Range {
   type Output = Range;
   fn add(self, rhs: f32) -> Self::Output {
      Range {
         min: self.min + rhs,
         max: self.max + rhs,
      }
   }
}

impl AddAssign<f32> for Range {
   fn add_assign(&mut self, rhs: f32) {
      self.min += rhs;
      self.max += rhs;
   }
}

impl Sub<f32> for Range {
   type Output = Range;
   fn sub(self, rhs: f32) -> Self::Output {
      Range {
         min: self.min - rhs,
         max: self.max - rhs,
      }
   }
}

impl Mul<f32> for Range {
   type Output = Range;
   fn mul(self, rhs: f32) -> Self::Output {
      range(self.min * rhs, self.max * rhs)
   }
}

impl Div<f32> for Range {
   type Output = Range;
   fn div(self, rhs: f32) -> Self::Output {
      range(self.min / rhs, self.max / rhs)
   }
}

impl Bounds {
   /// Create `Bounds` containing a given set of points. Panics if the given array is empty.
   pub fn containing(points: &[Vec2]) -> Bounds {
      if points.is_empty() {
         panic!("Bounds::containing called with an empty array");
      }
      let mut x = Range {
         min: points[0].x,
         max: points[0].x,
      };
      let mut y = Range {
         min: points[0].y,
         max: points[0].y,
      };
      for point in points {
         if point.x < x.min {
            x.min = point.x
         }
         if point.y < y.min {
            y.min = point.y
         }
         if point.x > x.max {
            x.max = point.x
         }
         if point.y > y.max {
            y.max = point.y
         }
      }
      Bounds { x, y }
   }

   /// Test if a point is contained in the bounds.
   pub fn contains(&self, point: Vec2) -> bool {
      self.x.contains(point.x) && self.y.contains(point.y)
   }

   pub fn contains_bounds(&self, other: &Bounds) -> bool {
      self.x.contains_range(other.x) && self.y.contains_range(other.y)
   }

   /// Tests for an intersection between two bounds.
   pub fn intersects(&self, other: Bounds) -> bool {
      self.x.intersects(other.x) && self.y.intersects(other.y)
   }

   /// Test for an intersection with a ray
   pub fn ray_intersects(&self, point: Vec2, dir: Vec2) -> bool {
      let mut tmin = -std::f32::INFINITY;
      let mut tmax = std::f32::INFINITY;

      if dir.x != 0.0 {
         let tx1 = (self.x.min - point.x) / dir.x;
         let tx2 = (self.x.max - point.x) / dir.x;
         tmin = f32::max(tmin, f32::min(tx1, tx2));
         tmax = f32::min(tmax, f32::max(tx1, tx2));
      }

      if dir.y != 0.0 {
         let ty1 = (self.y.min - point.y) / dir.y;
         let ty2 = (self.y.max - point.y) / dir.y;
         tmin = f32::max(tmin, f32::min(ty1, ty2));
         tmax = f32::min(tmax, f32::max(ty1, ty2));
      }

      return tmax >= tmin;
   }

   pub fn combine(&self, other: Bounds) -> Bounds {
      Bounds {
         x: self.x.combine(other.x),
         y: self.y.combine(other.y),
      }
   }

   pub fn expand(&self, distance: f32) -> Bounds {
      Bounds {
         x: self.x.expand(distance),
         y: self.y.expand(distance),
      }
   }

   pub fn min(&self) -> Vec2 {
      vec2(self.x.min, self.y.min)
   }

   pub fn perimeter(&self) -> f32 {
      2.0 * (self.x.width() + self.y.width())
   }

   pub fn center(&self) -> Vec2 {
      vec2(self.x.center(), self.y.center())
   }

   pub fn size(&self) -> Vec2 {
      vec2(self.x.width(), self.y.width())
   }

   pub fn interpolate(&self, pos: Vec2) -> Vec2 {
      vec2(self.x.interpolate(pos.x), self.y.interpolate(pos.y))
   }

   pub fn interpolation_matrix(&self) -> Mat3 {
      assert!(self.x.width() > 0.0);
      assert!(self.y.width() > 0.0);
      Mat3::scale(vec2(1.0 / self.x.width(), 1.0 / self.y.width())) * Mat3::translate(vec2(-self.x.min, -self.y.min))
   }

   /// Get the aligned bounding box of a transformed version of this bounding box.
   pub fn transformed(&self, pos: Vec2, angle: f32, scale: f32) -> Bounds {
      if relative_eq!(angle, 0.0) {
         *self * scale + pos
      } else {
         let c = pos + Mat3::rotate(angle) * self.center() * scale;
         let r = (self.x.width().max(self.y.width()) / 2.0) * scale;
         Bounds {
            x: Range {
               min: c.x - r,
               max: c.x + r,
            },
            y: Range {
               min: c.y - r,
               max: c.y + r,
            },
         }
      }
   }

   pub fn apply_velocity(self, vel: Vec2) -> Bounds {
      let mut b = self;
      if vel.x < 0.0 {
         b.x.min += vel.x;
      } else if vel.x > 0.0 {
         b.x.max += vel.x;
      }
      if vel.y < 0.0 {
         b.y.min += vel.y;
      } else if vel.y > 0.0 {
         b.y.max += vel.y;
      }
      b
   }
}

impl Add<Vec2> for Bounds {
   type Output = Bounds;
   fn add(self, rhs: Vec2) -> Self::Output {
      Bounds {
         x: self.x + rhs.x,
         y: self.y + rhs.y,
      }
   }
}

impl Sub<Vec2> for Bounds {
   type Output = Bounds;
   fn sub(self, rhs: Vec2) -> Self::Output {
      Bounds {
         x: self.x - rhs.x,
         y: self.y - rhs.y,
      }
   }
}

impl Mul<f32> for Bounds {
   type Output = Bounds;
   fn mul(self, rhs: f32) -> Self::Output {
      Bounds {
         x: self.x * rhs,
         y: self.y * rhs,
      }
   }
}

impl Mul<Vec2> for Bounds {
   type Output = Bounds;
   fn mul(self, rhs: Vec2) -> Self::Output {
      Bounds {
         x: self.x * rhs.x,
         y: self.y * rhs.y,
      }
   }
}

impl Div<f32> for Bounds {
   type Output = Bounds;
   fn div(self, rhs: f32) -> Self::Output {
      Bounds {
         x: self.x / rhs,
         y: self.y / rhs,
      }
   }
}

#[test]
fn test_bounds_contains() {
   let bounds = bounds((0.0, 100.0), (0.0, 100.0));
   assert!(bounds.contains(Vec2 { x: 100.0, y: 100.0 }));
   assert!(bounds.contains(Vec2 { x: 0.0, y: 0.0 }));
   assert!(bounds.contains(Vec2 { x: 50.0, y: 50.0 }));
   assert!(bounds.contains(Vec2 { x: 50.0, y: 100.0 }));
   assert!(!bounds.contains(Vec2 { x: 100.0, y: -0.1 }));
   assert!(!bounds.contains(Vec2 { x: 100.1, y: 0.0 }));
}
