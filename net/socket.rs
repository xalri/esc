pub use crossbeam_channel::{select, *};
use log::*;
use std::collections::HashMap;
use std::fmt::Debug;
use std::io;
use std::net::*;
use std::net::{Ipv4Addr, SocketAddr};
use std::sync::Arc;
use std::thread;
use std::time::*;

use super::connection::*;
use super::guarantee::*;
use crate::encode::*;
use crate::error::*;

/// Wraps a UDP socket with a connection protocol for optionally guaranteeing
/// the delivery and order of messages on a per-message basis.
/// The interface is based on crossbeam channels, and relies on an external loop to regularly
/// call the update method and forward raw incoming messages to the connection manager.
pub struct Socket<Data: Debug, Codec> {
   /// Manages connections.
   /// Must be updated regularly (every ~50ms).
   /// To send a message over the socket, use connections.send(..).
   pub connections: Connections<Data, GuaranteedRule<Codec>>,
   /// Receiver for message data.
   pub incoming_rx: Receiver<Guaranteed<Data>>,
   /// Messages received over the socket, must be passed to connections, which forwards the actual
   /// message data to the `incoming` receiver.
   pub raw_rx: Receiver<Guaranteed<Data>>,
   /// A sender to terminate the listen thread
   terminate_sx: Sender<()>,
   /// A handle to the thread listening for incoming messages
   listen_thread: Option<thread::JoinHandle<()>>,
}

impl<Data, Codec> Socket<Data, Codec>
where
   Codec: Encoding<Data> + Debug + Clone + 'static + Send,
   Data: Debug + Clone + 'static + Send,
{
   /// Takes the address to bind to and a `codec` for reading & writing the message
   /// data, and returns channels for sending & receiving messages.
   pub fn new<A: ToSocketAddrs>(addr: A, codec: Codec) -> Result<Socket<Data, Codec>> {
      let socket = UdpSocket::bind(addr)?;
      Socket::from_net_socket(socket, codec)
   }

   /// Bind a new socket and join the multicast group.
   pub fn new_multicast<A: ToSocketAddrs>(addr: A, codec: Codec) -> Result<Socket<Data, Codec>> {
      let socket = UdpSocket::bind(addr)?;

      let multi = Ipv4Addr::new(227, 27, 27, 4);
      let any = Ipv4Addr::new(0, 0, 0, 0);
      socket.join_multicast_v4(&multi, &any)?;

      Socket::from_net_socket(socket, codec)
   }

   /// Create a socket from a `std::net::UdpSocket`.
   pub fn from_net_socket(socket: UdpSocket, codec: Codec) -> Result<Socket<Data, Codec>> {
      socket
         .set_read_timeout(Some(Duration::from_millis(100)))
         .expect("Failed to set a read timeout on socket");

      let socket = Arc::new(socket);
      let codec = GuaranteedRule { data_rule: codec };

      let (incoming_sx, incoming_rx) = unbounded();
      let (raw_sx, raw_rx) = bounded(1);
      let (terminate_sx, terminate_rx) = bounded(0);

      // Listen for incoming messages, pass to a channel
      let listen_thread = thread::spawn({
         let socket = socket.clone();
         let codec = codec.clone();
         move || {
            let mut buffer = vec![0u8; 2048];
            loop {
               if let Ok(_) = terminate_rx.try_recv() {
                  return;
               }
               match socket.recv_from(&mut buffer) {
                  Ok((size, src)) => match decode(&codec, &buffer[0..size]) {
                     Ok(mut msg) => {
                        msg.addr = src;
                        raw_sx.send(msg).unwrap();
                     }
                     Err(e) => {
                        error!("Message decoding failed: {}, data: {:?}", e, &buffer[0..size]);
                     }
                  },
                  Err(e) => {
                     if e.kind() != io::ErrorKind::TimedOut && e.kind() != io::ErrorKind::WouldBlock {
                        panic!("Failed to receive from socket: {}", e);
                     }
                  }
               }
            }
         }
      });

      Ok(Socket {
         connections: Connections {
            codec,
            socket,
            connections: ConnectionMap::new(),
            connections_order: ConnectionMap::new(),
            incoming: incoming_sx,
         },
         listen_thread: Some(listen_thread),
         incoming_rx,
         raw_rx,
         terminate_sx,
      })
   }
}

impl<Data: Debug, Codec> Drop for Socket<Data, Codec> {
   fn drop(&mut self) {
      info!("Waiting for socket to close");
      self.terminate_sx.send(()).unwrap();
      self.listen_thread.take().unwrap().join().unwrap();
      info!("Socket closed");
   }
}

type ConnectionMap<Data> = HashMap<SocketAddr, Connection<Data>>;

// Manages a set of connections
pub struct Connections<Data: Debug, Codec> {
   codec: Codec,
   socket: Arc<UdpSocket>,
   connections: ConnectionMap<Data>,
   connections_order: ConnectionMap<Data>,
   incoming: Sender<Guaranteed<Data>>,
}

impl<Data, Codec> Connections<Data, Codec>
where
   Codec: Encoding<Guaranteed<Data>> + Clone + 'static,
   Data: Debug + Clone + 'static,
{
   /// Handle an incoming message
   pub fn recv(&mut self, msg: Guaranteed<Data>) {
      // Check if the message is guaranteed, if it is, send pass it
      // through to this client's `Connection`, otherwise add it
      // to the dispatch queue
      if msg.is_guaranteed() {
         let socket = &self.socket;
         let codec = &self.codec;
         let incoming = &self.incoming;

         let send = |msg: Guaranteed<Data>| {
            socket.send_to(&encode(codec, &msg).unwrap(), msg.addr).unwrap();
         };
         let recv = |msg| incoming.send(msg).unwrap();

         connection(&mut self.connections, &mut self.connections_order, &msg).receive(msg, Instant::now(), recv, send);
      } else {
         self.incoming.send(msg).unwrap();
      }
   }

   /// Update the connections, resending messages with missing acknowledgements.
   pub fn update(&mut self) {
      // Remove expired connections
      let now = Instant::now();
      for connections in &mut [&mut self.connections, &mut self.connections_order] {
         let remove: Vec<_> = connections
            .iter()
            .filter(|&(_, con)| con.status(now) == Status::Expired)
            .map(|(&addr, _)| addr)
            .collect();

         for addr in remove {
            debug!("Removing expired connection {}", addr);
            connections.remove(&addr);
         }
      }

      let socket = &self.socket;
      let codec = &self.codec;

      let send = |msg: &Guaranteed<Data>| {
         socket.send_to(&encode(codec, msg).unwrap(), msg.addr).unwrap();
      };

      // Update all the connections, re-sending any messages given to us by
      // each connection
      let now = Instant::now();
      for c in self.connections.values_mut() {
         c.update(now, send);
      }
      for c in self.connections_order.values_mut() {
         c.update(now, send);
      }
   }

   /// Send a message on the socket
   pub fn send(&mut self, msg: Guaranteed<Data>) {
      let msg = if msg.is_guaranteed() {
         connection(&mut self.connections, &mut self.connections_order, &msg)
            .send(msg, Instant::now())
            .clone()
      } else {
         msg
      };

      let socket = &self.socket;
      let codec = &self.codec;

      match encode(codec, &msg) {
         Ok(data) => {
            socket.send_to(&data, msg.addr).unwrap();
         }
         Err(e) => error!("Message encoding failed: {}, msg: {:?}", e, msg),
      };
   }
}

/// Get the connection for the given message, panics if the message is not
/// guaranteed.
fn connection<'a, 'b: 'a, 'c: 'a, Data: Debug>(
   connections: &'b mut ConnectionMap<Data>,
   connections_order: &'c mut ConnectionMap<Data>,
   msg: &Guaranteed<Data>,
) -> &'a mut Connection<Data> {
   let addr = msg.addr;
   let map = match msg.guarantee {
      GuaranteeLevel::Delivery => connections,
      GuaranteeLevel::Order => connections_order,
      _ => panic!("Attempt to get connection for non-guaranteed message"),
   };
   map.entry(addr).or_insert_with(|| Connection::new(Instant::now()))
}
