#version 150

in int tile_id;
in int pos;
in int flags;

out int v_tile;
out int v_flags;
out vec2 v_pos;

uniform ivec2 tile_size;
uniform int pitch;

void main() {
   v_tile = tile_id;
   v_flags = flags;
   v_pos = vec2((pos % pitch) * tile_size.x, (pos / pitch) * tile_size.y);
}
