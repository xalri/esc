extern crate rand;
#[macro_use]
extern crate log;

use rand::distributions::*;
use rand::*;
use std::cmp;
use std::collections::BinaryHeap;
use std::fmt::Debug;
use std::mem;

/// An endpoint for NetEmu, usually implements a protocol which attempts to
/// give some guarantee about packet delivery and order.
pub trait Endpoint<S, M> {
   /// The emulator wants us to send some message -- we can modify it
   /// first.
   fn send(&mut self, m: S, time: u64) -> Option<M>;
   /// We receive a message from the other endpoint, we return some data
   /// to be sent back to them, and some to the emulator
   fn recv(&mut self, m: M, time: u64) -> (Vec<M>, Vec<S>);
   /// Do any processing required for the given emulation time, returning a
   /// vec of messages to send to the other endpoint. Used for protocols
   /// which automatically re-send messages until they get an acknowledgement
   /// (e.g. go-back-n ARQ)
   fn update(&mut self, time: u64) -> Vec<M>;
}

/// Parameters for NetEmu
pub struct EmuParams {
   /// The latency in ms to emulate
   pub latency: f64,
   /// The standard deviation for normally distributed latency
   pub latency_deviation: f64,
   /// The probability of a packet being lost (0.0 - 1.0)
   pub packet_loss: f64,
   /// The probability of a packet being duplicated (0.0 - 1.0)
   /// (Not applied recursively)
   pub packet_dup: f64,
}

#[derive(Clone, Copy, Debug)]
enum Destination {
   A,
   B,
}

/// A packet currently in transit in the network emulator. When comparing two
/// packets, the one with the closest delivery time is considered greater.
#[derive(Clone, Debug)]
struct Packet<M> {
   /// The time at which the packet will reach it's destination
   delivery_time: u64,
   /// The packet's destination
   destination: Destination,
   /// The packet's payload.
   data: M,
}

/// Emulates the latency, packet loss etc of networks, for testing protocols.
pub struct NetEmu<S, M> {
   /// Emulation parameters
   params: EmuParams,
   end_a: Box<dyn Endpoint<S, M>>,
   end_b: Box<dyn Endpoint<S, M>>,
   /// The current time of the simulation
   time: u64,
   /// Messages received by A
   result_a: Vec<S>,
   /// Messages received by B
   result_b: Vec<S>,
   /// Messages which are in transit
   transit: BinaryHeap<Packet<M>>,
   /// The time to next update the endpoints
   next_update: u64,
}

impl EmuParams {
   pub fn latency_distribution(&self) -> Normal {
      Normal::new(self.latency, self.latency_deviation)
   }
}

impl<S, M: Clone + 'static> NetEmu<S, M>
where
   M: Debug,
{
   /// Create a new `NetEmu`. Panics if we fail to seed a random number
   /// generator
   pub fn new<A, B>(params: EmuParams, a: A, b: B) -> NetEmu<S, M>
   where
      A: Endpoint<S, M> + 'static,
      B: Endpoint<S, M> + 'static,
   {
      NetEmu {
         params: params,
         end_a: Box::new(a) as Box<dyn Endpoint<S, M>>,
         end_b: Box::new(b) as Box<dyn Endpoint<S, M>>,
         time: 0,
         result_a: Vec::new(),
         result_b: Vec::new(),
         transit: BinaryHeap::new(),
         next_update: 0,
      }
   }

   /// Update both endpoints
   fn update(&mut self) {
      self.next_update = self.time + 20;
      for m in self.end_a.update(self.time) {
         self.send_raw(m, Destination::B);
      }
      for m in self.end_b.update(self.time) {
         self.send_raw(m, Destination::A);
      }
   }

   /// Process the transit queue to update it to the current time. All packets
   /// which have reached their target endpoint are processed by that endpoint,
   /// and all of it's replies pushed to either the transit queue or the
   /// endpoint's result queue.
   fn process(&mut self) {
      while !self.transit.is_empty() && self.transit.peek().unwrap().delivery_time <= self.time {
         let packet = self.transit.pop().unwrap();
         let snd = {
            let (to, res) = match packet.destination {
               Destination::A => (&mut self.end_a, &mut self.result_a),
               Destination::B => (&mut self.end_b, &mut self.result_b),
            };
            let (snd, disp) = to.recv(packet.data, self.time);
            for m in disp {
               res.push(m)
            }
            snd
         };
         let dest = match packet.destination {
            Destination::A => Destination::B,
            Destination::B => Destination::A,
         };
         for m in snd {
            self.send_raw(m, dest);
         }
      }
   }

   /// Mutate a single packet, adding latency, maybe deleting or duplicating it.
   fn mutate(&mut self, m: M, to: Destination) -> Vec<Packet<M>> {
      let mut result = Vec::new();
      if thread_rng().gen_range(0.0, 1.0) < self.params.packet_loss {
         return result;
      }

      let mut diff = self.params.latency_distribution().sample(&mut thread_rng());
      if diff < 0.0 {
         diff = 0.0
      }
      let time = self.time + diff as u64;
      result.push(Packet {
         delivery_time: time,
         destination: to,
         data: m,
      });

      if thread_rng().gen_range(0.0, 1.0) < self.params.packet_dup {
         let x = result[0].clone();
         result.push(x);
      }
      result
   }

   /// Progress the simulation such that the next message to reach it's
   /// destination does so.
   pub fn next(&mut self) {
      if let Some(x) = self.transit.peek().map(|x| x.delivery_time) {
         let diff = x - self.time;
         self.run_ms(diff);
      }
   }

   /// Run the simulation for a given (simulated) time
   pub fn run_ms(&mut self, mut time: u64) {
      debug!("running for {} ms", time);
      while time > 0 {
         let inc = cmp::min(time, self.next_update - self.time);
         time -= inc;
         self.time += inc;
         self.process();
         if self.time == self.next_update {
            self.update();
         }
      }
   }

   /// Run the simulation until the in-transit queue has been empty for 10
   /// simulated seconds
   pub fn run(&mut self) {
      loop {
         while !self.transit.is_empty() {
            self.next();
         }
         self.run_ms(10000);
         if self.transit.is_empty() {
            break;
         }
      }
   }

   /// Send a packet from endpoint B to endpoint A.
   pub fn send_to_a(&mut self, m: S) {
      if let Some(m) = self.end_b.send(m, self.time) {
         self.send_raw(m, Destination::A);
      }
   }

   /// Send a packet from endpoint A to endpoint B.
   pub fn send_to_b(&mut self, m: S) {
      if let Some(m) = self.end_a.send(m, self.time) {
         self.send_raw(m, Destination::B);
      }
   }

   /// Transmit a packet without running the endpoint's `send` function.
   fn send_raw(&mut self, m: M, dest: Destination) {
      debug!("Attempting to send {:?} to {:?}", m, dest);
      for m in self.mutate(m, dest) {
         debug!("packet {:?} is in transit", m);
         self.transit.push(m);
      }
   }

   /// Get the packets receieved by endpoint A
   pub fn recv_a(&mut self) -> Vec<S> {
      mem::replace(&mut self.result_a, Vec::new())
   }

   /// Get the packets receieved by endpoint B
   pub fn recv_b(&mut self) -> Vec<S> {
      mem::replace(&mut self.result_b, Vec::new())
   }

   pub fn time(&self) -> u64 {
      self.time
   }
}

impl<M> cmp::PartialEq for Packet<M> {
   fn eq(&self, other: &Packet<M>) -> bool {
      self.delivery_time == other.delivery_time
   }
}

impl<M> cmp::PartialOrd for Packet<M> {
   fn partial_cmp(&self, other: &Packet<M>) -> Option<cmp::Ordering> {
      Some(self.cmp(other))
   }
}

impl<M> cmp::Ord for Packet<M> {
   fn cmp(&self, other: &Packet<M>) -> cmp::Ordering {
      match self.delivery_time.cmp(&other.delivery_time) {
         cmp::Ordering::Less => cmp::Ordering::Greater,
         cmp::Ordering::Equal => cmp::Ordering::Equal,
         cmp::Ordering::Greater => cmp::Ordering::Less,
      }
   }
}

impl<M> cmp::Eq for Packet<M> {}
