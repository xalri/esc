//! Various bits of an ECS. Component storage, entity IDs, and subscriptions.

// https://gitlab.com/xalri/esc/tree/7d5ea5fca883126b522f6144e9e8ff33557cd383/storage

mod store;
mod query;

pub use self::store::*;
pub use self::query::*;


use crate::util::ref_tuple::RefTuple;
use crate::util::any_map::*;
use std::cell::*;
use std::ops::*;

/// A collection of entity subscriptions
#[derive(Default)]
pub struct Subscriptions {
   subs: AnyMap<Subscription<()>>,
}

impl Subscriptions {
   /// Get an iterator over the IDs of entities with some collection of components.
   pub fn iter<Q: Query + 'static>(&mut self, stores: Q::Stores) -> impl Iterator<Item=usize> + '_ {
      self.get::<Q>(stores).iter()
   }

   pub fn get<Q: Query + 'static>(&mut self, stores: Q::Stores) -> &mut Subscription<Q> {
      let sub = self.subs.get_or_default::<Subscription<Q>>();
      sub.update(stores);
      sub
   }
}

/// A set of entities.
#[derive(Default)]
pub struct EntitySet {
   next: usize,
   free: Vec<usize>,
   pub released: Vec<usize>,
   purgatory: Vec<usize>,
}

impl EntitySet {
   /// Get an unused entity ID.
   pub fn create(&mut self) -> usize {
      if let Some(id) = self.free.pop() {
         id
      } else {
         let id = self.next;
         self.next += 1;
         id
      }
   }

   /// Check if an entity exists
   pub fn has(&self, _e: usize) -> bool {
      unimplemented!() // TODO: store a bitvec, size = self.next
   }

   /// Release an entity ID. The ID will be made available again after the next
   /// call to `propegate_released`.
   /// Note that this doesn't remove the components attached to the entity.
   pub fn release(&mut self, entity: usize) {
      self.released.push(entity);
   }

   /// Allow previously released entities to be used again.
   pub fn propegate_released(&mut self) {
      self.free.extend(self.purgatory.drain(..));
      self.purgatory.extend(self.released.drain(..));
   }
}

/// Game state and such.
/// Don't take multiple mutable references to the same resource,
/// that would invoke undefined behaviour.
#[derive(Default)]
pub struct Storage {
   entities: EntitySet,
   subscriptions: Subscriptions,
   res: UnsafeCell<AnyMap<Box<u8>>>,
   _stores: UnsafeCell<Vec<*mut dyn StoreTrait>>,
}

impl Storage {
   /// Run a function on the world.
   /// Takes as parameters shared or mutable references to any type which
   /// implements `Default`, the objects are created the first time a type is
   /// requested.
   pub fn run<'a, 'b: 'a, Args: RefTuple<'a>, F: Fn<Args>>(&'b mut self, f: F) -> F::Output
      where Self: std::marker::Unsize<<Args as RefTuple<'a>>::Resources>,
   {
      let args = Args::fetch(self);
      Fn::call(&f, args)
   }

   /// Get an unused entity ID.
   pub fn create(&mut self) -> usize {
      self.entities.create()
   }

   /// Check if an entity exists.
   pub fn has(&self, e: usize) -> bool {
      self.entities.has(e)
   }

   /// Add a component to an entity.
   pub fn insert<Component: 'static>(&mut self, e: usize, c: Component) {
      log::debug!("Inserting {} into {}", std::intrinsics::type_name::<Component>(), e);
      (self as &mut dyn AsMut<Store<Component>>).as_mut().insert(e, c)
   }

   /// Release an entity ID. The ID will be made available again after the next
   /// call to `clean`.
   /// Note that this doesn't remove the components attached to the entity.
   pub fn release(&mut self, entity: usize) {
      self.entities.release(entity);
   }

   /// Remove the components associated with deleted entities then free the entity IDs.
   pub fn clean(&mut self) {
      for id in &self.entities.released {
         for s in unsafe { &mut *self._stores.get() } {
            unsafe { &mut **s }.remove(*id)
         }
      }
      self.entities.propegate_released();
      for s in unsafe { &mut *self._stores.get() } {
         unsafe { &mut **s }.clean()
      }
   }
}

impl<T: Default + 'static> AsMut<T> for Storage {
   default fn as_mut(&mut self) -> &mut T {
      let res = unsafe { &mut *self.res.get() };
      if !res.has::<Box<T>>() {
         log::debug!("Creating {}", std::intrinsics::type_name::<T>());
         return &mut **res.insert(Box::new(Default::default()));
      }
      &mut **res.get::<Box<T>>().unwrap()
   }
}

impl<T: Default + StoreTrait + 'static> AsMut<T> for Storage {
   fn as_mut(&mut self) -> &mut T {
      let res = unsafe { &mut *self.res.get() };
      if !res.has::<Box<T>>() {
         log::debug!("Creating component store {}", std::intrinsics::type_name::<T>());
         let store = res.insert(Box::new(Default::default()));
         let _stores = unsafe { &mut *self._stores.get() };
         _stores.push(&mut **store as *mut dyn StoreTrait);
         return &mut **store
      }

      &mut **res.get::<Box<T>>().unwrap()
   }
}

impl AsMut<EntitySet> for Storage { fn as_mut(&mut self) -> &mut EntitySet { &mut self.entities } }
impl AsMut<Subscriptions> for Storage { fn as_mut(&mut self) -> &mut Subscriptions { &mut self.subscriptions } }
