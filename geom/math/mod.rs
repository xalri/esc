mod mat;
pub mod range;
mod segments;
mod vec;

pub use self::mat::*;
pub use self::range::*;
pub use self::range::{range, Range};
pub use self::segments::*;
pub use self::vec::*;

pub use num::clamp;
use num::*;
use std::cmp::PartialOrd;
use std::f32;
use std::f32::consts::PI;

/// Wrap a value between 0 and a given maximum.
pub fn wrap_max<T>(value: T, max: T) -> T
where
   T: Num + Copy,
{
   (max + value % max) % max
}

/// Wrap a value in a given range [min, max).
pub fn wrap<T>(value: T, min: T, max: T) -> T
where
   T: Num + Copy,
{
   min + wrap_max(value - min, max - min)
}

/// Wrap an angle in radians to -PI..PI
pub fn wrap_angle(value: f32) -> f32 {
   wrap(value, -PI, PI)
}

pub fn clamp_abs<T>(value: T, max: T) -> T
where
   T: Float + Copy {
   clamp(value, -max.abs(), max.abs())
}

pub fn flip_angle(mut a: f32, x: bool, y: bool) -> f32 {
   if x { a = -a }
   if y { a = PI - a }
   wrap_angle(a)
}

/// The signed difference between two angles
pub fn angle_diff(a: f32, b: f32) -> f32 {
   let x = wrap_angle(a) - wrap_angle(b);
   x + if x > PI { -2.0 * PI } else if x < -PI { 2.0 * PI } else { 0.0 }
}

pub fn invert(x: f32) -> f32 {
   if x == 0.0 { 0.0 } else { 1.0 / x }
}

/// Map a value from it's relative position in one range to the value of that
/// relative position in another, e.g. `map(1, 0..2, 10..20) = 15`
pub fn map<T>(value: T, from: [T; 2], to: [T; 2]) -> T
where
   T: Num + Copy + PartialOrd,
{
   let value = clamp(value, from[0], from[1]);
   let k = (value - from[0]) / (from[1] - from[0]);
   to[0] + ((to[1] - to[0]) * k)
}

/// Fast inverse sqrt
pub fn inv_sqrt(x: f32) -> f32 {
   if x < 0.0 {
      return f32::NAN;
   }
   if x == 0.0 {
      return f32::INFINITY;
   }
   if x == f32::INFINITY {
      return 0.0;
   }

   let y = x.to_bits();
   let y = 0x5f375a86 - (y >> 1);
   let y = f32::from_bits(y);
   y * (1.5 - (x * 0.5 * y * y))
}

// ========================================================================
// -------------------------------- Tests ---------------------------------
// ========================================================================

#[cfg(test)]
use approx::*;

#[test]
fn test_map() {
   assert_relative_eq!(map(0.5, [0.0, 1.0], [10.0, 20.0]), 15.0);
   assert_relative_eq!(map(2.0, [0.0, 1.0], [10.0, 20.0]), 20.0);
   assert_relative_eq!(map(2.0, [0.0, 1.0], [20.0, 10.0]), 10.0);
   assert_relative_eq!(map(0.7, [0.0, 1.0], [10.0, 20.0]), 17.0);
   assert_relative_eq!(map(0.7, [0.0, 1.0], [20.0, 10.0]), 13.0);
   assert_relative_eq!(map(7.0, [5.0, 10.0], [20.0, 10.0]), 16.0);
   assert_relative_eq!(map(7.0, [5.0, 10.0], [10.0, 20.0]), 14.0);
}

#[test]
fn test_wrap() {
   assert_relative_eq!(wrap(0.5, 0.0, 1.0), 0.5);
   assert_relative_eq!(wrap(-0.5, 0.0, 1.0), 0.5);
   assert_relative_eq!(wrap(-0.2, 0.0, 1.0), 0.8);
   assert_relative_eq!(wrap(1.5, 0.0, 1.0), 0.5);
   assert_relative_eq!(wrap(1.2, 0.0, 1.0), 0.2);
   assert_relative_eq!(wrap(0.0, 0.0, 1.0), 0.0);
   assert_relative_eq!(wrap(-1.0, 0.0, 1.0), 0.0);
   assert_relative_eq!(wrap(1.0, 0.0, 1.0), 0.0);
}
