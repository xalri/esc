//! Named resources with serializable metadata

mod cache;
mod directory;

pub use self::cache::*;
pub use self::directory::*;

use crate::error::*;
use std::sync::Arc;
use std::ops::*;
use serde::*;
use serde::de::{DeserializeOwned, Visitor};
use std::marker::PhantomData;

/// Global resources
pub mod res {
   use super::*;
   use once_cell::sync::Lazy;
   use std::sync::*;
   use once_cell::*;

   static RESOURCES: Lazy<RwLock<Resources>> =
      sync_lazy! { RwLock::new(Resources::default()) };

   /// Add a source to the global stack.
   pub fn push_source<S: Source + 'static>(source: S) {
      RESOURCES.write().unwrap().push_source(source)
   }

   /// Remove the source at the top of the global stack.
   pub fn pop_source() {
      RESOURCES.write().unwrap().pop_source()
   }

   /// Load a resource.
   pub fn get<R: Resource>(id: &str) -> Result<Res<R>> {
      RESOURCES.read().unwrap().get(id)
   }
}

/// A source of data for loading resources.
pub trait Source: Send + Sync {
   /// Read a file into a Vec<u8>.
   fn load(&self, path: &str) -> Result<Vec<u8>>;
}

/// A resource
pub trait Resource: Sized + 'static {
   /// A serializable description of the resource.
   /// Gives e.g. the path to the resource file and any extra properties.
   type Descriptor: Serialize + DeserializeOwned + 'static;

   /// Load the resource.
   fn load(source: &dyn Source, descriptor: Self::Descriptor) -> Result<Self>;
}

impl<T: Resource> Resource for Vec<T> {
   type Descriptor = Vec<T::Descriptor>;
   fn load(source: &dyn Source, vec: Self::Descriptor) -> Result<Self> {
      let mut result = vec![];
      for descriptor in vec {
         result.push(T::load(source, descriptor)?);
      }
      Ok(result)
   }
}

/// A wrapper around Arc<T> which implements Serialize & Deserialize
/// via an identifier using the global resource cache.
#[derive(Default, Debug)]
pub struct Res<T>(pub Arc<(T, String)>);

impl<T> Res<T> {
   pub fn new(value: T) -> Res<T> {
      Res(Arc::new((value, "".to_string())))
   }
}

impl<T> Deref for Res<T> {
   type Target = T;
   fn deref(&self) -> &T {
      &(self.0).0
   }
}

impl<T> Clone for Res<T> {
   fn clone(&self) -> Self { Res(self.0.clone()) }
}

impl<T> Serialize for Res<T> {
   fn serialize<S>(&self, serializer: S) -> ::std::result::Result<S::Ok, S::Error>
   where S: Serializer
   {
      serializer.serialize_str(&(self.0).1)
   }
}

impl<'de, T: Resource> Deserialize<'de> for Res<T> {
   fn deserialize<D>(deserializer: D) -> std::result::Result<Self, D::Error>
   where D: Deserializer<'de>
   {
      use std::fmt;

      struct Vis<T>(PhantomData<T>);
      impl<'de, T: Resource> Visitor<'de> for Vis<T> {
         type Value = Res<T>;

         fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("a string")
         }

         fn visit_str<E>(self, id: &str) -> std::result::Result<Self::Value, E>
         where E: de::Error {
            match res::get::<T>(id) {
               Ok(ptr) => Ok(ptr),
               Err(e) => Err(E::custom(format!("Failed to load '{}' during deserialization: {}", id, e)))
            }
         }
      }
      let ret = deserializer.deserialize_str(Vis::<T>(PhantomData));
      ret
   }
}

/// A stack of resource caches.
/// Gets resources from the first cache which contains the id.
#[derive(Default)]
pub struct Resources {
   stack: Vec<ResCache>,
}

impl Resources {
   /// Add a source to the stack.
   pub fn push_source<S: Source + 'static>(&mut self, source: S) {
      let mut cache = ResCache::with_source(source);
      cache.load_manifest("manifest.ron").expect("Failed to load manifest");
      self.stack.push(cache);
   }

   /// Remove the source at the top of the stack.
   pub fn pop_source(&mut self) {
      self.stack.pop();
   }

   /// Load some resource.
   pub fn get<R: Resource>(&self, id: &str) -> Result<Res<R>> {
      for s in self.stack.iter().rev() {
         if let Some(res) = s.get(id) {
            return res;
         }
      }
      bail!("Failed to find resource '{}' in the source stack", id)
   }
}
