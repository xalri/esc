//! Fixed-width bitmap font rendering

use glium::texture::*;
use glium::index::PrimitiveType;
use std::borrow::Cow;

use crate::geom::*;
use crate::resources::*;
use super::*;
use super::texture::*;
use super::materials::*;

/// Fixed-width bitmap font
pub struct FixedWidthFont {
   pub material: TileMaterial,
   pub char_offset: i32,
}


impl FixedWidthFont {
   pub fn build(&self, s: &str, transform: Mat3) -> Drawable<TileMaterial> {
      let lines: Vec<&str> = s.lines().collect();
      let pitch = lines.iter().map(|s| s.len()).max().unwrap_or(0);

      let mut data = vec![];

      for i in 0..lines.len() {
         let s = lines[i].as_bytes();
         data.extend((0..s.len()).map(|j| Tile {
            tile_id: (s[j] as i32 - self.char_offset) as u16,
            pos: (j + i * pitch) as u16,
            flags: 0,
         }));
      }

      Drawable::owned(
         transform,
         TileMaterial { pitch: pitch as i32, .. self.material.clone() },
         Mesh {
            vertices: Res::new(Vertices { data }),
            primitives: PrimitiveType::Points,
            mask: 1,
         }
      )
   }
}

/// The font used for debug text
pub struct DebugFont(pub FixedWidthFont);

impl Default for DebugFont {
   fn default() -> Self {
      /// A monospace font for printing debug info
      pub const DEBUG_FONT_IMAGE: RawImage2d<'static, u8> = RawImage2d {
         data: Cow::Borrowed(include_bytes!("./debug_font.bin")),
         width: 504,
         height: 144,
         format: ClientFormat::U8U8U8U8,
      };

      let mut tx = Texture::new(DEBUG_FONT_IMAGE).unwrap();
      tx.minify = MinifySamplerFilter::Linear;

      DebugFont(FixedWidthFont {
         material: TileMaterial {
            diffuse: Res::new(tx),
            tile_size: vec2i(21, 36),
            pitch: 0,
            depth: 0,
         },
         char_offset: 32,
      })
   }
}
