#version 150

in vec2 v_uv;

out vec4 fragColor;

uniform vec4 multiply;
uniform sampler2D tx;
uniform bool absolute_uv = false;

layout(std140) struct Tint {
   vec3 hsv;
   vec3 filter_center;
   vec3 filter_width;
   vec3 filter_smooth;
};

layout(std140) uniform Tints {
   Tint tint[8];
};

vec3 rgb2hsv(vec3 c) {
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c) {
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main() {
  vec4 c;
  vec2 uv = v_uv;
  if (absolute_uv) { uv = uv / vec2(textureSize(tx,0)); }
  c = texture(tx, uv) * multiply;

  vec3 _hsv = rgb2hsv(c.xyz);
  vec3 hsv_result = _hsv;
  
  for (int i = 0; i < 8; i++) {
     Tint t = tint[i];
     if (t.hsv.x != 0 || t.hsv.y != 0 || t.hsv.z != 0) {
       vec3 s = _hsv - t.filter_center;
       s.x = mod(s.x + 0.5, 1) - 0.5;
       vec3 half_filter = t.filter_width / 2;

       vec3 mult = smoothstep(-half_filter - t.filter_smooth, -half_filter, s)
         * (1 - smoothstep(half_filter, half_filter + t.filter_smooth, s));

       mult = vec3(mult.x * mult.y * mult.z);

       //fragColor = vec4(mult, c.w);
       hsv_result += t.hsv * mult;
       hsv_result.x = mod(hsv_result.x, 1);
       hsv_result = clamp(hsv_result, 0, 1);
     }
  }

  fragColor = vec4(hsv2rgb(hsv_result), c.w);
}
