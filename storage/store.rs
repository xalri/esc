use std::ops;
use std::mem;
use std::ptr;

const BITSET_WORDS: usize = 4;
const BITSET_WORD_SIZE: usize = 128;
const BLOCK_SIZE: usize = BITSET_WORDS * BITSET_WORD_SIZE;

/// A semi-contiguous container based on a hashed array tree.
pub struct Store<T> {
   pub(crate) blocks: Vec<Option<Box<Block<T>>>>,
   pub(crate) hash: u64,
   pub(crate) size: usize,
}

/// A block of contiguous components in a store
pub(crate) struct Block<T> {
   pub(crate) usage: BitSet,
   pub(crate) data: [mem::MaybeUninit<T>; BLOCK_SIZE],
}

/// The usage bitset for a block of components.
#[derive(Copy, Clone, Default, PartialEq, Eq)]
pub struct BitSet([u128; BITSET_WORDS]);

fn word_idx(bit: usize) -> (usize, usize) {
   (bit / BITSET_WORD_SIZE, bit % BITSET_WORD_SIZE)
}

fn ia(i: usize) -> (usize, usize) {
   (i / BLOCK_SIZE, i % BLOCK_SIZE)
}

impl BitSet {
   /// Check if the bitset contains some bit
   pub fn contains(&self, bit: usize) -> bool {
      let (ia, ib) = word_idx(bit);
      self.0[ia] & (1 << ib) != 0
   }

   /// Set some bit to true
   pub fn set(&mut self, bit: usize) {
      let (ia, ib) = word_idx(bit);
      self.0[ia] |= 1 << ib;
   }

   /// Set some bit to false
   pub fn reset(&mut self, bit: usize) {
      let (ia, ib) = word_idx(bit);
      self.0[ia] &= !(1 << ib);
   }

   /// Get the total number of set bits
   pub fn count_ones(&self) -> usize {
      self.0.iter().map(|v| v.count_ones() as usize).sum()
   }

   /// Get the length of the bitset
   pub fn len(&self) -> usize {
      BLOCK_SIZE
   }
}

impl ops::BitAnd<BitSet> for BitSet {
   type Output = BitSet;
   fn bitand(self, rhs: BitSet) -> BitSet {
      let mut ret = BitSet::default();
      for i in 0..ret.0.len() { ret.0[i] = self.0[i] & rhs.0[i]; }
      ret
   }
}

impl<T> Block<T> {
   /// Create an empty block
   pub fn empty() -> Block<T> {
      Block { usage: BitSet::default(), data: unsafe{ mem::zeroed() } }
   }
}

impl<T> Default for Store<T> {
   fn default() -> Self {
      Store { blocks: vec![], hash: 0, size: 0 }
   }
}

/// Operations on a store which are independent of the stored type
pub trait StoreTrait {
   /// Get the number of components in the store
   fn len(&self) -> usize;

   /// Test if the container contains an element at the given index.
   fn has(&self, idx: usize) -> bool;

   /// Remove an element from the container.
   /// Does nothing if the element doesn't exist.
   fn remove(&mut self, idx: usize);

   /// Remove empty chunks to reduce memory usage.
   fn clean(&mut self);
}

impl<T> StoreTrait for Store<T> {
   /// Get the number of components in the store
   fn len(&self) -> usize {
      self.size
   }

   /// Test if the container contains an element at the given index.
   fn has(&self, idx: usize) -> bool {
      let (ia, ib) = ia(idx);
      self.blocks.len() > ia && self.blocks[ia].is_some() && self.block(ia).usage.contains(ib)
   }

   /// Remove an element from the container.
   /// Does nothing if the element doesn't exist.
   fn remove(&mut self, idx: usize) {
      if !self.has(idx) { return }
      let (ia, ib) = ia(idx);
      let block = self.block_mut(ia);
      block.usage.reset(ib);
      unsafe { ptr::drop_in_place(block.data[ib].as_mut_ptr()) }
      self.hash += 1;
      self.size -= 1;
   }

   /// Remove empty chunks to reduce memory usage.
   fn clean(&mut self) {
      let mut i = self.blocks.len();
      while i > 0 {
         i -= 1;
         if self.blocks[i].is_some() && self.block(i).usage.count_ones() == 0 {
            self.blocks[i] = None;
         }
      }
   }
}

impl<T> Store<T> {
   /// Add an element to the container.
   /// If a value already exists at that index it will be overwritten.
   pub fn insert(&mut self, idx: usize, value: T) {
      let (ia, ib) = ia(idx);

      while self.blocks.len() <= ia { self.blocks.push(None) }
      if self.blocks[ia].is_none() {
         self.blocks[ia] = Some(Box::new(Block::empty()))
      }

      let block = self.block_mut(ia);
      let exists = block.usage.contains(ib);
      if exists {
         unsafe { ptr::drop_in_place(block.data[ib].as_mut_ptr()) }
      }
      block.data[ib].write(value);
      block.usage.set(ib);
      self.hash += 1;
      if !exists { self.size += 1; }
   }

   /// Get a reference to an element.
   /// Causes undefined behaviour if the element does not exist.
   pub unsafe fn get_ref(&self, idx: usize) -> &T {
      let (ia, ib) = ia(idx);
      self.block(ia).data[ib].get_ref()
   }

   /// Get a mutable reference to an element.
   /// Causes undefined behaviour if the element does not exist.
   pub unsafe fn get_mut(&mut self, idx: usize) -> &mut T {
      let (ia, ib) = ia(idx);
      self.block_mut(ia).data[ib].get_mut()
   }

   /// Get an iterator over the components in the store
   pub fn iter<'a>(&'a self) -> impl Iterator<Item = (usize, &'a T)> + 'a {
      self.blocks.iter()
         .enumerate()
         .filter_map(|(ia, a)| a.as_ref().map(|block| (ia, block)))
         .flat_map(|(ia, block)| {
            (0..BLOCK_SIZE).filter_map(move |ib| {
               if block.usage.contains(ib) {
                  unsafe { Some((ia * BLOCK_SIZE + ib, block.data[ib].get_ref())) }
               } else {
                  None
               }
            })
         })
   }

   /// Get a mutable iterator over the components in the store
   pub fn iter_mut<'a>(&'a mut self) -> impl Iterator<Item = (usize, &'a mut T)> + 'a {
      self.blocks.iter_mut()
         .enumerate()
         .filter_map(|(ia, a)| a.as_mut().map(|block| (ia, block)))
         .flat_map(|(ia, block)| {
            let block: *mut _ = block;
            (0..BLOCK_SIZE).filter_map(move |ib| {
               let block: &'a mut _ = unsafe { &mut *block };
               if block.usage.contains(ib) {
                  unsafe { Some((ia * BLOCK_SIZE + ib, block.data[ib].get_mut())) }
               } else {
                  None
               }
            })
         })
   }

   /// Get a reference to the ith block
   pub(crate) fn block(&self, i: usize) -> &Block<T> {
      self.blocks[i].as_ref().unwrap().as_ref()
   }

   /// Get a mutable reference to the ith block
   pub(crate) fn block_mut(&mut self, i: usize) -> &mut Block<T> {
      self.blocks[i].as_mut().unwrap().as_mut()
   }
}

#[cfg(not(doc))]
type StoreIter<'a, T> = impl Iterator<Item=&'a T>;
#[cfg(not(doc))]
type StoreIterMut<'a, T> = impl Iterator<Item=&'a mut T>;

#[cfg(not(doc))]
impl<'a, T> IntoIterator for &'a Store<T> {
   type Item = &'a T;
   type IntoIter = StoreIter<'a, T>;
   fn into_iter(self) -> StoreIter<'a, T> {
      self.iter().map(|(_, x)| x)
   }
}

#[cfg(not(doc))]
impl<'a, T> IntoIterator for &'a mut Store<T> {
   type Item = &'a mut T;
   type IntoIter = StoreIterMut<'a, T>;
   fn into_iter(self) -> StoreIterMut<'a, T> {
      self.iter_mut().map(|(_, x)| x)
   }
}

impl<T: Default> Store<T> {
   pub fn emplace(&mut self, idx: usize) {
      self.insert(idx, T::default())
   }
}

use std::intrinsics::type_name;

impl<T> ops::Index<usize> for Store<T> {
   type Output = T;
   fn index(&self, i: usize) -> &T {
      assert!(self.has(i), "Entity {} is missing {}", i, type_name::<T>());
      unsafe { self.get_ref(i) }
   }
}

impl<T> ops::IndexMut<usize> for Store<T> {
   fn index_mut(&mut self, i: usize) -> &mut T {
      assert!(self.has(i), "Entity {} is missing {}", i, type_name::<T>());
      unsafe { self.get_mut(i) }
   }
}

impl<T> Drop for Store<T> {
   fn drop(&mut self) {
      for mut v in self.blocks.drain(..).filter_map(|v| v) {
         let block = v.as_mut();
         for i in 0..BITSET_WORD_SIZE {
            if block.usage.contains(i) {
               unsafe { ptr::drop_in_place(block.data[i].as_mut_ptr()); }
            }
         }
      }
   }
}
