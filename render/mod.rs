//! Rendering and such. Implementation currently unoptimized.

mod anim;
mod colour;
mod debug;
mod depth;
mod draw;
mod fixed_width;
mod shaders;
mod materials;
mod texture;

pub use self::anim::*;
pub use self::colour::*;
pub use self::debug::*;
pub use self::depth::*;
pub use self::draw::*;
pub use self::fixed_width::*;
pub use self::shaders::*;
pub use self::materials::*;
pub use self::texture::*;

use crate::geom::*;
use glium::backend::*;
use glium::uniforms::*;
use glium::vertex::*;
use std::boxed::*;
use once_cell::unsync::OnceCell;

// Glium avoids global state by having it's functions for creating buffers etc take a reference
// to the current context (window / headless context). To simplify our API we store that context
// in a thread-local variable, attempts to access it from outside the main thread panic.
thread_local!{ static FACADE: OnceCell<Box<dyn Facade>> = OnceCell::new(); }

/// Get the thread-local rendering context, required by various glium functions.
pub fn facade() -> &'static dyn Facade {
   FACADE.with(|f| {
      match f.get() {
         Some(b) => unsafe { &*(&**b as *const _) },
         None => panic!("facade() called without an active window on the current thread")
      }
   })
}

/// Test if the current thread has a rendering context.
pub fn has_facade() -> bool {
   FACADE.with(|f| f.get().is_some())
}

/// Set the thread-local rendering context
pub(crate) fn init_facade(facade: Box<dyn Facade>) {
   FACADE.with(|f| {
      if let Err(_) = f.set(facade) {
         panic!("Attempted to create multiple windows on the same thread");
      }
   });
}

unsafe impl Attribute for Vec2 {
   #[inline]
   fn get_type() -> AttributeType {
      AttributeType::F32F32
   }
}

unsafe impl Attribute for Mat3 {
   #[inline]
   fn get_type() -> AttributeType {
      AttributeType::F32x3x3
   }
}

impl AsUniformValue for Vec2 {
   fn as_uniform_value(&self) -> UniformValue {
      UniformValue::Vec2(self.to_arr())
   }
}

impl AsUniformValue for Vec2i {
   fn as_uniform_value(&self) -> UniformValue {
      UniformValue::IntVec2(self.to_arr())
   }
}

impl AsUniformValue for Mat3 {
   fn as_uniform_value(&self) -> UniformValue {
      UniformValue::Mat3(self.to_arr())
   }
}
